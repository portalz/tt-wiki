<?php

namespace App\Http\Middleware;

use Closure;
use Cookie;
use Illuminate\Http\Response;

class VerifyConsent
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Cookie::get('VerifiedConsent') == null) {
            //The verification cookie is not set. Find something else for them to do until they get the cookie set.
            return new Response(view('misc/ConsentVerification'));
        }
        //The users has verified that they can legally view the site and are willing to
        return $next($request);
    }
}
