<?php

namespace App\Http\Controllers;

use function Couchbase\defaultDecoder;
use Illuminate\Http\Request;
use DB;
use App\Quotation;

class WikiController extends Controller
{
    public function WikiRequestParse(Request $request)
    {

        $type = $request->query('type');
        $id = $request->query('id');

        switch ($type) {
            case "effect":
                if (DB::table('active_effects')->where('Id', $id)->exists()) {
                    $effect_row = DB::table('active_effects')->where('Id', $id)->first();
                    $PreRequesite = DB::table('active_effects')->where('Id', $effect_row->PreRequisiteEffectSourceId)->first();
                    $ObtainedAtLocation = DB::table('active_locations')->where('dbName', $effect_row->ObtainedAtLocation)->first();
                    $CurseSpell = DB::table('active_spells')->where('GivesEffectSourceId', $effect_row->Id)->first();
                    $item_row = DB::table('active_items')->where('GivesEffectSourceId', $effect_row->Id)->first();
                    return view('ChildPageTemplates/EffectPageTemplate', ['data' => $effect_row, 'PreRequesite' => $PreRequesite, 'ObtainedAtLocation' => $ObtainedAtLocation, 'CurseSpell' => $CurseSpell, 'Item' => $item_row]);

                } else {
                    abort(404);
                }
                break;

            case "form":
                if (DB::table('active_forms')->where('Id', $id)->exists()) {
                    $form_row = DB::table('active_forms')->where('Id', $id)->first();
                    if ($form_row->ItemSourceId != null) {
                        return redirect('/wiki/wiki.php?type=item&id=' . $form_row->ItemSourceId);
                    }
                    else
                    {
                        $spell_row = DB::table('active_spells')->where('FormSourceId', $form_row->Id)->first();
                        $spell_loc = $this->GetSpellLocation($spell_row);
                        $exclusives_row = DB::table('active_spells')->where('ExclusiveToFormSourceId', $form_row->Id)->get();
                        $Portrait = DB::table('image_relations')->leftJoin('artist_details', 'image_relations.Artist', '=', 'artist_details.Id')->where('FileName', $form_row->PortraitUrl)->first();

                        return view('ChildPageTemplates/FormPageTemplate', ['data' => $form_row, 'spelldata' => $spell_row, 'exclusivesdata' => $exclusives_row, 'Portrait' => $Portrait, 'SpellLocation' => $spell_loc]);
                    }

                }
                {
                    abort(404);
                }
                break;

            case "furniture":
                if (DB::table('active_furniture')->where('Id', $id)->exists()) {
                    $furniture_row = DB::table('active_furniture')->where('Id', $id)->first();
                    //Furniture Sub-queries
                    $ItemGiven = DB::table('active_items')->where('Id', $furniture_row->GivesItemSourceId)->first();
                    $EffectGiven = DB::table('active_effects')->where('Id', $furniture_row->GivesEffectSourceId)->first();
                    $Portrait = DB::table('image_relations')->leftJoin('artist_details', 'image_relations.Artist', '=', 'artist_details.Id')->where('FileName', $furniture_row->PortraitUrl)->first();
                    return view('ChildPageTemplates/FurniturePageTemplate', ['data' => $furniture_row, 'ItemGiven' => $ItemGiven, 'EffectGiven' => $EffectGiven, 'Portrait' => $Portrait]);
                } else {
                    abort(404);
                }

                break;

            case "item":
                if (DB::table('active_items')->where('Id', $id)->exists()) {
                    $item_row = DB::table('active_items')->where('Id', $id)->first();
                    $Portrait = DB::table('image_relations')->leftJoin('artist_details', 'image_relations.Artist', '=', 'artist_details.Id')->where('FileName', $item_row->PortraitUrl)->first();
                    $form_row = DB::table('active_forms')->where('ItemSourceId', $item_row->Id)->first();
                    if ($form_row) {
//                      All of these queries rely on a form existing for the item, so we'll only do them if one exists.
                        $spell_row = DB::table('active_spells')->where('FormSourceId', $form_row->Id)->first();
                        $spell_loc = $this->GetSpellLocation($spell_row);
                        $tfcurses = DB::table('active_forms')->where('Id', $item_row->CurseTFFormSourceId)->first();
                        $exclusives_row = DB::table('active_spells')->where('ExclusiveToItemSourceId', $item_row->Id)->get();
                    } else {
//                      workaround for runes and consumables not having a form, just set all of the other collections to be an empty collection
                        $spell_loc = $spell_row = $tfcurses = $exclusives_row = collect([]);
                    }
                    $GivesEffect = DB::table('active_effects')->where('Id', $item_row->GivesEffectSourceId)->first();
                    return view('ChildPageTemplates/ItemPageTemplate', ['data' => $item_row, 'GivesEffect' => $GivesEffect, 'formdata' => $form_row, 'tfcurses' => $tfcurses, 'spelldata' => $spell_row, 'SpellLocation' => $spell_loc, 'exclusivesdata' => $exclusives_row, 'Portrait' => $Portrait]);
                } else {
                    abort(404);
                }
                break;

            case "spell":
                if (DB::table('active_spells')->where('Id', $id)->exists()) {
                    $spell_row = DB::table('active_spells')->where('Id', $id)->first();
                    $form_row = DB::table('active_forms')->where('Id', $spell_row->FormSourceId)->first();
                    if (DB::table('active_forms')->where('Id', $spell_row->FormSourceId)->exists()) {
                        $item_row = DB::table('active_items')->where('Id', $form_row->ItemSourceId)->first();
                    } else
                        $item_row = collect([]);

                    $ExclusiveToItem = DB::table('active_items')->where('Id', $spell_row->ExclusiveToItemSourceId)->first();
                    $ExclusiveToForm = DB::table('active_forms')->where('Id', $spell_row->ExclusiveToFormSourceId)->first();

                    $GivesEffect = DB::table('active_effects')->where('Id', $spell_row->GivesEffectSourceId)->first();

                    $spell_loc = $this->GetSpellLocation($spell_row);
                    return view('ChildPageTemplates/SpellPageTemplate', ['data' => $spell_row, 'GivesEffect' => $GivesEffect, 'formdata' => $form_row, 'SpellLocation' => $spell_loc, 'itemdata' => $item_row, 'ExclusiveToForm' => $ExclusiveToForm, 'ExclusiveToItem' => $ExclusiveToItem]);

                } else {
                    abort(404);
                }
                break;
            default:
                abort(404);
                break;
        }
    }

    public function GetSpellLocation($source_entry)
    {
//        var_dump($source_entry);
        if (isset($source_entry)) {
            if (strlen($source_entry->LearnedAtLocation)) {
                return DB::table('active_locations')->where('dbName', $source_entry->LearnedAtLocation)->get();
            }

            if (strlen($source_entry->LearnedAtRegion)) {
                return DB::table('active_locations')->where('Region', $source_entry->LearnedAtRegion)->get();
            }
        } else {
            return false;
        }

    }

}
