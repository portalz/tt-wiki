<?php
/**
 * Copyright (c) 2018., K.S. (kportalz@protonmail.com)
 */

namespace App\Http\Controllers;

use App\Models\Effect;
use App\Models\Form;
use Illuminate\Http\Request;
use DB;
use App\Quotation;
use App\Models\Item;
use App\Models\Spell;
use App\Models\Furniture;
use App\Models\Location;

class APIController extends Controller
{
    public function ValidateKey()
    {
        header('Access-Control-Allow-Origin: *');
        $result = DB::table('API_Keys')->where('API_Key', $_GET['key'])->where('valid', 1)->first();
        if ($result)
            return true;
        else
            return false;
    }

    public function dump_ids()
    {
        header('Access-Control-Allow-Origin: *');
        if ($this->ValidateKey()) {
            $effects = DB::table('active_effects')->select('Id', 'FriendlyName')->get();
            $forms = DB::table('active_forms')->select('Id', 'FriendlyName')->get();
            $furniture = DB::table('active_furniture')->select('Id', 'FriendlyName')->get();
            $items = DB::table('active_items')->select('Id', 'FriendlyName')->get();
            $spells = DB::table('active_spells')->select('Id', 'FriendlyName')->get();
            $results = collect(['effects' => $effects, 'forms' => $forms, 'furniture' => $furniture, 'items' => $items, 'spells' => $spells]);
            echo $results->toJson();
        } else
            abort(403);
    }

    public function dump_row()
    {
        if ($this->ValidateKey()) {
            //Must have dbname OR (never AND) id, AND a type. If this rule is not met, it is a bad request.
            if ((isset($_GET['dbname']) xor isset($_GET['id'])) && isset($_GET['type'])) {
                if (isset($_GET['dbname'])) {
                    $row = 'dbName';
                    $discriminator = $_GET['dbname'];
                } elseif (isset($_GET['id'])) {
                    $row = 'Id';
                    $discriminator = $_GET['id'];
                } else {
                    abort(400);
                }
                switch ($_GET['type']) {
                    case "effect":
                        $result = DB::table('active_effects')->where($row, $discriminator)->get();
                        break;
                    case "form":
                        $result = DB::table('active_forms')->where($row, $discriminator)->get();
                        break;
                    case "furniture":
                        $result = DB::table('active_furniture')->where($row, $discriminator)->get();
                        break;
                    case "item":
                        $result = DB::table('active_items')->where($row, $discriminator)->get();
                        break;
                    case "spell":
                        $result = DB::table('active_spells')->where($row, $discriminator)->get();
                        break;
                    default:
                        echo "what";
                        break;
                }
                echo $result->toJson();
            } else {
                abort(400);
            } ///end validate input
        } else {
            abort(403);
        }
    }

    public function autocomplete()
    {
        $query = $_GET['term'];
        $limit = 3;
        $ee = DB::table('ee')->select('eep')->where('eeloc', '=', 'autocomp')->where('eep', 'like', '%' . $query . '%')->limit($limit)->get();
        $items = DB::table('active_items')->select('FriendlyName')->where('FriendlyName', 'like', '%' . $query . '%')->limit($limit)->get();
        $forms = DB::table('active_forms')->select('FriendlyName')->where('MobilityType', '=', 'full')->where('FriendlyName', 'like', '%' . $query . '%')->limit($limit)->get();
        $effects = DB::table('active_effects')->select('FriendlyName')->where('FriendlyName', 'like', '%' . $query . '%')->orWhere('Description', 'like', '%' . $query . '%')->limit($limit)->get();
        $spells = DB::table('active_spells')->select('FriendlyName')->where('FriendlyName', 'like', '%' . $query . '%')->limit($limit)->get();
        $furniture = DB::table('active_furniture')->select('FriendlyName')->where('FriendlyName', 'like', '%' . $query . '%')->orWhere('dbType', 'like', '%' . $query . '%')->limit($limit)->get();
        echo ($items->values()->merge($forms->values())->merge($effects->values())->merge($spells->values())->merge($furniture->values()))->pluck('FriendlyName')
            ->merge($ee->values()->pluck('eep'));

    }

    //API Stuff for the discord bot

    public function cogSearch(Request $request)
    {
        $limit = 5;
        $results = collect();
        if ($request->query('type') == "effect" || $request->query('type') == null) {
            $results['effect'] = Effect::select('id', 'FriendlyName')->where('FriendlyName', 'like', '%' . $request->query('q') . '%')->limit($limit)->get();
            if (count($results['effect']) < $limit)
                $results['effect'] = $results['effect']->merge(Effect::select('id', 'FriendlyName')->where('Description', 'like', '%' . $request->query('q') . '%')->limit($limit - count($results['effect']))->get());
        }
        if ($request->query('type') == "form" || $request->query('type') == null) {
            $results['form'] = Form::select('id', 'FriendlyName')->where('FriendlyName', 'like', '%' . $request->query('q') . '%')->where('MobilityType', '=', 'full')->limit($limit)->get();
            if (count($results['form']) < $limit)
                $results['form'] = $results['form']->merge(Form::select('id', 'FriendlyName')->where('Description', 'like', '%' . $request->query('q') . '%')->where('MobilityType', '=', 'full')->limit($limit - count($results['form']))->get());
        }
        if ($request->query('type') == "item" || $request->query('type') == null) {
            $results['item'] = Item::select('id', 'FriendlyName')->where('FriendlyName', 'like', '%' . $request->query('q') . '%')->limit($limit)->get();
            if (count($results['item']) < $limit)
                $results['item'] = $results['item']->merge(Item::select('id', 'FriendlyName')->where('Description', 'like', '%' . $request->query('q') . '%')->limit($limit - count($results['item']))->get());
        }
        if ($request->query('type') == "spell" || $request->query('type') == null) {

            $results['spell'] = Spell::select('id', 'FriendlyName')->where('FriendlyName', 'like', '%' . $request->query('q') . '%')->limit($limit)->get();
            if (count($results['spell']) < $limit)
                $results['spell'] = $results['item']->merge(Spell::select('id', 'FriendlyName')->where('Description', 'like', '%' . $request->query('q') . '%')->limit($limit - count($results['spell']))->get());
        }
        if ($request->query('type') == "furniture" || $request->query('type') == null) {

            $results['furniture'] = Furniture::select('id', 'FriendlyName')->where('FriendlyName', 'like', '%' . $request->query('q') . '%')->orWhere('Description', 'like', '%' . $request->query('q') . '%')->limit(3)->get();
        }
        return response()->json($results);
    }

    public function cogLookup(Request $request)
    {
        $q = $request->query('q');
        if ($request->query('type') == null || $q == null) {
            abort(400);
        }
        switch ($request->query('type')) {
            case "effect":
                if (is_numeric($q)) {
                    return response()->json(Effect::find($q));
                } else {
                    return response()->json(Effect::where('FriendlyName', 'like', '%' . $q . '%')->first());
                }
                break;
            case "form":
                if (is_numeric($q)) {
                    return response()->json(Form::find($q));
                } else {
                    return response()->json(Form::where('FriendlyName', 'like', '%' . $request->query('q') . '%')->where('MobilityType', '=', 'full')->first());
                }
                break;
            case "furniture":
                if (is_numeric($q)) {
                    return response()->json(Furniture::find($q));
                } else {
                    return response()->json(Furniture::where('FriendlyName', 'like', '%' . $q . '%')->first());
                }
                break;
            case "item":
                if (is_numeric($q)) {
                    return response()->json(Item::find($q));
                } else {
                    return response()->json(Item::where('FriendlyName', 'like', '%' . $q . '%')->first());
                }
                break;
            case "location":
                if (is_numeric($q)) {
                    return response()->json(Location::find($q));
                } else {
                    return response()->json(Location::where('Name', 'like', '%' . $q . '%')->first());
                }
                break;
            case "spell":
                if (is_numeric($q)) {
                    return response()->json(Spell::find($q));
                } else {
                    return response()->json(Spell::where('FriendlyName', 'like', '%' . $q . '%')->first());
                }
                break;
            default:
                abort(400);

        }
    }

    public function checkImageAuthorization(Request $request)
    {
        if (!DB::table('image_relations')->where("FileName", $request->query("file"))->first())
            abort(401);
    }
}