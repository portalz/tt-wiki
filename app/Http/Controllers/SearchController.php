<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Quotation;

//TTwiki specific models
use App\Models\Effect;
use App\Models\Form;
use App\Models\Item;
use App\Models\Spell;
use App\Models\Furniture;

DB::enableQueryLog();

class SearchController extends Controller
{
    public function PostSearch(Request $request)
    {
        $query = $request->input('query');
        $effect_results = DB::table('active_effects')->where('FriendlyName', 'like', '%' . $query . '%')->orWhere('Description', 'like', '%' . $query . '%')->get();
        $form_results = DB::table('active_forms')->where('MobilityType', 'full')->where('FriendlyName', 'like', '%' . $query . '%')->orWhere('Description', 'like', '%' . $query . '%')->get();
        $item_results = DB::table('active_items')->where('FriendlyName', 'like', '%' . $query . '%')->orWhere('Description', 'like', '%' . $query . '%')->get();
        $spell_results = DB::table('active_spells')->where('FriendlyName', 'like', '%' . $query . '%')->orWhere('Description', 'like', '%' . $query . '%')->get();
        $furniture_results = DB::table('active_furniture')->where('FriendlyName', 'like', '%' . $query . '%')->orWhere('dbType', 'like', '%' . $query . '%')->orWhere('Description', 'like', '%' . $query . '%')->get();
        return view('WikiSearch', ['query' => $query, 'forms' => $form_results, 'effects' => $effect_results, 'items' => $item_results, 'spells' => $spell_results, 'furniture' => $furniture_results]);
    }

    public function Browse(Request $request, $type = null)
    {
//        Create empty collections so things can be optional
        if ($type != null) {
            $form_results = $effect_results = $item_results = $spell_results = $furniture_results = collect();
        }
        if ($type == "forms" or $type == null)
            $form_results = DB::table('active_forms')->where('MobilityType', '=', 'full')->get();
        if ($type == "effects" or $type == null)
            $effect_results = DB::table('active_effects')->get();
        if ($type == "items" or $type == null)
            $item_results = DB::table('active_items')->get();
        if ($type == "spells" or $type == null)
            $spell_results = DB::table('active_spells')->get();
        if ($type == "furniture" or $type == null)
            $furniture_results = DB::table('active_furniture')->get();
        return view('WikiBrowse', ['forms' => $form_results, 'effects' => $effect_results, 'items' => $item_results, 'spells' => $spell_results, 'furniture' => $furniture_results]);

    }

    public function TagFilterSearch(Request $request)
    {
        $form_results = $effect_results = $item_results = $spell_results = $furniture_results = collect();
        $filter = $request->input('filter');
        $query = $request->input('query');

        // Macro filters, which can have subfilters. This section separates the filter prefix and sets that as the filter. Later we'll pull the full filter back down from the input.
        // Item type filter
        if (substr($filter, 0, 9) == "item-type")
            $filter = "item-type";
        // Form gender filter
        if (substr($filter, 0, 11) == "form-gender")
            $filter = "form-gender";
        // Spell type filters
        if (substr($filter, 0, 10) == "spell-type")
            $filter = "spell-type";

        // Switch depending on the filter
        switch ($filter) {
            //item type filter
            case "item-type":
                $itemtype = substr($request->input('filter'), 10);
                //validate input
                if (!in_array($itemtype, ['consumable', 'accessory', 'underpants', 'shoes', 'pants', 'shirt', 'consumable_reuseable', 'undershirt', 'hat', 'pet', 'rune']))
                    abort(400);
                $item_results = DB::table('active_items')->where('ItemType', '=', $itemtype)->get();
                break;
            case "item-tfcurse":
                $item_results = Item::whereNotNull('CurseTfFormSourceId')->get();
                break;
            case "item-gives-effect":
                $item_results = Item::whereNotNull('GivesEffectSourceId')->get();
                break;
            //Form Filters
            case "form-gender":
                $gender = substr($request->input('filter'), 12);
                if (!in_array($gender, ['male', 'female']))
                    abort(400);
                $form_results = Form::where('Gender', '=', $gender)->where('MobilityType', '=', 'Full')->get();
                break;
            case "form-immobile":
                $form_results = Form::where('MoveActionPointDiscount', '<', '-200')->where('MobilityType', '=', 'Full')->get();
                break;
            //Spell filters
            case "spell-type":
                $spell_type = substr($request->input('filter'), 11);
                if (!in_array($spell_type, ['full', 'animal', 'mindcontrol', 'inanimate', 'curse']))
                    abort(400);
                $spell_results = Spell::where('MobilityType', '=', $spell_type)->get();
                break;
            case "spell-exclusivetoform":
                $spell_results = Spell::whereNotNull('ExclusiveToFormSourceId')->get();
                break;
            case "spell-exclusivetoitem":
                $spell_results = Spell::whereNotNull('ExclusiveToItemSourceId')->get();
                break;
            case "spell-nonlearnable":
                $spell_results = Spell::where('IsPlayerLearnable', 0)->get();
                break;
            //Effect Filters
            case "effect-source-lvlperk":
                $effect_results = Effect::whereNotNull('isLevelUpPerk')->get();
                break;
            case "effect-source-search":
                $effect_results = Effect::whereNotNull('ObtainedAtLocation')->get();
                break;
        }
        return view('WikiBrowse', ['filter' => $filter . "['" . $request->input('filter') . "']", 'forms' => $form_results, 'effects' => $effect_results, 'items' => $item_results, 'spells' => $spell_results, 'furniture' => $furniture_results]);


    }
}
