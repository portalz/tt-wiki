<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use DB;
use App\Quotation;

class LandingPage extends Controller
{
    public function Main()
    {
        $message = Db::table('ee')->where('eeloc', '=', 'landing')->inRandomOrder()->first();
        return view('PortalzMain/Master', ['message' => $message]);

    }
}
