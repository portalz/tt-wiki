<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
use Cookie;

class ConsentController extends Controller
{
    public function SetCookie(Request $request)
    {
        Cookie::queue('VerifiedConsent', 'true', 20160);
        return back();
    }
}
