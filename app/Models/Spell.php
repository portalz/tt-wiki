<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Spell extends Model
{
    public $timestamps = false;
    protected $table = 'active_spells';
}
