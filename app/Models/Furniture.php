<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Furniture extends Model
{
    public $timestamps = false;
    protected $table = 'active_furniture';
}
