<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::get('wiki/browse.php', 'SearchController@Browse'

//Autocomplete API, does not get ratelimiting and should not allow cross-site access
Route::get('autocomplete', 'APIController@autocomplete');

//* API Endpoints for the Discord bot */
Route::prefix('cog')->group(function () {
    Route::get('search', 'APIController@cogSearch');
    Route::get('lookup', 'APIController@cogLookup');
    Route::get('imageauth','APIController@checkImageAuthorization');

});
