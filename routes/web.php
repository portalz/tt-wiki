<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//**********************************************************************************************************************
//AS A GENERAL GUIDELINE, USE THE VERIFYCONSENT MIDDLEWARE ON ALL PAGES. IT CONFIRMS THAT A USER IS OF AGE.
//**********************************************************************************************************************


//Sample route that returns a view and uses the VerifyConsent middleware.
//Route::get('wiki', function () {
//    return view("WikiPage");
//})->middleware('VerifyConsent');

//Route::get('wiki/{type}/{id}', ['uses' =>'WikiController@WikiRequestParse', 'as'=>'wikipageroute']);
Route::get('wiki/wiki.php', ['uses' => 'WikiController@WikiRequestParse', 'as' => 'wikipageroute'])->middleware('VerifyConsent');

//Routes for the main search page
//GET Routes
Route::get('wiki', function () {
    return view("WikiSearch");
})->middleware('VerifyConsent');
Route::get('wiki/index.php', function () {
    return view("WikiSearch");
})->middleware('VerifyConsent');

Route::get('wiki', function () {
    return view("WikiSearch");
})->middleware('VerifyConsent')->name('Wiki');
Route::get('wiki/index.php', function () {
    return view("WikiSearch");
})->middleware('VerifyConsent');

Route::get('wiki/browse.php', 'SearchController@Browse')->middleware('VerifyConsent');
Route::get('wiki/browsebytag', 'SearchController@TagFilterSearch')->middleware('VerifyConsent');
Route::get('wiki/browse/{type?}', 'SearchController@Browse')->middleware('VerifyConsent')->name('Browse');

//POST routes
Route::post('wiki', 'SearchController@PostSearch')->middleware('VerifyConsent')->name('Search');;
Route::post('wiki/index.php', 'SearchController@PostSearch')->middleware('VerifyConsent');;

//Privacy Policy, does not require verify-consent.
Route::get('wiki/privacy', function () {
    return view("misc/PrivacyPolicy");
})->name('privacypolicy');

//The following routes should NEVER have the VerifyConsent middleware. These are API requests, or other routes outside the intended scope of the middleware.
Route::post('/wiki/VerifyConsent', 'ConsentController@SetCookie');

//Non-Wiki
Route::get('/', ['uses' => 'LandingPage@Main', 'as' => 'PortalzMain']);



//Legacy Routes so as not to break bookmarks
Route::redirect('/wiki/spellmap.php', '/legacy/spellmap.php', 301);
Route::redirect('/wiki/tables', '/tables/', 301);
Route::redirect('/wiki/browse.php', '/wiki/browse', 301);

//Old wiki links
Route::redirect('/newwiki', '/wiki', 301);
Route::redirect('/newwiki/browse.php', '/wiki/browse', 301);
Route::redirect('/newwiki/wiki.php', '/wiki', 301);


//Legacy routes  for things that haven't been ported yet and aren't super critical to get ported
Route::redirect('/wiki/quotes.php', '/legacy/quotes.php', 302);
Route::redirect('/wiki/stats.php', '/legacy/stats.php', 302);

//Routes to force error pages
Route::group(['prefix' => 'error'], function () {
    Route::get('404', function () {
        return view("errors/404");
    });

    Route::get('500', function () {
        return view("errors/500");
    });
    Route::get('503', function () {
        return view("errors/503");
    });
});