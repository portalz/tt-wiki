Welcome to the TTWiki project.
This is a browsable database for *Transformania Time!* game data. For a short time, it was more wiki-y than it is now, but some people abused the ability to edit and I didn't want to moderate it so here it is now, partially automated and not user-editable. 


Setup
======

_Requirements_
------------
* A server of some kind, even a Raspberry Pi will do. This readme covers Debian and Ubuntu systems. For RedHat, CentOS and Fedora based operating systems, you are on your own.
* [PHP 7.2](http://www.php.net/)
* [Apache HTTP Server](https://projects.apache.org/project.html?httpd-http_server) (NGINX works too, but I'm not going to go through the additional effort of making a guide for that, so if you prefer NGINX, you're on your own.)
* [MySQL](https://www.mysql.com/) or [MariaDB](https://mariadb.org/)

_Recommended_
-------------

* [ModSecurity](https://modsecurity.org/)
* [mod_cloudflare (only if you'll be using CloudFlare)](https://www.cloudflare.com/technical-resources/) 
* Disable ServerTokens in Apache and disable mod_autoindex


Steps to do things
------------------
I'm not going to write about how to setup apache, php and mysql since many people already have, so here's [this tutorial @ digitalocean](https://www.digitalocean.com/community/tutorials/how-to-install-linux-apache-mysql-php-lamp-stack-on-ubuntu-16-04).

Make a new directory in `/var/www/` called `yoursite.tld` (swap yoursite.tld with portalz.xyz or whatever your site is called) and clone this repo to it.

Follow [this guide @ digitalocean](https://www.digitalocean.com/community/tutorials/how-to-set-up-apache-virtual-hosts-on-ubuntu-14-04-lts) starting at Step 4 to setup Virtualhosts.
In your vhost config, set `/var/www/yoursite.tld/public` as the `DocumentRoot`.
You may need to configure AllowOverride in your vhost configuration to allow web routes to work properly


Okay, now that you have that out of the way let's begin.

Create a database. Create a user with a password. Give it full permissions to the database you made. Voila.

Copy `.env.example` to `.env` and configure it to your desired settings and configure your MySQL database details.

Run `php artisan migrate` to generate the empty databases.

To begin the import, run ```php update_all.php --all``` in the `cli/update` directory, it will download the latest JSON data from the TTWiki master server and import it. Omitting the --all flag on update_all.php will display options to manually import certain tables.


Legal
=====

I do not guarantee warranty of any kind.
 
Pages are generated from a database of content from the game *Transformania Time*. 

http://fairuse.stanford.edu/overview/fair-use/four-factors/

All images are used with the permission of their artists.


Credits
=======

_Third-Party Software_
--------------------

* [Laravel](https://laravel.com/)
* [Twitter Bootstrap](http://getbootstrap.com)
* [Pixelate.js](https://github.com/43081j/pixelate.js)
* [jQuery](https://jquery.com/)
* [jQuery UI](https://jqueryui.com/)
* [Moment.js](https://momentjs.com/)
* [FontAwesome](http://fontawesome.io/)
* [Sorttable.js](https://www.kryogenix.org/code/browser/sorttable/)


_Special Thanks To_
-----------------
* [Transformania Time](https://gitlab.com/transformania/tt-game)'s Dev Team (Judoo, Arrhae, Tempest and Eric)
* Transformania Time's many content creators








