#!/usr/bin/env bash
while test $# -gt 0; do
        case "$1" in
                -p|--pull)
                        php7.3 artisan down
                        git pull
                        php7.3 artisan up
                        exit 0
                        ;;
                -db|--db-import)
                        php7.3 artisan down
                        cd cli/update
                        php7.3 update_all.php -a
                        cd ../..
                        php7.3 artisan up
                        exit 0
                        ;;
                -off)
                        php7.3 artisan down
                        exit 0
                        ;;
                -on)    php7.3 artisan up
                        exit 0
                        ;;
                -h|--help|*)
                        echo "manage.sh"
                        echo " "
                        echo "usage: manage.sh [arguments]"
                        echo " "
                        echo "options:"
                        echo "-h, --help                show this help menu help"
                        echo "-p, --pull               pull new version from git"
                        echo "-db, --db-import  run a database import from master"
                        echo "-off               put the site in maintenance mode"
                        echo "-on           take the site out of maintenance mode"
                        echo
                        exit 0
                        ;;

        esac
done
