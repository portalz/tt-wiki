@extends('Master')
@section('title')
    Browse
@endsection
@section('content')
    @include('Subviews/SearchBox')
    @isset($filter)
        <?php $result_type = null;?>
        <p>using filter {{$filter}}</p>
    @endisset
    @include('Subviews/SearchCluster')
@endsection