@extends('Master')
@section('title')
    Privacy Policy
@endsection
@section('content')
    <h1><a id="Privacy_Policy_0"></a>Privacy Policy</h1>
    <hr>
    <h2><a id="Table_of_Contents_4"></a>Table of Contents</h2>
    <p><a href="#Summary_10">Summary</a><br>
        <a href="#Requests_47">Requests</a><br>
        <a href="#tldr_50">tl;dr</a>
    </p>
    <h2><a id="Summary_10"></a>Summary</h2>
    <h3><a id="Information_this_site_utilises_12"></a>Information this site uses</h3>
    <ul>
        <li>cookies</li>
        <li>IP addresses (under certain conditions, detail below)</li>
    </ul>
    <h3><a id="What_we_do_with_your_information_16"></a>What we do with your information</h3>
    <p><strong>Cookies</strong></p>
    <p>Cookies are used for the following purposes:</p>
    <ul>
        <li>Remembering that the user has verified that they are 18 or older.</li>
        <li>Telling the server whether or not to show each announcement in the site header.</li>
        <li>CSRF (cross-site request forgery) Attack Prevention</li>
    </ul>
    <p>This site does not access or set cross-site cookies.</p>
    <p><strong>IP Addresses</strong></p>
    <p>During routine browsing, this site does <em>not</em> record your IP address.<br>
        Apache/Nginx/ST-Quantum server access logs are written directly to the system’s “null device” (/dev/null), which
        discards everything immediately.</p>
    <p>If user-modifiable content is added in a future release, IP addresses may be recorded alongside edit logs for
        abuse control purposes.</p>
    <h3><a id="Where_data_is_stored_34"></a>Where data is stored</h3>
    <p><strong>Cookies</strong></p>
    <ul>
        <li>On your machine.</li>
    </ul>
    <p><strong>IP Addresses</strong></p>
    <ul>
        <li>Nowhere.</li>
    </ul>
    <h3><a id="Log_Purging_43"></a>Log Purging</h3>
    <p>Server access logs are discarded immediately.</p>
    <h2><a id="Requests_47"></a>Requests</h2>
    <p>If you would like to make a data request of some kind for some reason, please contact <a
                href="mailto:kportalz@protonmail.com">kportalz@protonmail.com</a> with the subject “DATA REQUEST”.</p>
    <hr>
    <h2><a id="tldr_50"></a>tl;dr</h2>
    <p>We don’t store personal information anywhere or give it to anyone ever.</p>
@endsection