<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Content Warning</title>
</head>
<body>
<div style="text-align:center;">
    <h1>Warning</h1>
    <p>This site contains content that is not to be viewed by users under the age of 18.</p>

    <h2><a href="{{route('privacypolicy')}}">[Privacy Policy]</a></h2>
    {!! Form::open(['url' => 'wiki/VerifyConsent']) !!}
    {{--//the button--}}
    {{ csrf_field() }}
    {!! Form::submit('I agree and wish to continue.'); !!}
    {!! Form::close() !!}
    <h2><a href="http://google.com/">I would like to leave this site.</a></h2>
    <p>If you continue to see this message, there might be an issue with either the site configuration or your
        browser.</p>
</div>
</body>
</html>