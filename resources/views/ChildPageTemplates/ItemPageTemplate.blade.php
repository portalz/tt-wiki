@extends('WikiPage')

@section('title')
    {{$data->FriendlyName}}
@endsection

@section('intermediatecrumbs')
    <li class="breadcrumb-item"><a href="{{config("app.url")}}/wiki/browse/items">Items</a></li>
@endsection

@section('wikibody')
    <div id="desc">
        <p><span class="lead">Description</span></p>
        <p>Third Person</p>
        <blockquote>
            <p>
                <em>
                    <small>{{$data->Description}}
                    </small>
                </em>
            </p>
        </blockquote>
        {{--There isn't a form present for consumables or runes--}}
        @if($formdata)
            <p>Second Person </p>
            <blockquote>
                <em>
                    <small>
                        {{$formdata->Description}}
                    </small>
                </em>
            </blockquote>
        @endif

        @if($data->UsageMessage_Item)
            <div>
                <p>Item's Perspective Usage Message</p>
                <blockquote>
                    <em>
                        <small>
                            {{$data->UsageMessage_Item}}
                        </small>
                    </em>
                </blockquote>
            </div>
        @endif

        @if($data->UsageMessage_Player)
            <div>
                <p>Owner's Perspective Usage Message</p>
                <blockquote>
                    <em>
                        <small>
                            {{$data->UsageMessage_Player}}
                        </small>
                    </em>
                </blockquote>
            </div>
        @endif


    </div>
    <hr>
    @if($data->ItemType == "rune" || $data->ItemType == "consumable" || $data->ItemType == "consumable_reuseable")
        @include("Subviews/StatTable")
    @else
        <p>
            <span class="lead">Stats</span>
        </p>
        <p>
            Stats for this item type are determined by the runes applied to it.
        </p>
    @endif

@endsection


@section('wikisidebar')
    @include("Subviews/Portrait")
    @if($data->MoneyValue!=0)
        <p><strong>Base Purchase Price: </strong><abbr title="Arpeyjis">⩜</abbr>{{$data->MoneyValue}}</p>
    @endif
    @if($data->MoneyValueSell!=0)
        <p><strong>Base Sell Price: </strong><abbr title="Arpeyjis">⩜</abbr>{{$data->MoneyValueSell}}</p>
    @endif

    {{--Item Type--}}
    <p><strong>Item Type: </strong>{{$data->ItemType}}</p>
    {{--If the item is a consumable, there are subtypes.--}}
    @if($data->ItemType=="consumable")
        <p><strong>Consumable Type: </strong>
            @switch($data->ConsumableSubItemType)
                @case(null)
                    Other
                    @break
                @case(0)
                    Rune
                    @break
                @case(1)
                    Tome
                    @break
                @case(2)
                    Spellbook
                    @break
                @case(3)
                    Restorative
                    @break
                @case(4)
                    Willpower Bomb
                    @break
            @endswitch
        </p>
    @endif




    @isset($GivesEffect)
        <p><strong>Gives Effect: </strong><a
                    href="wiki.php?type=effect&id={{$GivesEffect->Id}}">{{$GivesEffect->FriendlyName}}</a></p>
    @endisset
    @isset($spelldata->Id)
        <p><strong>Associated Spell: </strong> <a
                    href="wiki.php?type=spell&id={{$spelldata->Id}}">{{$spelldata->FriendlyName}}</a></p>
        @include("Subviews/SpellLocation")
    @endisset

    {{--If the item has a TF curse, display what it is.--}}
    @isset($tfcurses->Id)
        <p><strong>TF Curse: </strong><a href="wiki.php?type=form&id={{$tfcurses->Id}}">{{$tfcurses->FriendlyName}}</a>
        </p>
    @endisset
    @if (count($exclusivesdata) > 0)
        <p><strong>Item Exclusive Spells & Curses:</strong></p>
        <ul>
            @foreach($exclusivesdata as $exclusive)
                <li><a href="wiki.php?type=spell&id={{$exclusive->Id}}">{{$exclusive->FriendlyName}}</a></li>
            @endforeach
        </ul>
    @endif
    <hr>
    <div id="tags">
        <p><b>Tags</b></p>
        <a href="/wiki/browsebytag?filter=item-type-{{$data->ItemType}}"><span
                    class="badge label-tag badge-secondary">{{$data->ItemType}}</span></a>

        @if ($data->CurseTFFormSourceId)
            <a href="/wiki/browsebytag?filter=item-tfcurse"><span class="badge label-tag badge-primary">TF Curse</span></a>
        @endif   @if($data->GivesEffectSourceId)
            <a href="/wiki/browsebytag?filter=item-gives-effect"><span
                        class="badge label-tag badge-purple">Gives effect</span></a>
        @endif


    </div>
    <hr>
    <div id="tags">
        <p><b>Tags</b></p>
        <?php
        $result_type = "item";
        $result = $data;
        ?>
        @include('Subviews/SearchTags')
    </div>
    <hr>
    <div class="debug">
        <b>Debug Information</b>
        @isset($formdata)
            <p>Form Id: <kbd>{{$formdata->Id}}</kbd></p>
        @endisset

        <p>Id: <kbd>{{$data->Id}}</kbd></p>
{{--        <p>dbName: <samp>{{$data->dbName}}</samp></p>--}}
        <p>Image File name: <samp>{{$data->PortraitUrl}}</samp></p>
    </div>
@endsection