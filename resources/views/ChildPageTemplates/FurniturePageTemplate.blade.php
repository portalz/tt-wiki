@extends('WikiPage')

@section('title')
    {{$data->FriendlyName}}
@endsection

@section('intermediatecrumbs')
    <li class="breadcrumb-item"><a href="{{config("app.url")}}/wiki/browse/furniture">Furniture</a></li>
@endsection

@section('wikibody')
    <div id="desc">
        <p>
            <span class="lead">Description</span>
        </p>
        <blockquote>
            <p>
                <em>
                    <small>{{$data->Description}}</small>
                </em>
            </p>
        </blockquote>
    </div>

@endsection

@section('wikisidebar')
    @include("Subviews/Portrait")
    @isset($EffectGiven)
        <p><strong>Gives effect: </strong><a
                    href="wiki.php?type=effect&id={{$EffectGiven->Id}}">{{$EffectGiven->FriendlyName}}</a></p>
    @endisset
    @isset($ItemGiven)
        <p><strong>Gives item: </strong><a
                    href="wiki.php?type=item&id={{$ItemGiven->Id}}">{{$ItemGiven->FriendlyName}}</a></p>
    @endisset
    @if($data->APReserveRefillAmount)
        <p><strong>AP Reserve Fill Amount: </strong>{{$data->APReserveRefillAmount}}</p>
    @endif

    <hr>
    <div id="tags">
        <p><b>Tags</b></p>
        <?php
        $result_type = "furniture";
        $result = $data;
        ?>
        @include('Subviews/SearchTags')
    </div>
    <hr>
    <div class="debug">
        <b>Debug Information</b>
        <p>Id: <kbd>{{$data->Id}}</kbd></p>
{{--        <p>dbName: <samp>{{$data->dbName}}</samp></p>--}}
        <p>Image File name: <samp>{{$data->PortraitUrl}}</samp></p>
    </div>

@endsection

