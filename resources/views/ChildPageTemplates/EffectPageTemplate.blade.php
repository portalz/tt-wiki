@extends('WikiPage')

@section('title')
    {{$data->FriendlyName}}
@endsection

@section('intermediatecrumbs')
    <li class="breadcrumb-item"><a href="{{config("app.url")}}/wiki/browse/effects">Effects</a></li>
@endsection

@section('wikibody')
    <div id="desc">
        <p>
            <span class="lead">Description</span>
        </p>
        <blockquote>
            <p>
                <em>
                    <small>{{$data->Description}}</small>
                </em>
            </p>
        </blockquote>
    </div>
    @include("Subviews/StatTable")
@endsection

@section('wikisidebar')
    @if($data->isLevelUpPerk)
        <p><strong>Type: </strong> Level Up Perk </p>
        @isset($data->AvailableAtLevel)
            <p><strong>Unlock Level: </strong>{{$data->AvailableAtLevel}}</p>
        @endisset
        @isset($PreRequesite)
            <p><strong>Prerequisite: </strong> <a
                        href="wiki.php?type=effect&id={{$PreRequesite->Id}}">{{$PreRequesite->FriendlyName}}</a></p>
        @endisset
    @elseif(isset($ObtainedAtLocation))
        <p><strong>Type: </strong> Random Search </p>
        <p><strong>Location: </strong> {{$ObtainedAtLocation->Name}}</p>
    @elseif(isset($Item))
        <p><strong>Type: </strong> Item Perk </p>
        <p><strong>Item: </strong> <a
                    href="wiki.php?type=item&id={{$Item->Id}}">{{$Item->FriendlyName}}</a></p>
    @elseif($data->BlessingCurseStatus!=0)
        //Blessing or Curse? Currently these are not in use (as of 3/11/2019)
        <p><strong>Type: </strong>
            @switch($data->BlessingCurseStatus)
                @case(1)
                    Curse
                    @break
                @case(2)
                    Blessing
                    @break
            @endswitch
        </p>
        <p><strong>Spell: </strong> <a
                    href="wiki.php?type=spell &id={{$CurseSpell->Id}}">{{$CurseSpell->FriendlyName}}</a></p>
    @endif

    @if($data->Duration || $data->Cooldown)
        <p><strong>Duration: </strong> {{$data->Duration}}</p>
        <p><strong>Cooldown: </strong> {{$data->Cooldown}}</p>
    @endif
    {{--Required Gamemode for this Effect--}}
    @if($data->RequiredGameMode)
        <p><strong>Required Gamemode </strong>
            @switch($data->RequiredGameMode)
                @case(-1)
                    Any Gamemode
                    @break
                @case(0)
                    Superprotection
                    @break
                @case(1)
                    Protection
                    @break
                @case(2)
                    PvP
                    @break
            @endswitch
        </p>
    @endif
    <hr>
    <div id="tags">
        <p><b>Tags</b></p>
        <?php
        $result_type = "effect";
        $result = $data;
        ?>
        @include('Subviews/SearchTags')
    </div>
    <hr>
    <div class="debug">
        <b>Debug Information</b>
        <p>Id: <kbd>{{$data->Id}}</kbd></p>
{{--        <p>dbName: <samp>{{$data->dbName}}</samp></p>--}}
    </div>
@endsection

