@extends('WikiPage')

@section('title')
    {{$data->FriendlyName}}
@endsection

@section('intermediatecrumbs')
    <li class="breadcrumb-item"><a href="{{config(" app.url")}}/wiki/browse/spells">Spells</a></li>
@endsection

@section('wikibody')
    <div id="desc">
        <p><span class="lead">Description</span></p>
        <blockquote>
            <p>
                <em>
                    <small>{{$data->Description}}
                    </small>
                </em>
            </p>
        </blockquote>
        {{--Not all spells have a discovery message, only display it if it's relevant--}}
        @isset($data->DiscoveryMessage)
            <p><span class="lead">Discovery Message</span></p>
            <blockquote>
                <p>
                    <em>
                        <small>{{$data->DiscoveryMessage}}
                        </small>
                    </em>
                </p>
            </blockquote>
        @endisset

    </div>

@endsection

@section('wikisidebar')

    <p><strong>Spell Type: </strong>
        @switch($data->MobilityType)
            @case("animal")
                Pet
                @break
            @case("full")
                Animate
                @break
            @case("curse")
                Curse
                @break
            @case("inanimate")
                Inanimate
                @break
            @case("mindcontrol")
                Mind Control
                @break
            @case("weaken")
                Weaken
                @break
            @default
                Unknown, "{{$data->MobilityType}}"
                @break
        @endswitch
    </p>
    @if($data->MobilityType==="inanimate"||$data->MobilityType==="full" || $data->MobilityType==="animal")
        <p><strong>Form or Item: </strong>
            @if($data->MobilityType==="inanimate" || $data->MobilityType==="animal")
                <a href="wiki.php?type=item&id={{$itemdata->Id}}">{{$itemdata->FriendlyName}}</a>
            @elseif($data->MobilityType==="full")
                <a href="wiki.php?type=form&id={{$formdata->Id}}">{{$formdata->FriendlyName}}</a>
            @endif
        </p>
    @endif
    @isset($GivesEffect)
        <p><strong>Gives Effect: </strong><a
                    href="wiki.php?type=effect&id={{$GivesEffect->Id}}">{{$GivesEffect->FriendlyName}}</a></p>
    @endisset

    @isset($SpellLocation)
        @include("Subviews/SpellLocation")
    @endisset
    @isset($ExclusiveToForm)
        <p><strong>Exclusive to Form: </strong> <a
                    href="wiki.php?type=form&id={{$ExclusiveToForm->Id}}">{{$ExclusiveToForm->FriendlyName}}</a></p>
    @endisset
    @isset($ExclusiveToItem)
        <p><strong>Exclusive to Item: </strong> <a
                    href="wiki.php?type=item&id={{$ExclusiveToItem->Id}}">{{$ExclusiveToItem->FriendlyName}}</a></p>
    @endisset

    <hr>
    <div id="tags">
        <p><b>Tags</b></p>
        <?php
        $result_type = "spell";
        $result = $data;
        ?>
        @include('Subviews/SearchTags')
    </div>
    <hr>
    <div class="debug">


        <b>Debug Information</b>
        <p>Id: <kbd>{{$data->Id}}</kbd></p>
{{--        <p>dbName: <samp>{{$data->dbName}}</samp></p>--}}

    </div>
@endsection

