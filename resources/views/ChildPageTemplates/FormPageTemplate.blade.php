@extends('WikiPage')

@section('title')
    {{$data->FriendlyName}}
@endsection

@section('intermediatecrumbs')
    <li class="breadcrumb-item"><a href="{{config("app.url")}}/wiki/browse/forms">Forms</a></li>
@endsection

@section('wikibody')
    {{--Description of the form--}}
    <div id="desc">
        <p>
            <span class="lead">Description</span>
        </p>
        <blockquote>
            <p>
                <em>
                    <small>{{$data->Description}}</small>
                </em>
            </p>
        </blockquote>
    </div>

    <hr>
    @include("Subviews/StatTable")
@endsection

@section('wikisidebar')
    @include("Subviews/Portrait")
    <p>
        <strong>Gender: </strong>{{$data->Gender}}
    </p>
    @isset($spelldata)
        <p><strong>Associated Spell: </strong> <a
                    href="wiki.php?type=spell&id={{$spelldata->Id}}">{{$spelldata->FriendlyName}}</a></p>
        @include("Subviews/SpellLocation")
    @endif

    @if (count($exclusivesdata) > 0)
        <p><strong>Form Exclusive Spells & Curses:</strong></p>
        <ul>
            @foreach($exclusivesdata as $exclusive)
                <li><a href="wiki.php?type=spell&id={{$exclusive->Id}}">{{$exclusive->FriendlyName}}</a></li>
            @endforeach
        </ul>
    @endif
    <hr>
    <div id="tags">
        <p><b>Tags</b></p>
        <?php
        $result_type = "form";
        $result = $data;
        ?>
        @include('Subviews/SearchTags')
    </div>
    <hr>

    <div class="debug">
        <b>Debug Information</b>
        <p>Id: <kbd>{{$data->Id}}</kbd></p>
{{--        <p>dbName: <kbd>{{$data->dbName}}</kbd></p>--}}
    </div>
@endsection

