@extends('Master')
@section('title')
    @if(isset($query))
        {{$query}} – Search
    @else
    Main Page
    @endif

@endsection
@section('content')
    @include('Subviews/SearchBox')
    @isset($query)
        <?php $result_type = null;?>
        <p>Showing results for: {{$query}}</p>
        @include('Subviews/SearchCluster')
    @endisset
@endsection