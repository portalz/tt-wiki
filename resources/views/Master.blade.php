<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('title') - {{ config("app.name")}} </title>
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/jquery-ui.min.css">
    <link rel="stylesheet" href="/css/wiki-custom.css">
    <script src="/js/jquery.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/jquery-ui.min.js"></script>
    <script src="/js/imageblur.js"></script>
    <script src="/js/spoiler.js"></script>
    <script src="/js/statscale.js"></script>
    <script src="/js/notices.js"></script>
    <script>
        if ($("img").length) //don't blur if no images
        {
            document.querySelector('img').pixelate(); // fall back if jQuery doesn't load.
            $('img').pixelate();
        }
        $(document).ready(function () {
            $(".search").autocomplete({
                source: "/api/autocomplete",
            });
        });

    </script>
</head>
<body>
<div class="container">
    <div class="page-header">
        <h1><a href="/wiki">{{config("app.name")}}</a>
            <small>@yield('title')</small>
        </h1>
    </div>

    @if(env('TTW_ENABLE_NOTICES')===true)
        @include("Subviews/Notices")
    @endif
    <nav>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/wiki">{{config("app.name")}}</a></li>
            @yield('intermediatecrumbs')
            <li class="breadcrumb-item active">@yield('title')</li>
        </ol>
    </nav>

    @yield('content')

    <footer>
        <hr>
        <div class="text-center">
            <p class="small">
                There is no guarantee on the accuracy of the data used by this site, although it is sourced from a copy of the actual game data.
                <br>
                The content from which this site's database is generated is based on content used by the game <i>Transformania
                    Time</i>, and are owned by their respective creators.
                <br>
                If there are any issues or bugs with this site, please reach out in the <a href="https://discord.gg/z66CYzX">Transformania Time Discord</a>.
            </p>
            <p class="debug" style="color:#C0C0C0;">
                <small>
                    Page generated in {{ (microtime(true) - LARAVEL_START) }} seconds
                </small>

            </p>
            <p class="footer-links">
                <small>
                    <a href="{{route('privacypolicy')}}">[Privacy Policy]</a>
                </small>
            </p>

        </div>
    </footer>
</div>
</body>
</html>