<?php
$compstats = array('Discipline', 'Perception', 'Charisma', 'Fortitude', 'Agility', 'Allure', 'Magicka', 'Succour', 'Luck');
$expstats = array("HealthBonusPercent", "ManaBonusPercent", "ExtraSkillCriticalPercent", "HealthRecoveryPerUpdate", "ManaRecoveryPerUpdate", "SneakPercent", "AntiSneakPercent", "EvasionPercent", "EvasionNegationPercent", "MeditationExtraMana", "CleanseExtraHealth", "MoveActionPointDiscount", "SpellExtraTFEnergyPercent", "SpellExtraHealthDamagePercent", "CleanseExtraTFEnergyRemovalPercent", "SpellMisfireChanceReduction", "SpellHealthDamageResistance", "SpellTFEnergyDamageResistance", "ExtraInventorySpace", 'InstantHealthRestore', 'InstantManaRestore', 'ReusableHealthRestore', 'ReusableManaRestore');

//This probably belongs in a controller instead of a blade view but at this point I've stopped caring.
//This is the original stat conversion code from TTWiki's "convert_stats.php" but ever so slightly modified to work with Laravel's "Collections"
function convertStats($holo)
{
    $ha = $holo;
    $oldstats = array();
    $stats = array("HealthBonusPercent", "ManaBonusPercent", "ExtraSkillCriticalPercent", "HealthRecoveryPerUpdate", "ManaRecoveryPerUpdate", "SneakPercent", "AntiSneakPercent", "EvasionPercent", "EvasionNegationPercent", "MeditationExtraMana", "CleanseExtraHealth", "MoveActionPointDiscount", "SpellExtraTFEnergyPercent", "SpellExtraHealthDamagePercent", "CleanseExtraTFEnergyRemovalPercent", "SpellMisfireChanceReduction", "SpellHealthDamageResistance", "SpellTFEnergyDamageResistance", "ExtraInventorySpace", "InstantHealthRestore", "InstantManaRestore", "ReuseableHealthRestore", "ReuseableManaRestore");
    $oldstats["HealthBonusPercent"] = ($ha->Discipline * (.5)) + ($ha->Fortitude * (1));
    $oldstats["ManaBonusPercent"] = $ha->Magicka * .75;
    $oldstats["ExtraSkillCriticalPercent"] = ($ha->Agility * 0.075) + ($ha->Luck * 0.3);
    $oldstats["HealthRecoveryPerUpdate"] = $ha->Succour * .9;
    $oldstats["ManaRecoveryPerUpdate"] = $ha->Succour * .26;
    $oldstats["SneakPercent"] = $ha->Agility * .25;
    $oldstats["AntiSneakPercent"] = $ha->Perception * .5;
    $oldstats["EvasionPercent"] = ($ha->Agility * .3) + ($ha->Discipline * -0.1);
    $oldstats["EvasionNegationPercent"] = $ha->Perception * .15;
    $oldstats["MeditationExtraMana"] = $ha->Allure * .15;
    $oldstats["CleanseExtraHealth"] = $ha->Allure * .5;
    $oldstats["MoveActionPointDiscount"] = $ha->Agility * 0.0025;

    $oldstats["SpellExtraTFEnergyPercent"] = ($ha->Perception * .5) + ($ha->Magicka * 1);
    $oldstats["SpellExtraHealthDamagePercent"] = ($ha->Perception * .375) + ($ha->Charisma * .75);
    $oldstats["CleanseExtraTFEnergyRemovalPercent"] = $ha->Allure * .02;
    $oldstats["SpellMisfireChanceReduction"] = ($ha->Discipline * .08) + ($ha->Perception * .08) + ($ha->Luck * .08);
    $oldstats["SpellHealthDamageResistance"] = ($ha->Discipline * .5);
    $oldstats["SpellTFEnergyDamageResistance"] = ($ha->Discipline * .25);
    $oldstats["ExtraInventorySpace"] = $ha->Fortitude * .04;
    if (isset($stats['ReusableHealthRestore'])) {
        $oldstats["ReuseableHealthRestore"] = $stats['ReuseableHealthRestore'];
    } else {
        $oldstats["ReusableHealthRestore"] = 0;

    }
    if (isset($stats['ReuseableManaRestore'])) {
        $oldstats["ReuseableManaRestore"] = $stats['ReuseableManaRestore'];
    } else {
        $oldstats["ReusableManaRestore"] = 0;

    }
    if (isset($stats['InstantHealthRestore'])) {
        $oldstats["InstantHealthRestore"] = $stats['InstantHealthRestore'];
    } else {
        $oldstats["InstantHealthRestore"] = 0;
    }
    if (isset($stats['InstantManaRestore'])) {
        $oldstats["InstantManaRestore"] = $stats['InstantManaRestore'];
    } else {
        $oldstats["InstantManaRestore"] = 0;
    }
    return $oldstats;
}
$oldstats = convertStats($data);
?>
<div id="comp">
    <p>
        <span class="lead">Composite Stats</span>
    </p>
    <p>Level: <input id="formlevelinput" type="number" min="1" value="1"/></p>
    <table class="table table-bordered">
        <thead>
        <tr>
            <td>Stat</td>
            <td>Value</td>
        </tr>
        </thead>
        <tbody>
        @foreach($compstats as $stat)
            {{--This changes the display names of a couple of stats because they're stored as a different name in the database than they're shown as in-game.--}}
            <?php
            if ($stat == "Allure") {
                $stat_disp = "Restoration";
            } elseif ($stat == "Succour") {
                $stat_disp = "Regeneration";
            } else {
                $stat_disp = $stat;
            }
            ?>
            @if($data->$stat!=0)
                <tr id="stat-{{$stat}}" data-defval="{{$data->$stat}}"
                    class="@if($data->$stat>0){{"table-success"}}@elseif($data->$stat<0){{"table-danger"}}@endif">
                    <td>{{$stat_disp}}</td>
                    <td id="statcol-{{$stat}}">{{$data->$stat}}</td>
                </tr>
            @endif
        @endforeach
        </tbody>
    </table>
</div>
<div id="expanded-stats">
    <p>
        <span class="lead">Expanded Stats</span>
    </p>
    <table class="table table-bordered">
        <thead>
        <tr>
            <td>Stat</td>
            <td>Value</td>
        </tr>
        </thead>
        <tbody>
        <?php
        //    var_dump($oldstats);
        ?>
        @foreach($expstats as $stat)
            @if($oldstats["$stat"]!=0)
                <tr id="stat-{{$stat}}" data-defval="{{$oldstats["$stat"]}}"
                    class="@if($oldstats["$stat"]>0){{"table-success"}}@elseif($oldstats["$stat"]<0){{"table-danger"}}@endif">
                    <td>{{$stat}}</td>
                    <td id="statcol-{{$stat}}">{{$oldstats["$stat"]}}</td>
                </tr>
            @endif
        @endforeach

        @if(isset($data->MoveActionPointDiscount) AND $data->MoveActionPointDiscount!=0)
        <tr id="stat-MoveActionPointDiscount-Override"
            class="@if($data->MoveActionPointDiscount>0){{"table-success"}}@elseif($data->MoveActionPointDiscount<0){{"table-danger"}}@endif">
            <td><b>MoveActionPointDiscount (explicitly set in legacy stat field)</b></td>
            <td id="statcol-MoveActionPointDiscount-Override"><b>{{$data->MoveActionPointDiscount}}</b></td>
        </tr>
        @endif
        </tbody>
    </table>
    <p>Notice: Expanded stats are subject to change and may not be accurate.</p>
</div>
