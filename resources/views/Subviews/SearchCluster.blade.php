@if(!$forms->isEmpty())
    <?php $result_type = "form";?>
    <h2>Forms ({{$forms->count()}} results)</h2>
    <dl>
        @foreach($forms as $result)
            @if($result->MobilityType=="full")
                @include('Subviews/SearchEntry')
            @endif
        @endforeach
    </dl>
@endif
@if(!$items->isEmpty())
    <?php $result_type = "item";?>
    <h2>Items ({{$items->count()}} results)</h2>
    <dl>
        @foreach($items as $result)
            @include('Subviews/SearchEntry')
        @endforeach
    </dl>
@endif
@if(!$spells->isEmpty())
    <?php $result_type = "spell";?>
    <h2>Spells ({{$spells->count()}} results)</h2>
    <dl>
        @foreach($spells as $result)
            @include('Subviews/SearchEntry')
        @endforeach
    </dl>
@endif
@if(!$effects->isEmpty())
    <?php $result_type = "effect";?>
    <h2>Effects ({{$effects->count()}} results)</h2>
    <dl>
        @foreach($effects as $result)
            @include('Subviews/SearchEntry')
        @endforeach
    </dl>
@endif
@if(!$furniture->isEmpty())
    <?php $result_type = "furniture";?>
    <h2>Furniture ({{$furniture->count()}} results)</h2>
    <dl>
        @foreach($furniture as $result)
            @include('Subviews/SearchEntry')
        @endforeach
    </dl>
@endif