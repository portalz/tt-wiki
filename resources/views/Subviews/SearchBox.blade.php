<form action="{{route('Search')}}" method="POST">
    Looking for something? (<a href="{{route('Browse')}}">or browse entries</a>)
    (<a href="/legacy/spellmap.php">map of spells</a>)
    <input type="text" name="query" class="search"
           required
           value="@isset($query){{$query}}@endisset">
    {{ csrf_field() }}
    <input type="submit" name="submit" value="Submit">
</form>