@switch($result_type)
    @case("item")
        <a href="/wiki/browsebytag?filter=item-type-{{$result->ItemType}}"><span
                    class="badge label-tag badge-secondary">{{$result->ItemType}}</span></a>
        @if ($result->CurseTFFormSourceId)
            <a href="/wiki/browsebytag?filter=item-tfcurse"><span class="badge label-tag badge-primary">TF Curse</span></a>
        @endif   @if($result->GivesEffectSourceId)
            <a href="/wiki/browsebytag?filter=item-gives-effect"><span
                        class="badge label-tag badge-purple">Gives effect</span></a>
        @endif

        @break


    @case("form")
        @if($result->Gender=="male")
            <a href="/wiki/browsebytag?filter=form-gender-male"><span class="badge label-tag badge-blue">Male</span></a>
        @endif
        @if($result->Gender=="female")
            <a href="/wiki/browsebytag?filter=form-gender-female"><span
                        class="badge label-tag badge-purple">Female</span></a>
        @endif
        @if($result->MoveActionPointDiscount<-200)
            <a href="/wiki/browsebytag?filter=form-immobile"><span
                        class="badge label-tag badge-danger">Immobile</span></a>
        @endif
        @break
    @case("spell")
        @switch($result->MobilityType)
            @case("full")
                <a href="/wiki/browsebytag?filter=spell-type-{{$result->MobilityType}}"><span
                            class="badge label-tag badge-success">Animate</span></a>
                @break
            @case("animal")
                <a href="/wiki/browsebytag?filter=spell-type-{{$result->MobilityType}}"><span
                            class="badge label-tag badge-warning">Animal</span></a>
                @break
            @case("mindcontrol")
                <a href="/wiki/browsebytag?filter=spell-type-{{$result->MobilityType}}"><span
                            class="badge label-tag badge-purple">Mind Control</span></a>
                @break
            @case("inanimate")
                <a href="/wiki/browsebytag?filter=spell-type-{{$result->MobilityType}}"><span
                            class="badge label-tag badge-danger">Inanimate</span></a>
                @break
            @case("curse")
                <a href="/wiki/browsebytag?filter=spell-type-{{$result->MobilityType}}"><span
                            class="badge label-tag badge-purple">Curse</span></a>
                @break
        @endswitch
        @if($result->ExclusiveToFormSourceId)
            <a href="/wiki/browsebytag?filter=spell-exclusivetoform"><span
                        class="badge label-tag badge-info">Form Exclusive</span></a>
        @endif
        @if($result->ExclusiveToItemSourceId)
            <a href="/wiki/browsebytag?filter=spell-exclusivetoitem"><span
                        class="badge label-tag badge-info">Item Exclusive</span></a>
        @endif
        @if($result->IsPlayerLearnable=="false")
            <a href="/wiki/browsebytag?filter=spell-nonlearnable"><span
                        class="badge label-tag badge-danger">Non-learnable</span></a>
        @endif
        @break
    @case("effect")
        @if($result->isLevelUpPerk)
            <a href="/wiki/browsebytag?filter=effect-source-lvlperk"><span class="badge label-tag badge-success">Level-Up Perk</span></a>
        @elseif($result->ObtainedAtLocation)
            <a href="/wiki/browsebytag?filter=effect-source-search"><span class="badge label-tag badge-secondary">Random Search</span></a>
            {{--@else--}}
            {{--<span class="badge label-tag badge-purple">origin unknown</span>--}}
        @endif
        @break
@endswitch





