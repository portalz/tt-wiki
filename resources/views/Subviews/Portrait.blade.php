@if(isset($Portrait))
    <p class="text-center"><img src="https://cdn.portalz.xyz/img/{{$Portrait->FileName}}"
                                class="img-thumbnail pix" alt="TTImage" height="200" width="200" data-pixelate></p><p
            class="text-center">Graphic by <a href="{{$Portrait->ArtistPage}}">{{$Portrait->FriendlyName}}</a></p>
    <hr><p>


@else
    <p class="text-center">
        <img src="https://cdn.portalz.xyz/img/404.png" class="img-thumbnail" height="200" width="200"
             alt="The artist of this graphic has either not given permission to display their art, or we haven't gotten to it yet.">
    </p>
    <p>
        <small>We couldn't give you the image, so here's something else.</small>
    </p>
    <p>
        <small>"Sorry" by Nyx.</small>
    </p>
    <hr>
@endif
