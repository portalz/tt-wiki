<p><strong>Spell Location:</strong> @if(empty($SpellLocation)) This spell cannot be found.@endif</p>
@if(!empty($SpellLocation))
    <div id="spellloc" class="spoiler hidden-spoiler">
        <ul>
            {{--Loop through array of spell locations--}}
            @foreach($SpellLocation as $location)
                <li>{{$location->Name}}</li>
                {{--If there are more than 5, stop.--}}
                @break($loop->iteration>5)
            @endforeach
            {{--Display 'and x more' if there are more than 5--}}
            @if($SpellLocation->count()>5)
                <li>and {{$SpellLocation->count()-5}} more locations</li>
            @endif
        </ul>
    </div>
@endif
