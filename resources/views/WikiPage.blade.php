@extends('Master')
@section('content')
    <div class="row">

        <div class="col-md-8 card">
            <div class="card-body">
                @yield('wikibody')
            </div>
        </div>

        <div class="col-md-4 card panel-default wiki-sidebar">
            @yield('wikisidebar')
        </div>

    </div>
@endsection