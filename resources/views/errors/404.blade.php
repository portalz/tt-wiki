@extends('errors/error_master')
@section('title','404 Not Found')
@section('friendlytitle','404 Not Found')
@section('body')
    <p>Sorry! The page you tried to access could not be found. If you were linked here from somewhere else on this site, perhaps the archives are incomplete (and you should probably let someone know!).</p>
    <!-- Get it? It's a reference to the Star Wars prequels -->
@endsection
@section('longerror','404 Not Found')