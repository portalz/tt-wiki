@extends('errors/error_master')
@section('title','Service Unavailable')
@section('friendlytitle','We\'ll be back soon!')
@section('body')
    <p>Sorry for the inconvenience. Please try again in a few minutes! If this message persists, contact the site
        administrator.</p>
@endsection
@section('longerror','503 Service Unavailable')