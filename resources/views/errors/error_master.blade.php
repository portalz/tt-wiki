<!doctype html>
<title>{{ config("app.name")}} - @yield('title')</title>
<style>
    body {
        text-align: center;
        padding: 150px;
    }

    h1 {
        font-size: 50px;
    }

    body {
        font: 20px Helvetica, sans-serif;
        color: #333;
    }

    article {
        display: block;
        text-align: left;
        width: 650px;
        margin: 0 auto;
    }

    a {
        color: #dc8100;
        text-decoration: none;
    }

    a:hover {
        color: #333;
        text-decoration: none;
    }

    .hr {
        width: 25%;
        text-align: left;
        margin: 0px;
        height: 1px;
        color: #dc8100;
        background-color: #dc8100;

    }
</style>

<article>
    <h1>@yield('friendlytitle')</h1>
    <hr class="hr" style="color: #dc8100;">
    <div>
        @yield('body')

        <p>
            <small>@yield('longerror')</small>
        </p>

    </div>
</article>