@extends('errors/error_master')
@section('title','Internal Server Error')
@section('friendlytitle','Internal Server Error')
@section('body')
    <p>That's strange. Something unexpected happened and an error occurred.</p>
    <iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/t3otBjVZzT0?rel=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
@endsection
@section('longerror','500 Internal Server Error')