 <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Portalz.xyz</title>
    <link href="//cdn.portalz.xyz/css/bootstrap.min.css" rel="stylesheet">
    <link href="//cdn.portalz.xyz/css/custom.css" rel="stylesheet">
    <link rel="shortcut icon" sizes="16x16" href="resc/favicon.ico">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
</head>
<body>
<div class="container">
    <div class="page-header"><h3>ttwiki.transformation.tf</h3></div>
    <div class="row text-center">
        <div class="alert alert-warning" role="alert">
            <strong>Notice: </strong> This site is not intended for users under the age of 18. If you are under 18
            years of age, leave this page.
        </div>
        <a href="wiki" id="w" class="buttonlink">
            <div class="col-md-4 panel hoverbox panel-default button-sexy"><p>Wiki</p>
                <p><i class="fa fa-info-circle fa-5x"></i></p></div>
        </a><a href="wiki/tables" id="t" class="buttonlink">
            <div class="col-md-4 panel hoverbox panel-default button-sexy"><p>Item & Form Tables</p>
                <p><i class="fa fa-sort fa-5x"></i></p></div>
        </a><a href="more" id="m" class="buttonlink">
            <div class="col-md-4 panel hoverbox panel-default button-sexy"><p>More</p>
                <p><i class="fa fa-folder-open fa-5x"></i></p></div>
        </a></div>
</div>
<footer>
</footer>
<script src="//cdn.portalz.xyz/js/jquery.min.js"></script>
<script src="//cdn.portalz.xyz/js/bootstrap.min.js"></script>
</body>
</html>