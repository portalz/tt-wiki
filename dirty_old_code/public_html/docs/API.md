API Documentation
===

Getting Started
---------------

The API endpoints require a "key" query field on most requests, containing a unique API key.
The purpose of the API key is to provide a basic access control
mechanism to the service.


**Getting an API key**

At this point, there is no automated system for generating API keys. Contact your server administrator to obtain one.





**HTTP Response Codes and their use in the API**

| Code | Meaning |
|------|--------------------------------------------------------------------------------------------------------------------------------------------------|
| 200 | Valid request |
| 401 | Invalid or missing API key |
| 403 | Denied, possibly rate limited (depending on server configuration, in production this is a possible sign of being ratelimited). Try again later.  |
| 429 | Rate limited |
| 509 | Rate limited |
|  |  |





Endpoints
---------

**Autocompletion API**

```autocomplete.php```

* `Access-Control-Allow-Origin: *` **is not** sent
* An API key **is not** required

This is intended for the site's built-in search and not for third party use. 

It will not work for off-site requests because it does not send an Access-Control-Allow-Origin header.



**Key Verification API**

```check_key.php```

* `Access-Control-Allow-Origin: *` is sent
* An API key **is** required

This endpoint takes input via a GET request and requires a ```key``` field in the query string containing your API key.



If the key is valid, a 200 status code will be returned with an empty body. If it is invalid, a 401 will be returned with an empty body.


**Cookie Counting API**


```cookiecount.php```

* `Access-Control-Allow-Origin: *` is sent

Nope. No documentation. Sorry.


**dump_ids**

```dump_ids.php```

* `Access-Control-Allow-Origin: *` is sent
* An API key **is** required

This endpoint takes a GET request and requires a ```key``` field in the query string.
It will return a JSON array containing the ```Id``` and ```FriendlyName``` fields of all of content databases.


**dump_row**


```dump_row.php```

* `Access-Control-Allow-Origin: *` is sent
* An API key **is** required

This endpoint will display the contents of an entire row from a specified type.

Request type: GET

_Required fields:_

* ```key``` - Your API key
* ```type``` - String, identifies which kind of content to request.
    Valid :
    * effect
    * form
    * furniture
    * item
    * spell
* ```id``` OR ```dbname```, but not both.

_Special Responses_

* 404 - invalid `type`
* 400 - You've tried to query for both an dbname and an id.
* 200 - Request OK, if the content is *null*, it doesn't exist.



**Image Generator**

```image_gen.php```

* `Access-Control-Allow-Origin: *` **is not** sent, and is not required.
* An API key **is not** required

This endpoint outputs a PNG image that displays stuff.

_Required fields:_

```a``` - String, what to get an image about.  
    Valid :   
   * lastupdate - the last database update time
   
   
**Spells by Location**

```spellbyloc.php```

* `Access-Control-Allow-Origin: *` **is not** sent
* An API key **is not** required

Not intended for third-party use.
    
    
    
    




