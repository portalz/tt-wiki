<?php
/**
 * Copyright (c) 2017.,  K.S. (kportalz@protonmail.com)
 *
 */

$is_api=1;
require_once("../global.php");
$im = imagecreate(300, 30);
$bg = imagecolorallocate($im, 255, 255, 255);
$textcolor = imagecolorallocate($im, 0, 0, 0);
if($_GET['a']=='lastupdate')
{
	$result=mysqli_fetch_row(mysqli_query($link,"SELECT `Timestamp` FROM `config` WHERE `Setting`='LastImport'"))[0];
	imagestring($im, 5, 0, 0, "Last updated: ".$result, $textcolor);
}



// Output the image
header('Content-type: image/png');

imagepng($im);
imagedestroy($im);


?>