<?php
$is_api = 1;
include "../global.php";
$qterm = mysqli_real_escape_string($link, "%" . $_GET['term'] . "%");
$q = "(SELECT `FriendlyName` FROM `active_items` WHERE `FriendlyName` LIKE '$qterm')
        UNION (SELECT `FriendlyName` FROM `active_forms` WHERE (`MobilityType`='full' AND `FriendlyName` LIKE '$qterm'))
        UNION (SELECT `FriendlyName` FROM `active_spells` WHERE `FriendlyName` LIKE '$qterm')
        UNION (SELECT `FriendlyName` FROM `active_effects` WHERE `FriendlyName` LIKE '$qterm')
        UNION (SELECT `FriendlyName` FROM `active_furniture` WHERE `FriendlyName` LIKE '$qterm')";
//some arcane sorcery, this is part of an "extension"
if (ENABLE_EE_FILE == true) {
    $q .= " UNION (SELECT `Keyword` FROM `ee` WHERE (`Keyword` LIKE '$qterm' AND `Enabled`=1))";
}

$q .= " LIMIT 15";
$r = mysqli_query($link, $q);
$results = array();
while ($row = mysqli_fetch_assoc($r)) {
    $result = $row["FriendlyName"];
    array_push($results, $result);
}
$json = json_encode($results);
echo $json;
?>
