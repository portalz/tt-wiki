<?php
/**
 * Copyright (c) 2018., K.S. (kportalz@protonmail.com)
 */
$is_api = 1;
require_once("../global.php");
require_once("check_key.php");

$array=array();
$array["effects"]=$array["forms"]=$array["furniture"]=$array["items"]=$array["spells"]=array();

$effects=mysqli_query($link,"SELECT `Id`,`FriendlyName` FROM `active_effects` WHERE 1");
while ($item = mysqli_fetch_row($effects)) {
    $array["effects"][]=$item;
}
unset($effects);
$forms=mysqli_query($link,"SELECT `Id`,`FriendlyName` FROM `active_forms` WHERE 1");
while ($item = mysqli_fetch_row($forms)) {
    $array["forms"][]=$item;
}
unset($forms);
$furniture=mysqli_query($link,"SELECT `Id`,`FriendlyName` FROM `active_furniture` WHERE 1");
while ($item = mysqli_fetch_row($furniture)) {
    $array["furniture"][]=$item;
}
unset($furniture);
$items=mysqli_query($link,"SELECT `Id`,`FriendlyName` FROM `active_items` WHERE 1");
while ($item = mysqli_fetch_row($items)) {
    $array["items"][]=$item;
}
unset($items);
$spells=mysqli_query($link,"SELECT `Id`,`FriendlyName` FROM `active_spells` WHERE 1");
while ($item = mysqli_fetch_row($spells)) {
    $array["spells"][]=$item;
}
unset($spells);
echo json_encode($array);
unset($array);
mysqli_close($link);
?>