<?php
/**
 * Copyright (c) 2018., K.S. (kportalz@protonmail.com)
 */
$is_api = 1;
require_once("../global.php");
require_once("check_key.php");
if (!isset($_GET['type']) OR !(!isset($_GET['id']) XOR !isset($_GET['dbname']))) {

    if (!(!isset($_GET['id']) XOR !isset($_GET['dbname']))) {
        header("HTTP/1.0 400 Bad Request");
        $array['error'] = "too many arguments";
        die(json_encode($array));
    }
    header("HTTP/1.0 404 Not Found");
    die();
}
if (isset($_GET['dbname']))
    $q = 1;
if (isset($_GET['id']))
    $q = 2;

switch ($q) {
    case 1:
        switch ($_GET['type']) {
            case "effect":
                $stmt = $link->prepare("SELECT * FROM `active_effects` WHERE `dbName`=?");
                break;
            case "form":
                $stmt = $link->prepare("SELECT * FROM `active_forms` WHERE `dbName`=?");
                break;
            case "furniture":
                $stmt = $link->prepare("SELECT * FROM `active_furniture` WHERE `dbType`=?");
                break;
            case "item":
                $stmt = $link->prepare("SELECT * FROM `active_items` WHERE `dbName`=?");
                break;
            case "spell":
                $stmt = $link->prepare("SELECT * FROM `active_spells` WHERE `dbName`=?");
                break;
            default:
                header("HTTP/1.0 404 Not Found");
        }
        $stmt->bind_param("s", $_GET['dbname']);
        break;
    case 2:
        switch ($_GET['type']) {
            case "effect":
                $stmt = $link->prepare("SELECT * FROM `active_effects` WHERE `Id`=?");
                break;
            case "form":
                $stmt = $link->prepare("SELECT * FROM `active_forms` WHERE `Id`=?");
                break;
            case "furniture":
                $stmt = $link->prepare("SELECT * FROM `active_furniture` WHERE `Id`=?");
                break;
            case "item":
                $stmt = $link->prepare("SELECT * FROM `active_items` WHERE `Id`=?");
                break;
            case "spell":
                $stmt = $link->prepare("SELECT * FROM `active_spells` WHERE `Id`=?");
                break;
            default:
                header("HTTP/1.0 404 Not Found");
        }
        $stmt->bind_param("i", $_GET['id']);
        break;
}


$stmt->execute();
$result = $stmt->get_result();
echo json_encode($result->fetch_assoc());
$stmt->close();
$link->close();