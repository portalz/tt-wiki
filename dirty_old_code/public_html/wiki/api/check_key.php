<?php
/**
 * Copyright (c) 2018., K.S. (kportalz@protonmail.com)
 */
$is_api = 1;
require_once("../global.php");
$key = escape($_GET['key'], $link);
if (mysqli_num_rows(mysqli_query($link, "SELECT * FROM `API_Keys` WHERE `API_Key`='$key' AND `valid`=1")) !== 0) {

    header('Access-Control-Allow-Origin: *');
} else {
    header("HTTP/1.1 401 Unauthorized");
    header('Access-Control-Allow-Origin: *');
    die();
}
?>