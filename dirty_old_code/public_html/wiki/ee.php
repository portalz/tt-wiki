<?php
/**
 * Copyright (c) 2017., K.S. (kportalz@protonmail.com)
 *
 *
 */

//eeSearch displays in the search page, add fake results or things
function eeSearch($text)
{
    //Monika not found - Doki Doki Literature Club Reference
    if (strpos(strtolower($text), "monika") !== false || strpos(strtolower($text), "monica") !== false) {
        echo '<div class="console text-center">';
        echo '<kbd>monika.chr does not exist.</kbd>';
        echo '</div>';
    }

    if (strpos(strtolower($text), "firefox") !== false AND true == false) {
        echo
        '<h2 id="spell-title">Web Browsers (1 result)</h2>
         <dl id="spells">
            <p class="result">
                <dt><a  href="https://firefox.com">Firefox <small>(Web Browser)</small></a></dt>
                <dd>An free, open-source web browser developed by the Mozilla Foundation.</dd>
                Tags: <span class="badge label-tag badge-warning">Firefox</span>
            </p>
         </dl>';
    }

}

