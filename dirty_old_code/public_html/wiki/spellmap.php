<?php
/**
 * Copyright (c) 2017.,  K.S. (kportalz@protonmail.com)
 *
 */

$starttime = explode(' ', microtime());
$starttime = $starttime[1] + $starttime[0];

require_once("global.php");

?>
<!doctype html>

<html lang="en">
<head>
    <meta charset="utf-8">

    <title>Sunnyglade Spell Map</title>
    <meta name="description" content="Sunnyglade Spell Map">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    <link rel="stylesheet" href="//cdn.portalz.xyz/js/jquery-ui.min.css">
    <link rel="stylesheet" href="<?= BASE_DIR; ?>/css/spellmap.css">
    <link rel="shortcut icon" sizes="16x16" href="../resc/favicon.ico">
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
    <script src="//cdn.portalz.xyz/js/jquery-ui.min.js"></script>

    <!--[if lt IE 9]>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script>
    <![endif]-->
</head>
<body>

<div class="row">
    <div class="col-md-9">
        <table>
            <?php
            /*
                The required file to connect to the MySQL database
            */
            for ($y = 10; $y >= -7; $y--) {
                echo "<tr class=\"mapline\">";
                for ($x = -12; $x <= 9; $x++) {
                    $result = mysqli_fetch_array(mysqli_query($link, "SELECT * FROM `active_locations` WHERE `X`=$x AND `Y`=$y AND `Region`!='dungeon'"), MYSQLI_ASSOC);

                    if ($result) {
                        $spellcount_local = mysqli_fetch_array(mysqli_query($link, "SELECT COUNT(*) FROM `active_spells` WHERE `LearnedAtLocation`='" . escape($result["dbName"], $link) . "'"));
                        $spellcount_regional = mysqli_fetch_array(mysqli_query($link, "SELECT COUNT(*) FROM `active_spells` WHERE `LearnedAtRegion`='" . escape($result["Region"], $link) . "'"));
                        $spellcount["COUNT(*)"] = $spellcount_local["COUNT(*)"] + $spellcount_regional["COUNT(*)"];
                        $roomclass = " room";
                        if (!$result["Name_North"]) {
                            $roomclass .= " wall-north";
                        }
                        if (!$result["Name_East"]) {
                            $roomclass .= " wall-east";
                        }
                        if (!$result["Name_South"]) {
                            $roomclass .= " wall-south";
                        }
                        if (!$result["Name_West"]) {
                            $roomclass .= " wall-west";
                        }
                        if ($result["Region"] == "streets") {
                            $roomclass .= " streets";
                        }
                        if (isset($_GET['heatmap'])) {

                            if ($spellcount["COUNT(*)"] < 9) {
                                $roomclass .= " spells-" . $spellcount["COUNT(*)"];
                            } elseif ($spellcount["COUNT(*)"] >= 9) {
                                $roomclass .= " spells-9";
                            }
                        }
                    } else {
                        $roomclass = " void";
                        $spellcount_local["COUNT(*)"] = 0;
                        $spellcount_regional["COUNT(*)"] = 0;
                    }
                    echo @'<td class="map-cell ' . noxss($roomclass) . '" data-x=' . noxss($x) . ' data-y=' . noxss($y) . ' data-name="' . noxss($result["Name"]) . '" data-region="' . noxss($result["Region"]) . '" title="(' . noxss($x) . ',' . noxss($y) . ') ' . noxss($result["Name"]) . '" data-localspell="' . noxss($spellcount_local["COUNT(*)"]) . '" data-regionspell="' . noxss($spellcount_regional["COUNT(*)"]) . '" data-dbname="' . noxss($result["dbName"]) . '">(' . noxss($x) . ',' . noxss($y) . ')</td>';
                }
                echo '</tr>' . PHP_EOL;
            }

            ?>
        </table>
    </div>
    <div class="col-md-3">
        <div class="panel panel-default">
            <div class="panel-heading">Spells</div>
            <div class="panel-body" id="spellpanel">
                <table id="spellTable" class="spelltable table table-bordered" style="width:100%;">
                    <thead>
                    <tr>
                        <th>Spell</th>
                        <th style="text-align:right">Spoilers: <input type="checkbox" data-toggle="toggle"
                                                                      data-size="mini" id="spoilLocations"
                                                                      name="spoilLocations" value="n"></th>
                    </tr>
                    </thead>
                    <tbody id="spellList" class="disabled-area">
                    <tr style="">
                        <td colspan="2">
                            <p style="text-align:center;">
                                <small>
                                    Spells will only display here if the above switch is set to "On"
                                </small>
                            </p>

                        </td>

                    </tr>


                    </tbody>


                </table>
            </div>
        </div>

    </div>
</div>
<script>


    $(".room").tooltip();
    $("a").tooltip();
    $(document).ready(function () {
        $('#spoilLocations').change(function () {
            if (document.getElementById('spoilLocations').checked) {
                $("#spellList").removeClass("disabled-area");
                $(".spellentry").show();
            }
            else {
                $(".spellentry").hide();
                $("#spellList").addClass("disabled-area");
            }

        });
        $(".room").click(function () {

            $(".room").removeClass("selected");
            $(this).addClass("selected");
            $("#x").html($(this).data("x"));
            $("#y").html($(this).data("y"));
            $("#name").html($(this).data("name"));
            $("#region").html($(this).data("region"));
            $("#regionspell").html($(this).data("regionspell"));
            $("#localspell").html($(this).data("localspell"));
            $("#dbname").html($(this).data("dbname"));


            //I dedicate this shitshow to my good friend Satan for inventing Javascript.
            //api call stuff
            if (document.getElementById('spoilLocations').checked) {
                var location = $(this).data("dbname");
                var region = $(this).data("region");
                $.get("api/spellbyloc.php", {location: location})
                    .done(function (data) {
                        var obj = JSON.parse(data);
                        $(".spellentry").remove(); //clear it right before population to minimize emptiness time
                        for (var i = 0; i < obj.length; i++) {
                            $('tr:last').after('<tr class="spellentry spell-locational"><td colspan="2"><a href="wiki.php?type=spell&id=' + obj[i].Id + '">' + obj[i].FriendlyName + '</td></tr>');
                        }
                        $.get("api/spellbyloc.php", {region: region})
                            .done(function (data) {
                                var obj = JSON.parse(data);
                                for (var i = 0; i < obj.length; i++) {
                                    $('tr:last').after('<tr class="spellentry spell-regional"><td colspan="2"><a href="wiki.php?type=spell&id=' + obj[i].Id + '">' + obj[i].FriendlyName + '</td></tr>');
                                }
                            });


                    });
            }
        });

    });

</script>
<div class="well">
    <?php
    if (isset($_GET["heatmap"])) {
        echo '<small><a href="?">(disable density map)</a></small>';
    } elseif (!isset($_GET["heatmap"])) {
        echo '<small><a href="?heatmap">(enable density map)</a></small>';
    }
    ?>


    <p><h4>(<span id="x">0</span>,<span id="y">0</span>) <span id="name">null</span></h4></p>


    <p>Region: <span id="region">null</span></p>
    <p>Database Name: <span id="dbname">null</span></p>

    <p>Local spell count: <span id="localspell">0</span></p>
    <p>Regional spell count: <span id="regionspell">0</span></p>

    <small>
        <?php
        $mtime = explode(' ', microtime());
        $totaltime = $mtime[0] + $mtime[1] - $starttime;
        printf('Map loaded in %.3f seconds.', $totaltime);
        ?>
    </small>
</div>

</body>
</html>
