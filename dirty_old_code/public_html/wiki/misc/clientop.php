<?php
/**
 * Copyright (c) 2017., K.S. (kportalz@protonmail.com)
 *
 *
 */

// $is_api=1;
require_once("../global.php");
if (isset($_POST['a']) && isset($_POST['p'])) {
    $idstr = escape($_POST['a'], $link);
    $result = mysqli_query($link, "SELECT `TimeToHide`,`Cookie` FROM `notices` WHERE `IdStr`='$idstr'");
    if (mysqli_num_rows($result)) {
        $data = mysqli_fetch_row($result);
        if (!isset($_COOKIE[$data[1]])) {
            setcookie($data[1], 'true', time() + $data[0], '/');
            echo 'cookie set';
        } else {
            echo "cookie already set";
        }

    } else {
        echo 'invalid notification id';
        http_response_code(501);
    }

} else {
    echo 'invalid notification id';
    http_response_code(501);
}
