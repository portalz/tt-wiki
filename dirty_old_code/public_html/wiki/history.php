<?php
/**
 * Copyright (c) 2017.,  K.S. (kportalz@protonmail.com)
 *
 */

require_once('global.php');
if (isset($_GET['Id'])) {

    $pageid = escape($_GET['Id'], $link);
    $result = mysqli_query($link, "SELECT * FROM `wiki_history` WHERE `PageID`='$pageid'");


    while ($row = mysqli_fetch_array($result)) {
        echo "<h1>Page Edits</h1>";
        $id = $row['Id'];
        $time = $row['Timestamp'];
        $editedBy = $row['IPAddr'];
        $ipOctets = explode('.', $editedBy);
        $editedBy = $ipOctets[0] . '.' . $ipOctets[1] . '.' . preg_replace('/./', '*', $ipOctets[2]) . '.' . preg_replace('/./', '*', $ipOctets[3]);
        $editedBy = str_replace("..", ".***.", $editedBy);
        $changes = $row['PageContent'];


        echo "<hr>";
        echo noxss($_GET['Id']) . " - " . noxss($time) . " - " . noxss($editedBy) . "</b><br>";
        echo noxss($changes);

    }
}