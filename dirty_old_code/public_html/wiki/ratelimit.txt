SecRuleEngine On

        <LocationMatch "^/api">
                SecAction initcol:ip=%{REMOTE_ADDR},pass,nolog
                SecAction "phase:5,deprecatevar:ip.apicounter=1/1,pass,nolog"
                SecRule IP:APICOUNTER "@gt 3" "phase:2,pause:300,deny,status:509,setenv:RATELIMITED,skip:1,nolog"
                SecAction "phase:2,pass,setvar:ip.apicounter=+1,nolog"
                Header always set Retry-After "10" env=RATELIMITED
        </LocationMatch>

        ErrorDocument 509 "Rate Limit Exceeded"
