<?php
/**
 * Copyright (c) 2017.,  K.S. (kportalz@protonmail.com)
 *
 */

$title = "News";
$navpath = array("News");
require_once("global.php");
require_once("views/header.php");
?>
<div class="row">
    <div class="col-lg-8">
        <?php
        $result = mysqli_query($link, 'SELECT * FROM `news` WHERE 1 ORDER BY `TimeStamp` DESC');
        while ($row = mysqli_fetch_array($result)) {
            ?>
            <h1><?= $row['Title']; ?></h1>
            <p class="lead">
                by <?= noxss($row['Author']); ?>
            </p>
            <p><span class="glyphicon glyphicon-time"></span> Posted
                on <?= date('M j Y g:i A', strtotime($row["Timestamp"]));; ?></p>

            <?php
            $Parsedown = new Parsedown();
            echo $Parsedown->text(noxss($row['Content']));


            ?>

            <hr>
            <?php
        }
        ?>
    </div>
</div>


<?php require_once("views/footer.php"); ?>
