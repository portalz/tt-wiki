<?php
/**
 * Copyright (c) 2018., K.S. (kportalz@protonmail.com)
 */

require_once "global.php";
$title = "Sitemap";
$navpath = array($title);
require_once "views/header.php";
?>
    <ul>
        <li>
            <a href="/"><?= ROOT_NAME; ?></a>
            <ul>
                <li><a href="<?= BASE_DIR; ?>">TTWiki</a>
                    <ul>
                        <li><a href="<?= BASE_DIR; ?>/index.php">Index</a></li>
                        <li><a href="<?= BASE_DIR; ?>/browse.php">Browse</a>
                            <ul>
                                <li><a href="<?= BASE_DIR; ?>/browsetype.php?type=effect">Browse Effects</a></li>
                                <li><a href="<?= BASE_DIR; ?>/browsetype.php?type=form">Browse Forms</a></li>
                                <li><a href="<?= BASE_DIR; ?>/browsetype.php?type=item">Browse Items</a></li>
                                <li><a href="<?= BASE_DIR; ?>/browsetype.php?type=spell">Browse Spells</a></li>
                            </ul>
                        </li>
                        <li><a href="tables/">Tables</a>
                            <ul>
                                <li><a href="tables/itemtable.php">Item Table</a></li>
                                <li><a href="tables/formtable.php">Form Table</a></li>
                            </ul>
                        </li>
                        <li><a href="<?= BASE_DIR; ?>/news.php">News</a></li>
                        <li><a href="<?= BASE_DIR; ?>/stats.php">Content Statistics</a></li>
                        <li><a href="<?= BASE_DIR; ?>/spellmap.php">Spell Map (spoilers!)</a></li>
                    </ul>

                </li>
                <li>Documentation (Markdown)
                    <ul>
                        <li><a href="/docs/API.md">API Documentation</a></li>
                    </ul>
                </li>
            </ul>
        </li>
    </ul>


<?php
require_once "views/footer.php";