<?php
/**
 * Copyright (c) 2017., K.S. (kportalz@protonmail.com)
 *
 *
 */

require_once("global.php");
$title = "Browse";
$navpath = array('Browse');
include 'views/header.php';
include 'functions/search_functions.php';
include 'misc/search_form.php';

/* Items */
if (mysqli_num_rows($result = mysqli_query($link, "SELECT * FROM `active_items` WHERE `ItemType`!='pet' ORDER BY `FriendlyName` ASC"))) {
    echo "<h2 id=\"item-title\">Items (" . mysqli_num_rows($result) . " results)</h2>";
    echo '<dl id="items">';
    displayResults($result, "(Items)", "item");
    echo "</dl>";
}
/* Pets */
if (mysqli_num_rows($result = mysqli_query($link, "SELECT * FROM `active_items` WHERE `ItemType`='pet' ORDER BY `FriendlyName` ASC"))) {
    echo "<h2 id=\"pet-title\">Pets (" . mysqli_num_rows($result) . " results)</h2>";
    echo '<dl id="pets">';
    displayResults($result, "(Pets)", "item");
    echo "</dl>";
}
/* Forms */
if (mysqli_num_rows($result = mysqli_query($link, "SELECT * FROM `active_forms` WHERE `MobilityType`='full' ORDER BY `FriendlyName` ASC"))) {
    echo "<h2 id=\"form-title\">Forms (" . mysqli_num_rows($result) . " results)</h2>";
    echo '<dl id="forms">';
    displayResults($result, "(Forms)", "form");
    echo '</dl>';
}
/* Spells */
if (mysqli_num_rows($result = mysqli_query($link, "SELECT * FROM `active_spells` WHERE 1 ORDER BY `FriendlyName` ASC"))) {
    echo "<h2 id=\"spell-title\">Spells (" . mysqli_num_rows($result) . " results)</h2>";
    echo '<dl id="spells">';
    displayResults($result, "(Spells)", "spell");
    echo '</dl>';
}
/* Effects */
if (mysqli_num_rows($result = mysqli_query($link, "SELECT * FROM `active_effects` WHERE 1 ORDER BY `FriendlyName` ASC"))) {
    echo "<h2 id=\"effect-title\">Effects (" . mysqli_num_rows($result) . " results)</h2>";
    echo '<dl id="effects">';
    displayResults($result, "(Effects)", "effect");
    echo '</dl>';
}
/* Furniture */
if (mysqli_num_rows($result = mysqli_query($link, "SELECT * FROM `active_furniture` WHERE 1 ORDER BY `FriendlyName` ASC"))) {
    echo "<h2 id=\"furniture-title\">Furniture (" . mysqli_num_rows($result) . " results)</h2>";
    echo '<dl id="furniture">';
    displayResults($result, "(Furniture)", "furniture");
    echo '</dl>';
}

/* Custom */
if (mysqli_num_rows($result = mysqli_query($link, "SELECT * FROM `active_custom` WHERE 1 ORDER BY `Title` ASC"))) {
    echo "<h2 id=\"custom-title\">custom (" . mysqli_num_rows($result) . " results)</h2>";
    echo '<dl id="customs">';
    displayCustom($result, "(custom)", "custom");
    echo '</dl>';
}


?>
    <br><br><br><br>
<?php
require_once('views/footer.php');
