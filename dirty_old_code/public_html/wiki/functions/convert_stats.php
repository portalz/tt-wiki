<?php
/**
 * Copyright (c) 2017., K.S. (kportalz@protonmail.com)
 *
 *
 */

function convertStats($holo)
{
    $ha=$holo;
    $oldstats=array();
    $stats=array("HealthBonusPercent", "ManaBonusPercent", "ExtraSkillCriticalPercent", "HealthRecoveryPerUpdate", "ManaRecoveryPerUpdate", "SneakPercent", "AntiSneakPercent", "EvasionPercent", "EvasionNegationPercent","MeditationExtraMana","CleanseExtraHealth","MoveActionPointDiscount","SpellExtraTFEnergyPercent","SpellExtraHealthDamagePercent","CleanseExtraTFEnergyRemovalPercent","SpellMisfireChanceReduction","SpellHealthDamageResistance","SpellTFEnergyDamageResistance","ExtraInventorySpace","InstantHealthRestore","InstantManaRestore","ReuseableHealthRestore","ReuseableManaRestore");
    $oldstats["HealthBonusPercent"] = ($ha['Discipline']*(.25)) + ($ha['Fortitude']*(.2));
    $oldstats["ManaBonusPercent"] = $ha['Magicka']*1.5;
    $oldstats["ExtraSkillCriticalPercent"] = $ha['Luck'] * .21;
    $oldstats["HealthRecoveryPerUpdate"] = $ha['Allure']*.2;
    $oldstats["ManaRecoveryPerUpdate"] = $ha['Allure']*.2;
    $oldstats["SneakPercent"] = $ha['Agility'] * .75;
    $oldstats["AntiSneakPercent"] = $ha['Perception'] * 1;
    $oldstats["EvasionPercent"] = $ha['Agility'] * .5;
    $oldstats["EvasionNegationPercent"] = $ha['Perception']*.8;
    $oldstats["MeditationExtraMana"] = $ha['Succour']*.1;
    $oldstats["CleanseExtraHealth"] = $ha['Succour']*.1;
    $oldstats["MoveActionPointDiscount"] = $ha['Agility']*0.0025;
    $oldstats["SpellExtraTFEnergyPercent"] = ($ha['Charisma'] *.2) + ($ha['Magicka'] *.5);
    $oldstats["SpellExtraHealthDamagePercent"] = ($ha['Charisma'] * .75);
    $oldstats["CleanseExtraTFEnergyRemovalPercent"] = $ha['Allure'] * .02;
    $oldstats["SpellMisfireChanceReduction"] = ($ha['Perception']*.1)+($ha["Luck"] *.05);
    $oldstats["SpellHealthDamageResistance"] = ($ha['Discipline']*.5);
    $oldstats["SpellTFEnergyDamageResistance"] = ($ha['Discipline']*.5);
    $oldstats["ExtraInventorySpace"] = $ha['Fortitude'] * .065;
    if(isset($stats['ReusableHealthRestore']))
    {
        $oldstats["ReuseableHealthRestore"]=$stats['ReuseableHealthRestore'];
    }
    else
    {
        $oldstats["ReusableHealthRestore"]=0;

    }
    if(isset($stats['ReuseableManaRestore']))
    {
        $oldstats["ReuseableManaRestore"]=$stats['ReuseableManaRestore'];
    }
    else
    {
        $oldstats["ReusableManaRestore"]=0;

    }
    if(isset($stats['InstantHealthRestore']))
    {
        $oldstats["InstantHealthRestore"]=$stats['InstantHealthRestore'];
    }
    else
    {
        $oldstats["InstantHealthRestore"]=0;
    }
    if(isset($stats['InstantManaRestore']))
    {
        $oldstats["InstantManaRestore"]=$stats['InstantManaRestore'];
    }
    else
    {
        $oldstats["InstantManaRestore"]=0;
    }
    return $oldstats;
}