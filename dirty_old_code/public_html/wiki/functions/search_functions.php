<?php
define("MAX_LEN", 170);
function displayCustom($results, $text, $type)
{

    while ($row = mysqli_fetch_array($results, MYSQLI_ASSOC)) {
        $id = $row['Id'];
        $name = $row['Title'];
        $desc = $row['PageContent'];

        echo "<p><dt>" . "<a  href=\"wiki.php?type=custom&id=" . noxss($id) . "\">" . noxss($name) . " </a></dt>\n";
        echo "<dd>" . tokenTruncate(noxss($desc), MAX_LEN) . "...</dd></p>";
    }
}

function displayResults($results, $text, $type)
{

    while ($row = mysqli_fetch_array($results)) {
        $id = $row['Id'];
        $name = $row['FriendlyName'];
        $name = html_entity_decode($name);
        $desc = $row['Description'];
        $colors = array("default", "primary", "success", "info", "warning");
        $tags = array();
        switch ($type) {
            case "item":
                $tags = addTag($row['ItemType'], $tags, "secondary");
                if ($row['CurseTFFormdbName']) {
                    $tags = addTag("TF Curse", $tags, "primary");
                }
                if ($row['IsUnique']) {
                    $tags = addTag('IsUnique', $tags, "success");
                }
                break;
            case "form";
                if ($row['Gender'] == "male") {
                    $tags = addTag('Male', $tags, "blue");
                } elseif ($row['Gender'] == "female") {
                    $tags = addTag('Female', $tags, "purple");
                }
                if ($row['MoveActionPointDiscount'] == -999) {
                    $tags = addTag('Immobile', $tags, "danger");
                }
                break;
            case "spell":
                switch ($row['MobilityType']) {
                    case "full":
                        $tags = addTag('Animate', $tags, "success");
                        break;
                    case "animal":
                        $tags = addTag('Animal', $tags, "warning");
                        break;
                    case "mindcontrol":
                        $tags = addTag('Mind Control', $tags, "purple");
                        break;
                    case "inanimate":
                        $tags = addTag('Inanimate', $tags, "danger");
                        break;
                    case "curse":
                        $tags = addTag('Curse', $tags, "purple");
                        break;
                }
                if ($row['ExclusiveToForm']) {
                    $tags = addTag('Form Exclusive', $tags, "info");
                }
                if ($row['ExclusiveToItem']) {
                    $tags = addTag('Item Exclusive', $tags, "info");
                }
                break;
            case "effect":
                if ($row['isLevelUpPerk']) {
                    $tags = addTag('Level-Up Perk', $tags, 'success');
                } elseif ($row['ObtainedAtLocation']) {
                    $tags = addTag('Random Search', $tags, 'secondary');
                } else {
                    $tags = addTag('Probably comes from a spell, form, item or something', $tags, 'purple');
                }
                break;
        }
        echo "<p class=\"result\"><dt>" . "<a  href=\"wiki.php?type=" . noxss($type) . "&id=" . noxss($row['Id']) . "\">" . noxss($row['FriendlyName']) . " <small>" . noxss($text) . "</small></a></dt>\n";
        echo "<dd>" . tokenTruncate(noxss($row['Description']), MAX_LEN) . "...</dd>";
        if ($tags) {
            displayTags($tags);
        }

        echo "</p>";

    }
}

function tokenTruncate($string, $your_desired_width)
{
    $parts = preg_split('/([\s\n\r]+)/', $string, null, PREG_SPLIT_DELIM_CAPTURE);
    $parts_count = count($parts);

    $length = 0;
    $last_part = 0;
    for (; $last_part < $parts_count; ++$last_part) {
        $length += strlen($parts[$last_part]);
        if ($length > $your_desired_width) {
            break;
        }
    }

    return implode(array_slice($parts, 0, $last_part));
}

function displayTags($tags)
{
    echo "Tags: ";
    foreach ($tags as $tag) {
        $label = "label-info";
        echo '<span class="badge label-tag badge-' . $tag[1] . '">' . $tag[0] . '</span> ';
    }
}

function addTag($option, $tags, $color)
{
    array_push($tags, array($option, $color));
    return $tags;
}
