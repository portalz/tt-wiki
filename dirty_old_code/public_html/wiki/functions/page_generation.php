<?php
/**
 * Copyright (c) 2017.,  K.S. (kportalz@protonmail.com)
 *
 */
/**
 * This file contains miscellaneous functions used in the generation of pages that contain content.
 */

function getExpandedStats($itemdata, $callType)
{
    $comp = array();
    $comp['Discipline'] = $itemdata["Discipline"];
    $comp['Perception'] = $itemdata["Perception"];
    $comp['Charisma'] = $itemdata["Charisma"];
    $comp['Fortitude'] = $itemdata["Fortitude"];
    $comp['Agility'] = $itemdata["Agility"];
    $comp['Allure'] = $itemdata["Allure"];
    $comp['Magicka'] = $itemdata["Magicka"];
    $comp['Succour'] = $itemdata["Succour"];
    $comp['Luck'] = $itemdata["Luck"];
    if ($callType == "item") {
        $comp['ReuseableHealthRestore'] = $itemdata["ReuseableHealthRestore"];
        $comp['ReuseableManaRestore'] = $itemdata["ReuseableManaRestore"];
        $comp['InstantManaRestore'] = $itemdata["InstantManaRestore"];
        $comp['InstantHealthRestore'] = $itemdata["InstantHealthRestore"];
    }

    return convertStats($comp);
}

function expandedStatsTable($expanded, $itemdata, $callType)
{
    $stats = array("HealthBonusPercent", "ManaBonusPercent", "ExtraSkillCriticalPercent", "HealthRecoveryPerUpdate", "ManaRecoveryPerUpdate", "SneakPercent", "AntiSneakPercent", "EvasionPercent", "EvasionNegationPercent", "MeditationExtraMana", "CleanseExtraHealth", "MoveActionPointDiscount", "SpellExtraTFEnergyPercent", "SpellExtraHealthDamagePercent", "CleanseExtraTFEnergyRemovalPercent", "SpellMisfireChanceReduction", "SpellHealthDamageResistance", "SpellTFEnergyDamageResistance", "ExtraInventorySpace");
    echo "<table class=\"table table-bordered\"><thead><tr><td>Stat</td><td>Value</td></tr></thead><tbody>";
    foreach ($stats as $stat) {
        if ($expanded["$stat"] != 0) {
            if ($expanded["$stat"] < 0) {
                $class = "table-danger";
            } else if ($expanded["$stat"] > 0) {
                $class = "table-success";
            } else {
                $class = "";
            }
            //echo "<tr class=\"$class\"><td>$stat</td><td>" . $expanded["$stat"] . "</td></tr>";
            echo '<tr id="stat-' . $stat . '" data-defval="' . $expanded["$stat"] . '" class="' . $class . '"><td >' . $stat . '</td><td id="statcol-' . $stat . '">' . $expanded["$stat"] . '</td></tr>';

        }
    }

    //this is a workaround to display special stats such as reusable and instant restores
    if ($callType == "item") //only do this for items, because only items use these stats fields
    {
        $specstats = array('InstantHealthRestore', 'InstantManaRestore', 'ReuseableHealthRestore', 'ReuseableManaRestore');
        foreach ($specstats as $specstat) {
            if ($itemdata["$specstat"] != 0) {
                if ($itemdata["$specstat"] < 0) {
                    $class = "table-danger";
                }
                if ($itemdata["$specstat"] > 0) {
                    $class = "table-success";
                }
                echo "<tr class=\"$class\"><td>$specstat</td><td>" . $itemdata["$specstat"] . "</td></tr>";
                //echo '<tr id="stat-' . $specstat . '" data-defval="' . $itemdata["$specstat"] . '" class="' . $class . '"><td >' . $specstat . '</td><td id="statcol-' . $specstat . '">' . $itemdata["$specstat"] . '</td></tr>';


            }
        }
    }
    echo "</tbody></table><p>Notice: The expanded stats may not be completely accurate and are subject to change.</p>";
}

function compStatsTable($comp, $itemdata)
{
    echo '<p>Level: <input id="formlevelinput" type="number" min="1" value="1"/></p>';
    echo "<table class=\"table table-bordered\"><thead><tr><td>Stat</td><td>Value</td></tr></thead><tbody>";
    foreach ($comp as $stat) {
        if ($itemdata["$stat"] != 0) {
            echo "";
            if ($itemdata["$stat"] > 0) {
                $class = "table-success";
            }
            if ($itemdata["$stat"] < 0) {
                $class = "table-danger";
            }

            //allure/succour to restoration/regeneration
            if ($stat == "Allure") {
                $stat_disp = "Restoration";
            } elseif ($stat == "Succour") {
                $stat_disp = "Regeneration";
            } else {
                $stat_disp = $stat;
            }

            echo '<tr id="stat-' . $stat . '" data-defval="' . $itemdata["$stat"] . '" class="' . $class . '"><td >' . $stat_disp . '</td><td id="statcol-' . $stat . '">' . $itemdata["$stat"] . '</td></tr>';
        }
    }
    echo "</tbody></table>";
}

function file_ext_strip($filename)
{
    return preg_replace('/.[^.]*$/', '', $filename);
}

function displayImage($imurl, $conn)
{
    $result = mysqli_query($conn, "SELECT * FROM `image_relations` INNER JOIN `artist_details` ON `image_relations`.`Artist`=`artist_details`.`Id` WHERE `image_relations`.`FileName`='$imurl'");
    if (mysqli_num_rows($result)) {
        $imgres = mysqli_fetch_array($result, MYSQLI_ASSOC);
        echo '<p class="text-center"><img src="' . IMGCDN . noxss($imgres["FileName"]) . '" class="img-thumbnail pix" alt="TTImage" height="200" width="200" data-pixelate></p><p class="text-center">Graphic by <a href="' . noxss($imgres["ArtistPage"]) . '">' . noxss($imgres["FriendlyName"]) . '</a></p><hr>';
    } else {
        echo '<p class="text-center">
				<img src="' . IMGCDN . "404.png" . '" class="img-thumbnail" height="200" width="200" alt="The artist of this graphic has either not given permission to display their art, or we haven\'t gotten to it yet.">
			</p>
			<p>
				<small>We couldn\'t give you the image, so here\'s something else. </small>
			</p>
			<p>
				<small>"Sorry" by Nyx.</small>
			</p>
			<hr>
			';
    }
}

//FUNCTIONS THAT DO THINGS FOR DATA AND STUFF
//THE NAMES ARE EXACTLY WhAT THEY DO

//prints a list of spells in a region from an array
function printSpellArrayAsList($array)
{

    if (@($array[0] != null)) //hide spells with no locations
    {
        echo '<strong>Spell Location: </strong>';
        echo '<div id="spellloc" class="spoiler hidden-spoiler">';
        echo '<ul id="paged_list" >';
        $c = 1;
        foreach ($array as $key) {
            if ($c <= 5) {
                echo '<li>' . noxss($key[0]) . "</li>";
            } else {
                // echo '<li class="hidden-list-item">'.noxss($key[0])."</li>";
            }
            $c++;
        }
        if ($c > 5) {
            $remaining = $c - 6;
            echo '<li class="showmore"><em>and ' . $remaining . ' more in this region</em></li>';
        }
        echo "</ul>";
        // echo '<div class="pagination pagination-large">
        //           <ul class="pager"></ul>
        //       </div>';
        echo '</div>';
    } else {


    }
}


function getSpellFromForm($dbName)
{
    global $link;
    $query = "SELECT `FriendlyName`,`Id`,`LearnedAtLocation`,`LearnedAtRegion` FROM `active_spells` WHERE `FormdbName`='" . escape(stripslashes($dbName), $link) . "'";
    return mysqli_fetch_array(mysqli_query($link, $query), MYSQLI_ASSOC);
}

function getLocationFromSpell($linked_spell)
{
    $location = array();
    global $link;
    if ($linked_spell["LearnedAtRegion"] != "") //3 is the column for regional spells
    {
        $temp = escape($linked_spell["LearnedAtRegion"], $link);
        $result = mysqli_query($link, "SELECT `Name` FROM `active_locations` WHERE `Region`='$temp'");
        while ($row = mysqli_fetch_array($result)) {
            $location[] = $row;
        }
    } else {
        $temp = escape($linked_spell["LearnedAtLocation"], $link);
        $location[] = mysqli_fetch_row(mysqli_query($link, "SELECT `Name` FROM `active_locations` WHERE `dbName`='$temp'"));
    }
    return $location;
}

function getMobilityTypeFromSpell($tempdata)
{
    switch ($tempdata) {
        case "weaken":
            return "Other";
            break;
        case "inanimate":
            return "Inanimate";
            break;
        case "full":
            return "Animate";
            break;
        case "curse":
            return "Curse";
            break;
        case "animal":
            return "Pet";
            break;
        default:
            return "Unknown. This is an error";
            break;
    }
}

function getFormLinkFromDbName($tempdata)
{
    global $link;
    return mysqli_fetch_row(mysqli_query($link, "SELECT `FriendlyName`,`Id` FROM `active_forms` WHERE `dbName`='" . escape($tempdata, $link) . "'"));
}
