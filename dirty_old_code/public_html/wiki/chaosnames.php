<?php
/**
 * Copyright (c) 2017., K.S. (kportalz@protonmail.com)
 *
 *
 */

require_once("global.php");

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>TT Wiki - Chaos Names</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="//cdn.portalz.xyz/js/jquery-ui.min.css">
    <link rel="shortcut icon" sizes="16x16" href="//portalz.xyz/resc/favicon.ico">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
    <script src="//cdn.portalz.xyz/js/jquery-ui.min.js"></script>
    <script src="//cdn.portalz.xyz/js/sortable.js"></script>
    <!-- inspired by breena -->
</head>

<body>
<div class="container">
    <table class="table table-striped table-bordered sortable">
        <tr>
            <th>Round</th>
            <th>Original Name</th>
            <th>Chaos Name</th>
        </tr>
        <?php
        $result = mysqli_query($link, "SELECT * FROM `chaos_names` WHERE 1");
        while ($row = mysqli_fetch_array($result)) {
            echo '<tr>' . '<td>' . noxss($row["Round"]) . '</td><td>' . noxss($row["OriginalName"]) . '</td><td>' . noxss($row["ChaosName"]) . '</td></tr>';
        }
        ?>
    </table>
</div>
</body>
</html>