<!-- Description -->
<div id="desc">
    <p><span class="lead">Description</span></p>
    <p>Third Person</p>
    <blockquote>
        <p>
            <em>
                <small><?= noxss($data['Description']); ?></small>
            </em>
        </p>
    </blockquote>
    <?php
    if ($data["IsUnique"] == null) {
        ?>
        <p>First Person </p>
        <blockquote>
            <em>
                <small>
                    <?= noxss(mysqli_fetch_row(mysqli_query($link, "SELECT `Description` FROM `active_forms` WHERE `BecomesItemDbName`='" . escape($data["dbName"], $link) . "'"))[0]); ?>
                </small>
            </em>
        </blockquote>
        <?php
    }
    ?>
</div>
<hr>
<!-- Composite Stats Table-->
<div id="comp">
    <p>
        <span class="lead">Composite Stats</span>
    </p>
    <?php
    compStatsTable($compstats, $data);
    ?>
</div>
<hr>
<!-- Expanded Stats Table -->
<div id="noncomp">
    <p>
        <span class="lead">Expanded Stats</span>
    </p>
    <?php
    expandedStatsTable(getExpandedStats($data, $type), $data, $type);
    ?>
</div>