<?php
//furniture has a database column for an image, however none of them have an entry in the field.
displayImage(escape($data["PortraitUrl"], $link), $link);
?>
<p><strong>Base Cost: </strong>⩜<?= $data["BaseCost"]; ?></p>
<p><strong>Base Contract
        Length: </strong><?= $data["BaseContractTurnLength"] . " " . ($data["BaseContractTurnLength"] != 1 ? ' turns' : ' turn') ?>
</p>
<p><strong>Usage
        Cooldown: </strong><?= $data["MinutesUntilReuse"] . " " . ($data["MinutesUntilReuse"] != 1 ? ' minutes' : ' minute') ?>
</p>
<?php
if ($data["GivesEffect"] != null) {
    echo '<p><strong>Gives Effect: </strong><a href="wiki.php?type=effect&id=' . crossQuery("Id", "active_effects", "`dbName`='" . escape($data["GivesEffect"], $link) . "'", $link) . '">' . crossQuery("FriendlyName", "active_effects", "`dbName`='" . escape($data["GivesEffect"], $link) . "'", $link) . '</a></p>';
}
if ($data["GivesItem"] != null) {
    echo '<p><strong>Gives Item: </strong><a href="wiki.php?type=item&id=' . crossQuery("Id", "active_items", "`dbName`='" . escape($data["GivesItem"], $link) . "'", $link) . '">' . crossQuery("FriendlyName", "active_items", "`dbName`='" . escape($data["GivesItem"], $link) . "'", $link) . '</a></p>';
}

if ($data["APReserveRefillAmount"]) {
    echo '<p><strong>AP Reserve Refill: </strong>' . $data["APReserveRefillAmount"] . '</p>';
}
?>
