<hr>
<div id="entry-dbinfo">
    <p><strong>Debug Information</strong></p>
    <?php
    if (isset($data["dbName"])) {
        echo '<p><abbr title="Database Name, the author\'s name is likely to be found here.">DBN:</abbr> <samp>' . noxss($data["dbName"]) . '</samp></p>';
    } elseif (isset($data["dbType"])) {
        // furniture table uses dbType instead of dbName for some reason.
        echo '<p><abbr title="Database Name, the author\'s name is likely to be found here.">DBN:</abbr> <samp>' . noxss($data["dbType"]) . '</samp></p>';
    }


    if (isset($data["Id"])) {
        echo '<p><abbr title="ID">NUMID:</abbr> <kbd>' . noxss($data["Id"]) . '</kbd></p>';
    }
    if (isset($data["PortraitUrl"]) && $data["PortraitUrl"] != null) {
        echo '<p><abbr title="Image URL">IMGURL:</abbr> <samp>' . noxss($data["PortraitUrl"]) . '</samp></p>';
    }
    ?>
</div>
