<hr>
<footer>
    <div class="text-center">
        <p class="small">
            This site is not maintained by the creators or developers of <i>Transformania Time</i>, and thus is not
            officially supported.
            <br>
            There is no guarantee on the accuracy of the data used by this site.
            <br>
            The content from which this site's database is generated is based on content used by the game <i>Transformania
                Time</i>, and is not owned, nor do we claim ownership of the content of this site.
            <br>
            If there are any issues or bugs with this site, please contact Kevin Portalz at <u><a
                        href="mailto:KPortalz@protonmail.com">kportalz@protonmail.com</a></u> or <u><a
                        href="https://www.transformaniatime.com/PvP/LookAtPlayer_FromMembershipId/2047">in-game</a></u>
        </p>
        <p class="debug" style="color:#C0C0C0;">
            <small>
                <?php
                $mtime = explode(' ', microtime());
                $totaltime = $mtime[0] + $mtime[1] - $starttime;
                echo 'Page generated in ' . $totaltime . ' seconds by ' . gethostname() .' @ ['.explode(".", $_SERVER['SERVER_ADDR'])[3].']';
                ?>
            </small>
        </p>

    </div>
</footer>
</div>
</body>
</html>