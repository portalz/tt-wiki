<?php
/**
 * Copyright (c) 2017.,  K.S. (kportalz@protonmail.com)
 *
 */
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?= noxss($title).' - '.SITE_NAME;?></title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
    <link rel="stylesheet" href="<?= BASE_DIR; ?>/css/wiki-custom.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <script src="<?= BASE_DIR; ?>/js/imageblur.js"></script>
    <script src="<?= BASE_DIR; ?>/js/spoiler.js"></script>
    <script src="<?= BASE_DIR; ?>/js/statscale.js"></script>
    <script src="<?= BASE_DIR; ?>/js/notices.js"></script>
    <script>
        if ($("img").length) //don't blur if no images
        {
            document.querySelector('img').pixelate(); // fall back if jQuery doesn't load.
            $('img').pixelate();
        }
    </script>
</head>
<body>
<div class="container">
    <div class="page-header">
        <h1><a href="/wiki"><?=SITE_NAME;?></a>
            <small><?php echo noxss($title); ?></small>
        </h1>
    </div>
    <nav aria-label="breadcrumb" role="navigation">
        <ol class="breadcrumb">
            <?php
                displayBreadcrumbs($navpath);
            ?>
        </ol>
    </nav>
    <?php
    displayNotices();
    ?>
