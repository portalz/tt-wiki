<?php
displayImage(escape($data["PortraitUrl"], $link), $link);
?>
<p>
    <strong>Gender: </strong>
    <?= noxss($data["Gender"]); ?>
</p>
<p>
    <?php $linked_spell = getSpellFromForm($data["dbName"]); ?>
    <strong>Associated Spell: </strong>
    <a href="wiki.php?type=spell&id=<?= $linked_spell["Id"]; ?>">
        <?= $linked_spell["FriendlyName"]; ?>
    </a>
</p>
<p>

</p>
<?php
//Spell Location
printSpellArrayAsList(getLocationFromSpell(getSpellFromForm($data["dbName"])));
?>


<?php
/* Check if there are any form-specific spells or curses */
$formname = $data["dbName"];
$result = mysqli_query($link, "SELECT `FriendlyName`,`Id` FROM `active_spells` WHERE `ExclusiveToForm`='".escape($formname,$link)."'");
if (mysqli_num_rows($result)) {
    echo '<p><strong>Form Exclusive Spells & Curses:</strong></p><ul>';
    while ($row = mysqli_fetch_array($result)) {
        echo '<li><a href="wiki.php?type=spell&amp;id=' . noxss($row['Id']) . '">' . noxss($row['FriendlyName']) . '</a></li>';
    }
    echo '</ul>';
}
?>
<hr>
