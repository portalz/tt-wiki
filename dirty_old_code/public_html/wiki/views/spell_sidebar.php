<p><strong>Spell: </strong>
    <?= noxss($data["FriendlyName"]); ?>
</p>
<p><strong>Type: </strong>
    <?= noxss(getMobilityTypeFromSpell($data["MobilityType"])); ?>
</p>
<?php
//this gets to be inside an if statement and echo since the whole thing conditional
if ($data["FormdbName"]) {
    $result = getFormLinkFromDbName($data["FormdbName"]);
    echo "<p> <strong>Form/Item: </strong> <a href=\"wiki.php?type=form&amp;id=" . noxss($result[1]) . "\">" . noxss($result[0]) . "</a> </p>";
    unset($result);
}

//Exclusive-to control structure
if ($data["ExclusiveToForm"]) {
    $result = mysqli_fetch_row(mysqli_query($link, "SELECT `FriendlyName`,`Id` FROM `active_forms` WHERE `dbName`='" . escape($data["ExclusiveToForm"], $link) . "'"));
    echo "<p> <strong>Exclusive to Form: </strong> <a href=\"wiki.php?type=form&amp;id=" . noxss($result[1]) . "\">" . noxss($result[0]) . "</a> </p>";
    unset($result);
} elseif ($data["ExclusiveToItem"]) {
    $result = mysqli_fetch_row(mysqli_query($link,"SELECT `FriendlyName`, `Id` FROM `active_forms` WHERE `BecomesItemDbName`='".escape($data["ExclusiveToItem"], $link)."'"));
    echo "<p> <strong>Exclusive to Item: </strong> <a href=\"wiki.php?type=form&amp;id=" . noxss($result[1]) . "\">" . noxss($result[0]) . "</a> </p>";
    unset($result);
} else {
    //Spell Location
    printSpellArrayAsList(getLocationFromSpell($data));
}

if ($data["GivesEffect"]) {
    $result = mysqli_fetch_row(mysqli_query($link, "SELECT `FriendlyName`, `Id` FROM `active_effects` WHERE `dbName`='" . escape($data["GivesEffect"], $link) . "'"));
    echo "<p><strong>Gives Effect: </strong><a href=\"wiki.php?type=effect&amp;id=" . noxss($result[1]) . "\">" . noxss($result[0]) . "</a></p>";
}
?>
<hr>
