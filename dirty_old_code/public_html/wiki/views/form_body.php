<!-- Description -->
<div id="desc">
    <p>
        <span class="lead">Description</span>
    </p>
    <blockquote>
        <p>
            <em>
                <small><?= noxss($data['Description']); ?></small>
            </em>
        </p>
    </blockquote>
</div>
<hr>
<!-- Composite Stats Table -->
<div id="comp">
    <p>
        <span class="lead">Composite Stats</span>
    </p>
    <?php compStatsTable($compstats, $data); ?>
</div>

<!-- Expanded Stats Table -->
<div id="noncomp">
    <p>
        <span class="lead">Expanded Stats</span>
    </p>
    <?php
    expandedStatsTable(getExpandedStats($data, $type), $data, $type);
    ?>
</div>
   