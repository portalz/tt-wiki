<!-- Description -->
<div id="spell-desc">
    <p>
        <span class="lead">Description</span>
    </p>
    <blockquote>
        <em>
            <small><?= noxss($data['Description']); ?>"</small>
        </em>
    </blockquote>
</div>
<!-- Discovery Message -->
<div id="desc-disc">
    <p>
        <span class="lead">Discovery Message</span>
    </p>
    <blockquote>
        <em>
            <small><?= noxss($data['DiscoveryMessage']); ?></small>
        </em>
    </blockquote>
</div>
<?php
//Spell Effect stats if available
if ($data["GivesEffect"]) {
    $effect_data = mysqli_fetch_array(mysqli_query($link, "SELECT * FROM `active_effects` WHERE `dbName`='" . escape($data["GivesEffect"], $link) . "'"), MYSQLI_ASSOC);
    ?>
    <!-- Composite Stats Table -->
    <div id="comp">
        <p>
            <span class="lead">Composite Stats</span>
        </p>
        <?php
        compStatsTable($compstats, $effect_data);
        ?>
    </div>
    <hr>
    <!-- Expanded Stats Table -->
    <div id="noncomp">
        <p>
            <span class="lead">Expanded Stats</span>
        </p>
        <?php
        //we need to prepare the data for conversion by a weird thing that only accepts JSON
        expandedStatsTable(getExpandedStats($effect_data, $type), $data, $type);
        ?>
    </div>
    <?php
}
?>