<?php
displayImage(escape($data["PortraitUrl"], $link), $link);

//displays money value if available
if ($data["MoneyValue"] != 0) {
    echo "<p><strong>Base Purchase Price: </strong><abbr title=\"Arpeyjis\">⩜</abbr>" . noxss($data["MoneyValue"]) . "</p>";
}
if ($data["MoneyValueSell"] != 0) {
    echo "<p><strong>Base Sell Price: </strong><abbr title=\"Arpeyjis\">⩜</abbr>" . noxss($data["MoneyValueSell"]) . "</p>";
}

echo "<p><strong>Item Type: </strong>" . noxss($data["ItemType"]) . "</p>";

if ($data["ItemType"] == "rune") {
    echo "<p><small>This item is a rune and cannot be equipped</small></p>";
}

if ($data["GivesEffect"] != null) {
    echo '<p><strong>Gives Effect: </strong><a href="wiki.php?type=effect&id=' . crossQuery("Id", "active_effects", "`dbName`='" . escape($data["GivesEffect"], $link) . "'", $link) . '">' . crossQuery("FriendlyName", "active_effects", "`dbName`='" . escape($data["GivesEffect"], $link) . "'", $link) . '</a></p>';
}
//Queryable dbname
$dbName = escape($data["dbName"], $link);
if (($formdbname = escape(mysqli_fetch_row(mysqli_query($link, "SELECT `dbName` FROM `active_forms` WHERE `BecomesItemDbName`='$dbName'"))[0], $link)) != null) {
    if ($data["ItemType"] != "consumable") //since consumable items don't have associated spells (thus far), we'll use that to determine if it has a spell
    {
        //Spell Location
        $spellFromForm = getSpellFromForm($formdbname);
        echo '<p><strong>Spell: </strong><a href="wiki.php?type=spell&id=' . $spellFromForm["Id"] . '">' . noxss($spellFromForm["FriendlyName"]) . '</a></p>';
        printSpellArrayAsList(getLocationFromSpell($spellFromForm));
        if ($data["CurseTFFormdbName"] != null) {
            $formName = mysqli_fetch_array(mysqli_query($link, "SELECT `Id`,`FriendlyName` FROM `active_forms` WHERE `dbName`='" . $data["CurseTFFormdbName"] . "'"), MYSQLI_ASSOC);
            echo "<p><strong>Transformation Curse: </strong><a href=\"wiki.php?type=form&amp;id=" . noxss($formName["Id"]) . "\">" . noxss($formName["FriendlyName"]) . "</a></p>";
        }
    } else {
        echo "This item is obtained via random search";
        echo "</p>";
    }
}

/* List Spells & Curses that are available to someone who possesses the item */
$result = mysqli_query($link, "SELECT `FriendlyName`,`Id` FROM `active_spells` WHERE `ExclusiveToItem`='$dbName'");
if (mysqli_num_rows($result)) {
    echo '<p><strong>Item Exclusive Spells & Curses:</strong></p><ul>';
    while ($row = mysqli_fetch_array($result)) {
        echo '<li><a href="wiki.php?type=spell&amp;id=' . noxss($row['Id']) . '">' . noxss($row['FriendlyName']) . '</a></li>';
    }
    echo '</ul>';
}
