<?php
/**
 * Copyright (c) 2017.,  K.S. (kportalz@protonmail.com)
 *
 */

require_once("global.php");

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>TT Wiki - Quotes</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="//cdn.portalz.xyz/js/jquery-ui.min.css">
    <link rel="shortcut icon" sizes="16x16" href="//portalz.xyz/resc/favicon.ico">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
    <script src="//cdn.portalz.xyz/js/jquery-ui.min.js"></script>
    <style>
        blockquote {
            background: #f9f9f9;
            border-left: 10px solid #ccc;

            quotes: "\201C" "\201D";
        }

        blockquote p {
            display: inline;
        }


    </style>
</head>

<body>
<div class="container">

    <?php
    $result = mysqli_query($link, "SELECT `quote`, `time` FROM `quotes` WHERE 1");
    while ($row = mysqli_fetch_array($result)) {
        ?>
        <div class="quote">
            <blockquote>
                <br>
                <?= nl2br(str_replace('\r\n', '<br>', noxss($row['quote']))); ?>
                <br>
                <p><b> &mdash; TT Global Chat, <?= noxss($row['time']); ?></b></p>
            </blockquote>
        </div>
        <?php
    } ?>
</div>
</body>
</html>