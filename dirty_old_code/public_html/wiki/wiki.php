<?php
/**
 * Copyright (c) 2017.,  K.S. (kportalz@protonmail.com)
 *
 */

require_once("global.php");
require_once("functions/convert_stats.php");
require_once("functions/page_generation.php");
if (!isset($_GET['id']) or !is_numeric($_GET['id']) or !isset($_GET['type'])) {
    fuckOff("404");
} //if any conditions are not met, kill it with fire.
$input = array();
$input["id"] = escape(preg_replace('/\D/', '', $_GET['id']),$link); //make sure that the id has nothing that's not a number_format
$type = $_GET['type'];
switch ($type) {
    //https://i.imgur.com/fFR5ilj.gif
    //This is no nonsense code.
    case "item":
        $stmt = $link->prepare("SELECT * FROM `active_items` WHERE `Id`=?");
        $stmt->bind_param("i", $input["id"]);
        $stmt->execute();
        $result = $stmt->get_result();
        if (!$result->num_rows) { //if there are no rows, it probably doesn't exist.
            fuckOff("404");
        } else {
            $data = $result->fetch_assoc();
        }
        $title = $data['FriendlyName'];
        //The true/false operator in the following string array is part of a very hacky solution to give the illusion of items and pets being in different tables
        $navpath = array('<a href="browsetype.php?type=' . ($data["ItemType"] == 'pet' ? 'pet' : 'item') . '">' . ($data["ItemType"] == 'pet' ? 'Pets' : 'Items') . '</a>', noxss($data['FriendlyName']));
        break;
    case "form":
        $stmt = $link->prepare("SELECT * FROM `active_forms` WHERE `Id`=?");
        $stmt->bind_param("i", $input["id"]);
        $stmt->execute();
        $result = $stmt->get_result();
        if (!$result->num_rows) {
            fuckOff("404");
        } else {
            $data = $result->fetch_assoc();
        }
        $title = $data['FriendlyName'];
        $navpath = array('<a href="browsetype.php?type=form">Forms</a>', noxss($data['FriendlyName']));
        //Oops. The user got to the form page for an item. It happens, we'll send them to the right place.
        if ($data["BecomesItemDbName"] != null) {
            rescueUser($data["BecomesItemDbName"]);
        }
        break;
    case "spell":
        $stmt = $link->prepare("SELECT * FROM `active_spells` WHERE `Id`=?");
        $stmt->bind_param("i", $input["id"]);
        $stmt->execute();
        $result = $stmt->get_result();
        if (!$result->num_rows) {
            fuckOff("404");
        } else {
            $data = $result->fetch_assoc();
        }
        $title = $data['FriendlyName'];
        $navpath = array('<a href="browsetype.php?type=spell">Spells</a>', noxss($data['FriendlyName']));
        break;
    case "effect":
        $stmt = $link->prepare("SELECT * FROM `active_effects` WHERE `Id`=?");
        $stmt->bind_param("i", $input["id"]);
        $stmt->execute();
        $result = $stmt->get_result();
        if (!$result->num_rows) {
            fuckOff("404");
        } else {
            $data = $result->fetch_assoc();
        }
        $title = $data['FriendlyName'];
        $navpath = array('<a href="browsetype.php?type=effect">Effects</a>', noxss($data['FriendlyName']));
        break;
    case "custom":
        $stmt = $link->prepare("SELECT * FROM `active_custom` WHERE `Id`=?");
        $stmt->bind_param("i", $input["id"]);
        $stmt->execute();
        $result = $stmt->get_result();
        if (!$result->num_rows) {
            fuckOff("404");
        } else {
            $data = $result->fetch_assoc();
        }
        $title = $data['Title'];
        $navpath = array('<a href="browsetype.php?type=custom">User-Created Content</a>', noxss($data['Title']));
        break;
    case "furniture":
        $result = mysqli_query($link, "SELECT * FROM `active_furniture` WHERE `Id`=" . $input["id"]);
        if (!mysqli_num_rows($result)) {
            fuckOff("404");
        } else {
            $data = mysqli_fetch_array($result, MYSQLI_ASSOC);
        }
        $title = $data['FriendlyName'];
        $navpath = array('<a href="browsetype.php?type=furniture">Covenant Furniture</a>', noxss($title));
        break;
    default:
        fuckOff("404"); //kill the page if the page type is not one of the previously displayed pages.
        break;
}
require 'views/header.php';
?>
<div class="row">
    <div class="col-md-8 card">
        <div class="card-body">
            <?php
            switch ($type) {
                case "effect":
                    require("views/effect_body.php");
                    break;
                case "custom":
                    $Parsedown = new Parsedown();
                    echo $Parsedown->text(noxss($data['PageContent']));
                    break;
                case "form":
                    require("views/form_body.php");
                    break;
                case "furniture":
                    require("views/furniture_body.php");
                    break;
                case "item":
                    require("views/item_body.php");
                    break;
                case "spell":
                    require("views/spell_body.php");
                    break;
                default:
                    //this should never be used since it comes through wiki.php. ever.
                    fuckOff("418");
                    break;
            }
            ?>
        </div>
    </div>


    <div class="col-md-4 card panel-default">
        <div class="card-body">
            <?php
            switch ($type) {
                case "custom":
                    $Parsedown = new Parsedown();
                    echo $Parsedown->text(noxss($data['SidebarContent']));
                    break;
                case "effect":
                    require("views/effect_sidebar.php");
                    break;
                case "form":
                    require("views/form_sidebar.php");
                    break;
                case "furniture":
                    require("views/furniture_sidebar.php");
                    break;
                case "item":
                    require("views/item_sidebar.php");
                    break;
                case "spell":
                    require("views/spell_sidebar.php");
                    break;
            }
            //Only display the dbinfo segment if it's not a custom page.
            if ($_GET['type'] != "custom") {
                require("views/generic_dbinfo_sidebar.php");
            }
            ?>

        </div>
    </div>
    <?php
    if ($_GET['type'] == "custom") {
        if (loadConfig("EditingEnabled", "Value") == "true") {
            if ($data["Locked"] == false) {
                $id = $_GET['id'];
                echo '<span><a href="custom/customedit.php?Id=' . noxss($id) . '">Edit this page</a></span>';
            } else {
                echo '<span>This page is locked.</span>';
            }
        } else {
            echo '<span>Editing is disabled sitewide.</span>';
        }
    }
    ?>

</div>



<?php
require 'views/footer.php';
?>
