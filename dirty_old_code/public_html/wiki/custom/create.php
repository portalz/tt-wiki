<?php
/**
 * Copyright (c) 2017.,  K.S. (kportalz@protonmail.com)
 *
 */

require_once("../global.php");
//if editing is disabled, kill everything
if (loadConfig("EditingEnabled", "Value") != "true") {
    include "../templates/403.php";
    die();
}


$navpath = array('<a href="#">Custom</a>',"Create Custom");

$title = "custom";
include '../views/header.php';
?>
    <form method="POST" action="create.php">
        Page name: <input type="text" name="title">
        <input type="submit">
    </form>
<?php
if (isset($_POST['title'])) {
    $title = escape($_POST['title'], $link);
    if (mysqli_num_rows(mysqli_query($link, "SELECT * FROM `active_custom` WHERE `Title` LIKE '$title'")) == 0) {
        mysqli_query($link, "INSERT INTO `active_custom` (`Id`, `Title`, `PageContent`, `SidebarContent`, `LastEdit`) VALUES (NULL, '$title', '[Page Content]', '[Sidebar]', CURRENT_TIMESTAMP)");
        $query = "SELECT `Id` FROM `active_custom` WHERE `Title`='$title'";
        $ip = escape(clientIP(), $link);
        $result = mysqli_query($link, $query);
        $pageID = mysqli_fetch_row($result);
        $pageID = $pageID[0];
        mysqli_query($link, "INSERT INTO `wiki_history` (`Id`, `PageID`, `Timestamp`, `PageContent`, `IPAddr`, `Title`) VALUES (NULL, '$pageID', CURRENT_TIMESTAMP, 'NEW PAGE', " . escape(clientIP(), $link) . ", 'NEW PAGE')");
        header("Location: ../wiki.php?type=custom&id=$pageID");
        echo "Successful";


    } else {
        echo "Exists. Failed";
    }
}
include "../views/footer.php";
