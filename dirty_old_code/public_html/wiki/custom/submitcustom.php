<?php
/**
 * Copyright (c) 2017.,  K.S. (kportalz@protonmail.com)
 *
 */

require_once("../global.php");

//make sure editing is globally enabled, if not do nothing.
if (loadConfig("EditingEnabled", "Value") == "true") {


    if (isset($_POST['Id'])) {
        if (is_numeric($_POST['Id'])) {

            $id = escape($_POST['Id'], $link);
            $sidebar = escape($_POST['sbc'], $link);
            $title = escape("Page Edit", $link);
            $content = escape($_POST['content'], $link);
            $ip = escape(clientIP(), $link);
            $sectest = mysqli_fetch_row(mysqli_query($link, "SELECT * FROM `active_custom` WHERE `Id`=$id"));
            if ($sectest[5] != 1) {
                mysqli_query($link, "INSERT INTO `wiki_history` (`Id`, `PageID`, `Timestamp`, `PageContent`, `SidebarContent`, `IPAddr`) VALUES (NULL, '$id', CURRENT_TIMESTAMP, '$content', '$sidebar', '$ip')");
                //now the change has been logged
                mysqli_query($link, "UPDATE `active_custom` SET `PageContent`='$content',`SidebarContent`='$sidebar' WHERE `active_custom`.`Id` = $id");
                header("Location: ../wiki.php?type=custom&id=$id");
            } else {
                echo "This page is locked and cannot be edited.";
            }

        }

    }
}