<?php
/**
 * Copyright (c) 2017.,  K.S. (kportalz@protonmail.com)
 *
 */

require_once "../global.php";
$error_st = 0;
//make sure editing is globally enabled, if not kill everything with 403s
if (loadConfig("EditingEnabled", "Value") != "true") {
    include "../templates/403.php";
    die();
} else {
    if (isset($_GET['Id'])) {
        escape($_GET['Id'], $link);
        if (is_numeric($_GET['Id'])) {
            $id = $_GET['Id'];
            $pageResult = mysqli_query($link, "SELECT * FROM `active_custom` WHERE `Id`=$id");
            //If the page does not exist, create it
            if (mysqli_num_rows($pageResult) == 0) {
                $error_st = 1;
            }
            $res = mysqli_fetch_row($pageResult);
            if ($res[5] == 1) {
                $error_st = 2;
            }
            $pageResult = mysqli_query($link, "SELECT * FROM `active_custom` WHERE `Id`=$id");
            $data = mysqli_fetch_array($pageResult);

        } else {
            echo "Oops.";
        }

    }
}
$title = "Edit Page";
$navpath = array('<a href="../browsetype.php?type=custom">User-Created Content</a>', noxss($data["Title"]), "Edit");

include '../views/header.php';
?>


<style>
    textarea {
        width: 100%;
        height: 100ch;
    }

    .textwrapper {
        border: 1px solid #999999;
        margin: 5px 0;
        padding: 3px;
    }
</style>
<p>
    <small>Custom pages are formatted using Markdown. A reference for markdown can be found <a
                href="https://daringfireball.net/projects/markdown/basics">here</a></small>
</p>
<p>
    <small><b>For anti-abuse purposes, your IP address (<?= clientIP(); ?>) will be recorded if you
            submit this form.</b></small>
</p>
<p>
    <small>Do not use the image tag to embed game graphics unless the artist has given explicit written permission (have
        them email "KPortalz@protonmail.com") AND it is hosted somewhere other than <i>Transformania Time!</i>.
        Embedding images hosted by the <i>Transformania Time!</i> servers can be detrimental to the server performance
        and is not allowed.
    </small>
</p>
<div class="text-center">
    <?php
    switch ($error_st) {
        case 0:
            echo '<form method="POST" action="submitcustom.php">';
            echo '<div class="row">';
            echo '<div class="col-md-8" class="textwrapper">';
            echo '<textarea name="content" rows="25">' . noxss($data["PageContent"]) . '</textarea>';
            echo '</div>';
            echo '<div class="col-md-4" class="textwrapper">';
            echo '<textarea name="sbc" rows="25">' . noxss($data["SidebarContent"]) . '</textarea>';
            echo '<input type="hidden" name="Id" value="' . $id . '">';
            echo '<p><input type="submit" value="Submit"></p>';
            echo '</form>';
            break;
        case 1:
            echo "<h1>Error: The page does not exist.</h1>";
            break;
        case 2:
            echo "<h1>Error: The page you tried to load is locked and cannot be edited</h1>";
            break;
    }
    ?>


</div>
</div>
</body>
</html>
