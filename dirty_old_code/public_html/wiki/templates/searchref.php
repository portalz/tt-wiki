<?php
/**
 * Copyright (c) 2017.,  K.S. (kportalz@protonmail.com)
 *
 */
//203 is the best response code I can find for this purpose.
header("HTTP/1.0 203 Non-Authoritative Information");
?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Content Warning</title>
</head>
<body>
	<div style="text-align:center;">
	<h1>Warning</h1>
	<p>This site contains content that is not to be viewed by users under the age of 18, and may be disturbing.</p>
	<p>This site uses cookies to store user preferences. If you do not feel comfortable with the use of cookies, this site just might not be for you.</p>
	<p><b>By continuing, you confirm you are at least 18 years of age and are over the legal age in your country to view sexually explicit material.</b></p>
	<form method="POST">
		<input type="hidden" name="sc" value="1">
		<input type="submit" name="vf" value="Continue.">
	</form>
	<h2><a href="http://google.com/">No.</a></h2>

	<p>If you continue to see this message, there might be an issue with either the site configuration or your browser.</p>
  
	</div>
</body>
</html>