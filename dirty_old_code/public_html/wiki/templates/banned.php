<?php
/**
 * Copyright (c) 2017.,  K.S. (kportalz@protonmail.com)
 *
 */
header("HTTP/1.0 403 Forbidden");
?>
<!doctype html>
	<html lang="en">
		<head>
			<title>Banned</title>
		</head>
		<body>
			<h1>Banned</h1>
			<h4>You have been banned for the following reason: <b>"<?=$banInfo["Reason"];?>"</b>, effective as of <b><?=date('M j Y g:i A', strtotime($banInfo["Added"]));?> Contact the site administrator (appeal@portalz.xyz) to appeal this ban.</b></h4>
		</body>
	</html>
