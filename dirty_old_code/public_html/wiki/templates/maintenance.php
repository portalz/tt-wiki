<?php
/**
 * Copyright (c) 2017.,  K.S. (kportalz@protonmail.com)
 *
 */

header('HTTP/1.1 503 Service Temporarily Unavailable');
header('Status: 503 Service Temporarily Unavailable');
header('Retry-After: 300');
?>

<!doctype html>
<title>TTWiki - Site Maintenance</title>
<style>
  body { text-align: center; padding: 150px; }
  h1 { font-size: 50px; }
  body { font: 20px Helvetica, sans-serif; color: #333; }
  article { display: block; text-align: left; width: 650px; margin: 0 auto; }
  a { color: #dc8100; text-decoration: none; }
  a:hover { color: #333; text-decoration: none; }
</style>

<article>
    <h1>We&rsquo;ll be back soon!</h1>
    <div>
		
        <p>Sorry for the inconvenience. Please try again in a few minutes! If this message persists, contact <a href="mailto:<?=SERVER_ADMIN;?>"><?=SERVER_ADMIN;?></a></p>
		<p>Message: <?php if(!@noxss(loadConfig("MaintenanceEnabled","Comment"))){echo "Server Error";}else{echo loadConfig("MaintenanceEnabled","Comment",$link);}?>
		<p><small>HTTP/1.0 - 503 Service Unavailable</small></p>
		
    </div>
</article>
