<?php
/**
 * Copyright (c) 2017.,  K.S. (kportalz@protonmail.com)
 *
 */

header("HTTP/1.1 418 I'm a teapot");
?>
<!doctype html>
	<html lang="en">
		<head>
			<title>418 - I'm a Teapot</title>
		</head>
		<body>
			<h1>418 I'm a Teapot</h1>
			<h4>The HTCPCP Server is a teapot. The responding entity MAY be short and stout.</h4>
		</body>
	</html>
