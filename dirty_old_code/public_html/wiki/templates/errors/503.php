<?php
/**
 * Copyright (c) 2017.,  K.S. (kportalz@protonmail.com)
 *
 */
header("HTTP/1.0 503 Service Unavailable");
?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8"></meta>
	<title>503 - Service Unavailable</title>
</head>
<body>
	<h1>Service Unavailable</h1>
	<h4>The server is unavailable to handle your request.</h4>
		<ul>
			<li>contact <a href="mailto:<?=SERVER_ADMIN;?>"><?=SERVER_ADMIN;?></a></li>
		</ul>

</body>