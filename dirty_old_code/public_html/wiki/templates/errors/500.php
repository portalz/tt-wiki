<?php
/**
 * Copyright (c) 2017.,  K.S. (kportalz@protonmail.com)
 *
 */

header("HTTP/1.0 500 Internal Server Error");
?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8"></meta>
	<title>500 - Internal Server Error</title>
</head>
<body>
	<h1>Internal Server Error</h1>
	<h4>The server experienced an error. If you believe this may be a bug:	</h4>
		<ul>
			<li>contact <a href="mailto:<?=SERVER_ADMIN;?>"><?=SERVER_ADMIN;?></a></li>
		</ul>

</body>