<?php
/**
 * Copyright (c) 2017.,  K.S. (kportalz@protonmail.com)
 *
 */

header("HTTP/1.0 404 Not Found");
?>
<!doctype html>
	<html lang="en">
		<head>
			<title>404 - Not Found</title>
		</head>
		<body>
			<h1>404 - Page Not Found</h1>
			<h4>The page you tried to reach could not be found.</h4>
		</body>
	</html>
