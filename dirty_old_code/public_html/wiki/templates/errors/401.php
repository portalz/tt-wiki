<?php
/**
 * Copyright (c) 2017.,  K.S. (kportalz@protonmail.com)
 *
 */

header("HTTP/1.0 401 Unauthorized");
?>
<!doctype html>
	<html lang="en">
		<head>
			<title>401 - Unauthorized</title>
		</head>
		<body>
			<h1>401 - Unauthorized</h1>
			<h4>You are not authorized to view this page.</h4>
		</body>
	</html>
