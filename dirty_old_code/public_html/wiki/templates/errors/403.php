<?php
/**
 * Copyright (c) 2017.,  K.S. (kportalz@protonmail.com)
 *
 */

header("HTTP/1.0 403 Forbidden");
?>
<!doctype html>
	<html lang="en">
		<head>
			<title>403 - Forbidden</title>
		</head>
		<body>
			<h1>403 - Forbidden</h1>
			<h4>Access to this resource is denied.</h4>
		</body>
	</html>
