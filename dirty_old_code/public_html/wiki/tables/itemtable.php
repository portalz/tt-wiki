<!DOCTYPE html>
<?php $jsondir="https://sunnyglade-tech.xyz/json/";?>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>item sorter</title>

	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">
	<link rel="shortcut icon" sizes="16x16" href="../resc/favicon.ico">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
	
	<script src="sortable.js"></script>
	<script>
	$(document).ready(function() {
		$("#clear").on("click", function(e) {
			e.preventDefault();
		});

		$('#hatbox').click(function () {
			$(".hat").fadeToggle("fast");
		});
		$('#shirtbox').click(function() {
			$(".shirt").fadeToggle("fast");
		});
		$('#undershirtbox').click(function() {
			$(".undershirt").fadeToggle("fast");
		});
		$('#pantsbox').click(function() {
			$(".pants").fadeToggle("fast");
		});
		$('#underpantsbox').click(function() {
			$(".underpants").fadeToggle("fast");
		});
		$('#shoebox').click(function() {
			$(".shoes").fadeToggle("fast");
		});
		$('#jewlerybox').click(function() {
			$(".accessory").fadeToggle("fast");
		});
		$('#petbox').click(function() {
			$(".pet").fadeToggle("fast");
		});
		$('#consumethis').click(function() {
			$(".consumable").fadeToggle("fast");
		});
		$('#savemefromthisjavascript').click(function() { 
			$(".consumable_reuseable").fadeToggle("fast");
		});
	});
	</script>
	
</head>
<body>
<div class="container">
<p>If you don't understand the stats, don't worry, I don't either. Check the (magic) matrix <a href="http://transformaniatime.com/Item/ShowStatsTable">here</a> and the forum post <a href="http://luxianne.com/forum/viewtopic.php?f=9&t=738">here.</a></p>
<p><form><b>Show item type:</b> <input type="checkbox" id="hatbox" checked> Hats | <input type="checkbox" id="shirtbox" checked> Shirts| <input type="checkbox" id="undershirtbox" checked> Undershirts | <input type="checkbox" id="pantsbox" checked> Pants | <input type="checkbox" id="underpantsbox" checked> Underpants | <input type="checkbox" id="shoebox" checked> Shoes | <input type="checkbox" id="jewlerybox" checked> Accessory | <input type="checkbox" id="petbox" checked> Pets | <input type="checkbox" id="consumethis" checked> Consumable |  <input type="checkbox" id="savemefromthisjavascript" checked> Reuseable | </form></p>
<p><strong>For the form table, click <a href="formtable.php">here</a></strong></p>
</div>
<?php
	$json = file_get_contents($jsondir."items.json");
	$statslist = array('FriendlyName','ItemType','Discipline','Perception','Charisma','Fortitude','Agility','Allure','Magicka','Succour','Luck');
	$datas=json_decode($json, true);
	echo "<table class=\"sortable table table-bordered\"><tr><th>Name</th><th>Type</th><th>Discipline</th><th>Perception</th><th>Charisma</th><th>Fortitude</th><th>Agility</th><th>Allure</th><th>Magicka</th><th>Succour</th><th>Luck</th></tr>";
	foreach($datas as $data){
		echo '<tr class="gameitem '.$data["ItemType"].'">';
		foreach($statslist as $statitem)
		{
			
				if($data["$statitem"]==null){$data["$statitem"]=0;}
				if($data["$statitem"]<=0){$class="danger";}
				if($data["$statitem"]>=0){$class="success";}
				if($data["$statitem"]==0){$class="active";}

				
				if($statitem == "FriendlyName")
				{
					echo '<td class="'.$class.'" data><a href="http://portalz.xyz/wiki/wiki.php?type=item&id='.$data["Id"].'">'.$data["$statitem"].'</a></td>';
				}
				else
				{
					echo "<td class=\"$class\" data>".$data["$statitem"]."</td>";
				}
			
		}
		echo "</tr>";
		
	}
	echo "</table>";
	?>
 
	
	

	</body>
</html>