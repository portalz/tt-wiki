<!DOCTYPE html>
<?php $jsondir = "https://sunnyglade-tech.xyz/json/"; ?>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>form sorter</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">
    <link rel="shortcut icon" sizes="16x16" href="../resc/favicon.ico">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
    <script src="sortable.js"></script>
</head>
<body>
<div class="container">
    <p>If you don't understand the stats, don't worry, I don't either. Check the (magic) matrix <a
                href="http://transformaniatime.com/Item/ShowStatsTable">here</a> and the forum post <a
                href="http://luxianne.com/forum/viewtopic.php?f=9&t=738">here.</a> Soon this will convert to readable
        stats and sort by those, too.</p>
    <p><strong>For the item table, click <a href="itemtable.php">here</a></strong></p>
    <?php
    $json = file_get_contents($jsondir . "forms.json");
    $statslist = array('FriendlyName', 'MobilityType', 'Discipline', 'Perception', 'Charisma', 'Fortitude', 'Agility', 'Allure', 'Magicka', 'Succour', 'Luck');
    $datas = json_decode($json, true);
    echo "<table class=\"sortable table table-bordered\"><tr><th>Name</th><th>Type</th><th>Discipline</th><th>Perception</th><th>Charisma</th><th>Fortitude</th><th>Agility</th><th>Allure</th><th>Magicka</th><th>Succour</th><th>Luck</th></tr>";
    foreach ($datas as $data) {
        echo '<tr class="gameitem">';
        foreach ($statslist as $statitem) {
            if ($data["MobilityType"] == "full") {
                if ($data["$statitem"] == null) {
                    $data["$statitem"] = 0;
                }
                if ($data["$statitem"] <= 0) {
                    $class = "danger";
                }
                if ($data["$statitem"] >= 0) {
                    $class = "success";
                }
                if ($data["$statitem"] == 0) {
                    $class = "active";
                }
                if ($statitem == "FriendlyName") {
                    echo '<td class="' . $class . '"><a href="http://portalz.xyz/wiki/wiki.php?type=form&id=' . $data["Id"] . '">' . $data["$statitem"] . '</a></td>';
                } else {
                    echo "<td class=\"$class\">" . $data["$statitem"] . "</td>";
                }
            }
        }
        echo "</tr>";

    }
    echo "</table>";
    ?>


</div>
</body>
</html>
