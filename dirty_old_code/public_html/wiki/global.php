<?php
/**
 * Copyright (c) 2017.,  K.S. (kportalz@protonmail.com)
 *
 */

$starttime = explode(' ', microtime());
$starttime = $starttime[1] + $starttime[0];
require_once("configuration.php");
require_once("lib/Parsedown.php");
//Handle any errors before the page loads
//connect to the mysql server

//Shiny new type
$link = new mysqli(MYSQL_SERVER, MYSQL_USER, MYSQL_PASS, MYSQL_DB);
if ($link->connect_errno) {
    echo "Failed to connect to MySQL: (" . $link->connect_errno . ") " . $link->connect_error;
}
//old SQL for legacy stuff
//@$link = mysqli_connect(MYSQL_SERVER, MYSQL_USER, MYSQL_PASS, MYSQL_DB);
//if (!$link) {
//    fuckOff("503");
//}


if (@loadConfig("MaintenanceEnabled", "Value") == "true") {
    if (!isset($is_api)) {
        include "templates/maintenance.php";
        die();
    } else if (isset($is_api)) {
        header("HTTP/1.0 500 Internal Server Error");
        header('Retry-After: 300');
        die();
    }
}
if (mysqli_num_rows($banResult = mysqli_query($link, "SELECT * FROM `bans` WHERE `REMOTE_ADDR`='" . escape(clientIP(), $link) . "' AND `Active`='1'"))) {
    $banInfo = mysqli_fetch_array($banResult, MYSQLI_ASSOC);
    include "templates/banned.php";
    die();
}
if (file_exists("ee.php") && ENABLE_EE_FILE == true) {
    require_once "ee.php";
}
//global.php
//global functions used by the site
//Any code that is to be run at the beginning of all pages indiscriminantly

//here's all of the composite stats fields because shitty workarounds.
$compstats = array('Discipline', 'Perception', 'Charisma', 'Fortitude', 'Agility', 'Allure', 'Magicka', 'Succour', 'Luck');
/*
//////////////////////////////////////////////////
IMPORTANT
//////////////////////////////////////////////////
The code below displays a warning to the viewer if they don't have the proper cookie set.
*/
if (!isset($is_api)) {
    if (!isset($_COOKIE['CONTENT_WARNING']) && !isset($_POST['sc'])) {
        include("templates/searchref.php");
        die();
    } elseif (isset($_POST['sc']) && !isset($_COOKIE['CONTENT_WARNING'])) {
        setcookie("CONTENT_WARNING", rand(), time() + 604800); //sets cookie with 1 week expiration
    }
}
/*
//////////////////////////////////////////////////
FUNCTIONS USED GLOBALLY
//////////////////////////////////////////////////
*/
//The function to kick things on errors.
function fuckOff($e)
{
    switch ($e) {
        case "401":
            include "templates/errors/401.php";
            die();
            break;
        case "403":
            include "templates/errors/403.php";
            die();
            break;
        case "404":
            include "templates/errors/404.php";
            die();
            break;
        case "418":
            include "templates/errors/418.php";
            die();
            break;
        case "500":
            include "templates/errors/500.php";
            die();
            break;
        case "503":
            include "templates/errors/503.php";
            die();
            break;
        default:
            include "templates/errors/500.php";
            die();
            break;
    }
}

//escapes mysql variables
function escape($var, $conn)
{
    return mysqli_real_escape_string($conn, $var);
}

//filter for possible XSS
function noxss($input)
{
    return htmlspecialchars($input, ENT_QUOTES, 'UTF-8');
}

function clientIP()
{
    if (ENABLE_CLOUDFLARE == true) {
        if (isset($_SERVER["HTTP_CF_CONNECTING_IP"])) //is it ACTUALLY using cloudflare?
        {
            return $_SERVER["HTTP_CF_CONNECTING_IP"];
        } else //no, user messed up configuration. default to the REMOTE_ADDR
        {
            return $_SERVER["REMOTE_ADDR"];
        }
    } else {
        return $_SERVER["REMOTE_ADDR"];
    }
}

//display notices in the top of the page
function displayNotices()
{
    //if you're looking for where the notices come from, it's in the `notices` table for ease of adding and removing notices. Setting the "TimeToHide" column to zero will make notices unhideable.
    $notices = mysqli_query($GLOBALS['link'], 'SELECT * FROM `notices` WHERE `Active`=1');
    while ($row = mysqli_fetch_array($notices)) {
        if (!isset($_COOKIE[$row['Cookie']])) {
            echo '<div class="alert alert-' . $row['Type'] . ' ' . ($row['TimeToHide'] != 0 ? 'alert-dismissable' : '') . '" id="alert_' . $row['IdStr'] . '" role="alert">';
            if ($row['TimeToHide'] != 0) {
                echo '<button type="button" data-strid="' . $row['IdStr'] . '" id="alertclose_' . $row['IdStr'] . '" class="close" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
            }
            echo '<strong>' . $row['Title'] . '</strong>
				<p> ' . $row['Content'] . '</p>
			</div>';
        }
    }
    //other notices can be defined here if you only want them to appear under certain conditions

    //for example, this notice checks if the user's IP is in a certain range, and if it is, it shows them a purple notice telling them they're visiting from a vauge, yet menacing government agency
    // if(checkVagueYetMenacingGovernmentAgency())
    // {
    // 	echo '<div class="alert alert-success" style="background:#9C3B92;color:white;">You appear to be visiting from a vague, yet menacing government agency. Welcome.</div>';
    // }
}

/*Is the user at the form page for an item instead of the item page for said item? TAKE THEM THERE!*/
function rescueUser($dbname)
{
    global $link;
    $stmt = $link->prepare("SELECT `Id` FROM `active_items` WHERE `dbName`=?");
    $stmt->bind_param("s", $dbname);
    $stmt->execute();
    $stmt->bind_result($item_id);
    $stmt->fetch();
    $url = "wiki.php?type=item&id=" . $item_id;
    header("Location: $url");

}

//loadConfig(option,value,conn) loads a setting from the dynamic configuration database table
function loadConfig($configOption, $value)
{
    global $link;
//    switch ($value) {
//        case "Value":
//            $stmt = $link->prepare("SELECT `Value`  FROM `config` WHERE `Setting`=?");
//            break;
//        case "Timestamp":
//            $stmt = $link->prepare("SELECT `Timestamp` FROM `config` WHERE `Setting`=?");
//            break;
//        case "Comment":
//            $stmt = $link->prepare("SELECT `Comment` FROM `config` WHERE `Setting`=?");
//            break;
//        default:
//            return false;
//    }
//    $stmt->bind_param("s", $configOption);
//    $stmt->execute();
//    $stmt->bind_result($res);
//    $stmt->fetch();
//    return $res;
    return mysqli_fetch_row(mysqli_query($link, "SELECT `$value` FROM `config` WHERE `Setting`='$configOption'"))[0];
}


function crossQuery($column, $table, $conditional, $conn)
{

    return mysqli_fetch_row(mysqli_query($conn, 'SELECT `' . $column . '` FROM `' . $table . '` WHERE ' . $conditional . ';'))[0];
}

function displayBreadcrumbs($navpath)
{
    $navpath = array_merge(['<a href="' . BASE_DIR . '">' . SITE_NAME . '</a>'], $navpath);
    $navpath = array_merge(['<a href="' . BASE_DIR . '/sitemap.php">' . ROOT_NAME. '</a>'], $navpath);

	$bc_last = sizeof($navpath) - 1;
    for ($i = 0; $i <= $bc_last - 1; $i++) {
        echo '<li class="breadcrumb-item">' . $navpath[$i] . '</li>';
    }
    echo '<li class="breadcrumb-item active">' . strip_tags($navpath[$bc_last]) . '</li>';
}

?>
