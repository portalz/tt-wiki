<?php
/**
 * Copyright (c) 2017.,  K.S. (kportalz@protonmail.com)
 *
 */

require_once("global.php");
$title = "Main Page";
$navpath = array('Main Page');
include 'views/header.php';
include 'functions/search_functions.php';


include 'misc/search_form.php';

if (isset($_POST['submit'])) {

    echo '<p>Showing results for: ' . noxss($_POST['query']) . '</p>';

    $array = array();
    $text = escape($_POST['query'], $link);
    if(file_exists("ee.php") && ENABLE_EE_FILE==true)
    {
        eeSearch($text);
    }
    if(LOG_SEARCHES==true) {
        mysqli_query($link, "INSERT INTO `history` (`Id`, `Round`, `TimeStamp`, `Query`) VALUES (NULL, '" . loadConfig("GameRound", "value") . "', CURRENT_TIMESTAMP, '$text')");
    }

    /* Items */

    if (mysqli_num_rows($result = mysqli_query($link, "SELECT * FROM `active_items` WHERE (`Description` LIKE '%$text%' OR `FriendlyName` LIKE '%$text%' OR `dbName` LIKE '%$text%' OR `PortraitURL` LIKE '%$text%') AND `ItemType`!='pet' ORDER BY `FriendlyName` ASC"))) {
        $count= mysqli_num_rows($result);
        echo "<h2 id=\"item-title\">Items (" . $count . ($count == 1 ? ' result':' results').")</h2>";
        echo '<dl id="items">';
        displayResults($result, "(Items)", "item");
        echo "</dl>";
    }


    /* Pets */
    if (mysqli_num_rows($result = mysqli_query($link, "SELECT * FROM `active_items` WHERE (`Description` LIKE '%$text%' OR `FriendlyName` LIKE '%$text%' OR `dbName` LIKE '%$text%' OR `PortraitURL` LIKE '%$text%') AND `ItemType`='pet' ORDER BY `FriendlyName` ASC"))) {
        $count= mysqli_num_rows($result);
        echo "<h2 id=\"item-title\">Pets (" . $count . ($count == 1 ? ' result':' results').")</h2>";
        echo '<dl id="items">';
        displayResults($result, "(Pets)", "item");
        echo "</dl>";
    }


    /* Forms */
    if (mysqli_num_rows($result = mysqli_query($link, "SELECT * FROM `active_forms` WHERE (`Description` LIKE '%$text%' OR `FriendlyName` LIKE '%$text%' OR `dbName` LIKE '%$text%')  AND `MobilityType`='full' ORDER BY `FriendlyName` ASC"))) {
        $count= mysqli_num_rows($result);
        echo "<h2 id=\"form-title\">Forms (" . $count . ($count == 1 ? ' result':' results').")</h2>";
        echo '<dl id="forms">';
        displayResults($result, "(Forms)", "form");
        echo '</dl>';
    }
    /* Spells */
    if (mysqli_num_rows($result = mysqli_query($link, "SELECT * FROM `active_spells` WHERE `Description` LIKE '%$text%' OR `FriendlyName` LIKE '%$text%' OR `DiscoveryMessage` LIKE '%$text%' ORDER BY `FriendlyName` ASC"))) {
        $count= mysqli_num_rows($result);
        echo "<h2 id=\"spell-title\">Spells (" . $count . ($count == 1 ? ' result':' results').")</h2>";
        echo '<dl id="spells">';
        displayResults($result, "(Spells)", "spell");
        echo '</dl>';
    }
    // Effects
    if (mysqli_num_rows($result = mysqli_query($link, "SELECT * FROM `active_effects` WHERE `Description` LIKE '%$text%' OR `FriendlyName` LIKE '%$text%' OR `MessageWhenHit` LIKE '%$text%' OR `MessageWhenHit_M` LIKE '%$text' OR `MessageWhenHit_F` LIKE '%$text' OR `AttackerWhenHit` LIKE '%$text' OR `AttackerWhenHit_M` LIKE '%$text' OR `AttackerWhenHit_F` LIKE '%$text' ORDER BY `FriendlyName` ASC"))) {
        $count= mysqli_num_rows($result);
        echo "<h2 id=\"effect-title\">Effects (" . $count . ($count == 1 ? ' result':' results').")</h2>";
        echo '<dl id="effects">';
        displayResults($result, "(Effects)", "effect");
        echo '</dl>';
    }
    // Furniture
    if (mysqli_num_rows($result = mysqli_query($link, "SELECT * FROM `active_furniture` WHERE `Description` LIKE '%$text%' OR `FriendlyName` LIKE '%$text%' ORDER BY `FriendlyName` ASC"))) {
        $count= mysqli_num_rows($result);
        echo "<h2 id=\"furniture-title\">Furniture (" . $count . ($count == 1 ? ' result':' results').")</h2>";
        echo '<dl id="furniture">';
        displayResults($result, "(Furniture)", "furniture");
        echo '</dl>';
    }

    /* Custom */
    if (mysqli_num_rows($result = mysqli_query($link, "SELECT * FROM `active_custom` WHERE `PageContent` LIKE '%$text%' OR `Title` LIKE '%$text%'"))) {
        $count= mysqli_num_rows($result);
        echo "<h2 id=\"custom-title\">custom (" . $count . ($count == 1 ? ' result':' results').")</h2>";
        echo '<dl id="customs">';
        displayCustom($result, "(custom)", "custom");
        echo '</dl>';
    }


//	echo "DONE ITEMS";

}
?>

    <br><br><br><br>

<?php
require_once("views/footer.php");
