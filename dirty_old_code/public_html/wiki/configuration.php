
<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
/*
  This configuration file contains "static settings" that will most likely not change during the course of a deployment.
  Settings such as enabling/disabling maintenance mode, editing, and the most recent import are considered "dynamic settings" and can be found in the `config` table of your database.
  They are set in the database to allow for easier integration with an administration panel that is not available to the public.
*/
define("ROLE", "dev");
define("IMGCDN","https://cdn.portalz.xyz/img/"); //location where game images can be found
define("CCDN","//cdn.portalz.xyz/"); //location where other things can be found
define('SERVER_ADMIN', 'kevin@portalz.xyz'); //the email address of the server administrator
define('BASE_DIR', 'https://dev.portalz.xyz/wiki'); //the base directory of the installation
define('SITE_NAME', 'Testing Grounds');
define('ROOT_NAME', 'portalz.xyz');

//MySQL Connection Details
define('MYSQL_SERVER','localhost');
define('MYSQL_USER','kevpo');
define('MYSQL_PASS', '51k2quHVARcurl');
define('MYSQL_DB', 'ttcalc_dev');



define('LOG_SEARCHES',false);
define('ENABLE_EE_FILE',true);
define('COOKIE_AUTHKEY',''); // the password to use for cookie counting

//Stuff for Discord WebHooks. If you have it setup, it'll give updates information in a discord channel. That's not a thing yet. Maybe later.
define('ENABLE_DISCORD_WEBHOOKS',false);
define('DISCORD_WEBHOOK_URL','https://discordapp.com/api/webhooks/389266940532621323/dqkxCeCs1QVpfJi7Csjp6OPHnBFREgX9KonRGRo_cgwhC74GmZpkH-QOhHErrVQtJiQk');
define('DISCORD_WEBHOOK_USERNAME','Webhook Test');


define('ENABLE_CLOUDFLARE','true'); //Is CloudFlare enabled? If so, it'll handle banned IPs and edit log IPs differently.
define('JSON_LOCATION','https://sunnyglade-tech.xyz/json/');
