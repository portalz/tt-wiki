<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpgradeFurnitureForForeignKeyUpdates extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('active_furniture', function ($table) {
            $table->dropColumn('GivesItem');
            $table->dropColumn('GivesEffect');

            $table->unsignedInteger('GivesEffectSourceId')->nullable()->comment('The Id of the effect that this furniture gives. Optional.');
            $table->unsignedInteger('GivesItemSourceId')->nullable()->comment('The Id of the items that this furniture gives. Optional.');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
