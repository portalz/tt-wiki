<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpgradeEffectsForForeignKeyUpdate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('active_effects', function ($table) {
            $table->dropColumn('dbName');
            $table->dropColumn('PreRequesite');

            $table->unsignedInteger('PreRequisiteEffectSourceId')->nullable()->comment('The prerequisite perk for this effect. Optional.');
            $table->Integer('RequiredGameMode')->nullable()->comment('Game mode required for this effect. Optional.');
            $table->Integer('BlessingCurseStatus')->nullable()->comment('I dunno.');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
