<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('active_locations', function (Blueprint $table) {
//            Increments because it doesn't come with an ID column
            $table->increments('Id');
            $table->text('dbName');
            $table->text('Name')->nullable();
            $table->longText('Description')->nullable();
            $table->integer('X');
            $table->integer('Y');
            $table->text('IsSafe')->nullable();
            $table->text('ImageUrl')->nullable();
            $table->text('Name_North')->nullable();
            $table->text('Name_East')->nullable();
            $table->text('Name_South')->nullable();
            $table->text('Name_West')->nullable();
            $table->text('Region')->nullable();
            $table->integer('CovenantController')->nullable();
            $table->integer('TakeoverAmount')->nullable();
            $table->text('FriendlyName_North')->nullable();
            $table->text('FriendlyName_East')->nullable();
            $table->text('FriendlyName_South')->nullable();
            $table->text('FriendlyName_West')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('active_locations');
    }
}
