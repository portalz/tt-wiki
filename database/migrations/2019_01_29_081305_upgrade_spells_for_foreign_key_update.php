<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpgradeSpellsForForeignKeyUpdate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('active_spells', function ($table) {
            $table->dropColumn('dbName');
            $table->dropColumn('FormdbName');
            $table->dropColumn('ExclusiveToForm');
            $table->dropColumn('ExclusiveToItem');
            $table->dropColumn('GivesEffect');

            $table->unsignedInteger('FormSourceId')->nullable()->comment('The Form that this spells causes the target to become, matches to Id on the forms table. Optional.');
            $table->unsignedInteger('GivesEffectSourceId')->nullable()->comment('The Id of the effect that this spell gives. Optional.');
            $table->unsignedInteger('ExclusiveToFormSourceId')->nullable()->comment('The Id of the FORM that this spell is exclusive to. Optional.');
            $table->unsignedInteger('ExclusiveToItemSourceId')->nullable()->comment('The Id of the ITEM that this spell is exclusive to. Optional.');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
