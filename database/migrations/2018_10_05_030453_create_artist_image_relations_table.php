<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArtistImageRelationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('image_relations', function (Blueprint $table) {
            //Columns
            $table->increments('Id');
            $table->text('FileName');
            $table->text('ImageLink');
            $table->unsignedInteger('Artist');

            //Foreign Keys
            $table->foreign('Artist')->references('Id')->on('artist_details');

            //Metadata
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('image_relations');
    }
}
