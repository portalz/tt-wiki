<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemsPetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('active_items', function (Blueprint $table) {
            //Columns
            $table->unsignedInteger('Id');
            $table->text('dbName')->nullable();
            $table->text('FriendlyName')->nullable();
            $table->longText('Description')->nullable();
            $table->text('PortraitUrl')->nullable();
            $table->text('IsUnique')->nullable();
            $table->decimal('Discipline', 11, 3)->default(0);
            $table->decimal('Perception', 11, 3)->default(0);
            $table->decimal('Charisma', 11, 3)->default(0);
            $table->decimal('Fortitude', 11, 3)->default(0);
            $table->decimal('Agility', 11, 3)->default(0);
            $table->decimal('Allure', 11, 3)->default(0);
            $table->decimal('Magicka', 11, 3)->default(0);
            $table->decimal('Succour', 11, 3)->default(0);
            $table->decimal('Luck', 11, 3)->default(0);
            $table->decimal('MoneyValue', 11, 2)->nullable();
            $table->decimal('MoneyValueSell',11,2)->nullable();
            $table->text('ItemType');
            $table->integer('UseCooldown')->default(0);
            $table->boolean('Findable')->nullable();
            $table->integer('FindWeight')->default(0);
            $table->text('GivesEffect')->nullable();
            $table->integer('InstantHealthRestore')->default(0);
            $table->integer('InstantManaRestore')->default(0);
            $table->integer('ReuseableHealthRestore')->default(0);
            $table->integer('ReuseableManaRestore')->default(0);
            $table->text('CurseTFFormdbName')->nullable();

            //Keys and Indexes
            $table->primary('Id');

            //Metadata


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('active_items');
    }
}
