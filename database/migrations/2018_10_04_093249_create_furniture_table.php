<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFurnitureTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('active_furniture', function (Blueprint $table) {
            //Columns
            $table->unsignedInteger('Id');
            $table->text('dbType');
            $table->text('FriendlyName')->nullable();
            $table->text('GivesEffect')->nullable();
            $table->decimal('APReserveRefillAmount',11,3)->default(0);
            $table->decimal('BaseCost',11,3)->default(0);
            $table->integer('BaseContractTurnLength')->default(0);
            $table->text('GivesItem')->nullable();
            $table->integer('MinutesUntilReuse')->default(0);
            $table->longText('Description')->nullable();
            $table->text('PortraitUrl')->nullable();

            //Keys and Indexes
            $table->primary('Id');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('active_furniture');
    }
}
