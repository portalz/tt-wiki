<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpgradeFormsForForeignKeyUpdate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('active_forms', function ($table) {
            $table->dropColumn('dbName');
            $table->dropColumn('BecomesItemDbName');

            $table->unsignedInteger('BecomesItemSourceId')->nullable()->comment('the Id on the Items table for the item associated with this form. Optional.');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
