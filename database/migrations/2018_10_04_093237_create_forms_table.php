<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFormsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('active_forms', function (Blueprint $table) {

            $table->unsignedInteger('Id');
            $table->text('dbName');
            $table->text('FriendlyName')->nullable();
            $table->longText('Description')->nullable();
            $table->text('TFEnergyType')->nullable();
            $table->decimal('TFEnergyRequired', 11, 3);
            $table->text('Gender')->nullable();
            $table->text('MobilityType');
            $table->text('BecomesItemDbName')->nullable();
            $table->text('PortraitUrl')->nullable();
            $table->boolean('IsUnique')->nullable();
            $table->decimal('Discipline', 11, 3)->default(0);
            $table->decimal('Perception', 11, 3)->default(0);
            $table->decimal('Charisma', 11, 3)->default(0);
            $table->decimal('Fortitude', 11, 3)->default(0);
            $table->decimal('Agility', 11, 3)->default(0);
            $table->decimal('Allure', 11, 3)->default(0);
            $table->decimal('Magicka', 11, 3)->default(0);
            $table->decimal('Succour', 11, 3)->default(0);
            $table->decimal('Luck', 11, 3)->default(0);
            $table->decimal('Submission_Dominance', 11, 3)->default(0);
            $table->decimal('Corruption_Purity', 11, 3)->default(0);
            $table->decimal('Chaos_Order', 11, 3)->default(0);
            $table->decimal('ManaBonusPercent', 11, 3)->default(0);
            $table->decimal('ExtraSkillCriticalPercent', 11, 3)->default(0);
            $table->decimal('HealthBonusPercent', 11, 3)->default(0);
            $table->decimal('HealthRecoveryPerUpdate', 11, 3)->default(0);
            $table->decimal('ManaRecoveryPerUpdate', 11, 3)->default(0);
            $table->decimal('SneakPercent', 11, 3)->default(0);
            $table->decimal('EvasionPercent', 11, 3)->default(0);
            $table->decimal('EvasionNegationPercent', 11, 3)->default(0);
            $table->decimal('MeditationExtraMana', 11, 3)->default(0);
            $table->decimal('CleanseExtraHealth', 11, 3)->default(0);
            $table->decimal('MoveActionPointDiscount', 11, 3)->default(0);
            $table->decimal('SpellExtraTFEnergyPercent', 11, 3)->default(0);
            $table->decimal('SpellExtraHealthDamagePercent', 11, 3)->default(0);
            $table->decimal('CleanseExtraTFEnergyRemovalPercent', 11, 3)->default(0);
            $table->decimal('SpellMisfireChanceReduction', 11, 3)->default(0);
            $table->decimal('SpellHealthDamageResistance', 11, 3)->default(0);
            $table->decimal('SpellTFEnergyDamageResistance', 11, 3)->default(0);
            $table->decimal('ExtraInventorySpace', 11, 3)->default(0);

            //Keys and Indexes
            $table->primary('Id');

            //Metadata

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('active_forms');
    }
}
