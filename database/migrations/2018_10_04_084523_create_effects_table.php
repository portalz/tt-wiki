<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEffectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('active_effects', function (Blueprint $table) {
            //Columns
            $table->unsignedInteger('Id');
            $table->text('dbName');
            $table->text('FriendlyName')->nullable();
            $table->longText('Description')->nullable();
            $table->integer('AvailableAtLevel')->nullable();
            $table->text('PreRequesite')->nullable();
            $table->boolean('isLevelUpPerk')->default(0);
            $table->integer('Duration')->nullable();
            $table->integer('Cooldown')->nullable();
            $table->text('ObtainedAtLocation')->nullable();
            $table->boolean('IsRemovable')->default(0);
            $table->longText('MessageWhenHit')->nullable();
            $table->longText('MessageWhenHit_M')->nullable();
            $table->longText('MessageWhenHit_F')->nullable();
            $table->longText('AttackerWhenHit')->nullable();
            $table->longText('AttackerWhenHit_M')->nullable();
            $table->longText('AttackerWhenHit_F')->nullable();
            $table->decimal('Discipline',11,3)->default(0);
            $table->decimal('Perception',11,3)->default(0);
            $table->decimal('Charisma',11,3)->default(0);
            $table->decimal('Fortitude',11,3)->default(0);
            $table->decimal('Agility',11,3)->default(0);
            $table->decimal('Allure',11,3)->default(0);
            $table->decimal('Magicka',11,3)->default(0);
            $table->decimal('Succour',11,3)->default(0);
            $table->decimal('Luck',11,3)->default(0);

            //Keys and Indexes
            $table->primary('Id');

            //Metadata


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('active_effects');
    }
}
