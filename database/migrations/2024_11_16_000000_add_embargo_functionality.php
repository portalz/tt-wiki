<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddEmbargoFunctionality extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('embargo', function (Blueprint $table) {
            $table->unsignedInteger('Id')->autoIncrement();
            $table->string('record_type');
            $table->unsignedInteger('record_id');
            $table->dateTime('expiration')->nullable();
            $table->text('comment')->nullable();
            $table->boolean('active')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('embargo');

    }
}
