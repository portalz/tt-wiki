<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpellsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('active_spells', function (Blueprint $table) {
            //Columns
            $table->unsignedInteger('Id');
            $table->text('dbName');
            $table->text('FriendlyName')->nullable();
            $table->text('FormdbName')->nullable();
            $table->longText('Description')->nullable();
            $table->integer('ManaCost')->nullable();
            $table->decimal('TFPointsAmount',11,3)->nullable();
            $table->decimal('HealthDamageAmount',11,3)->nullable();
            $table->text('LearnedAtRegion')->nullable();
            $table->text('LearnedAtLocation')->nullable();
            $table->longText('DiscoveryMessage')->nullable();
            $table->text('IsLive')->nullable();
            $table->boolean('IsPlayerLearnable')->nullable();
            $table->text('GivesEffect')->nullable();
            $table->text('ExclusiveToForm')->nullable();
            $table->text('ExclusiveToItem')->nullable();
            $table->text('MobilityType')->nullable();
            //Metadata

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('active_spells');
    }
}
