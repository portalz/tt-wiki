<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpgradeItemsForForeignKeyUpdate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('active_items', function ($table) {
            $table->dropColumn('dbName');
            $table->dropColumn('GivesEffect');
            $table->dropColumn('CurseTFFormdbName');

            $table->unsignedInteger('GivesEffectSourceId')->nullable()->comment('the Id on the effect this item gives. Optional.');
            $table->unsignedInteger('CurseTFFormSourceId')->nullable()->comment('the Id on the effect this item gives. Optional.');
            $table->Integer('ConsumableSubItemType')->nullable()->comment('What type of consumable is this? Can you eat it?');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
