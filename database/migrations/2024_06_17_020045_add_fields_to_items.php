<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldsToItems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('active_items', function (Blueprint $table) {
            $table->unsignedInteger('RuneLevel')->nullable();
            $table->text('UsageMessage_Item')->nullable();
            $table->text('UsageMessage_Player')->nullable();
            $table->text('CurseTFFormdbName')->nullable();
            $table->dropColumn(['ReuseableHealthRestore', 'ReuseableManaRestore', 'InstantHealthRestore', 'InstantManaRestore']);

        });
        Schema::table('active_items', function (Blueprint $table) {
            $table->decimal('ReuseableHealthRestore', 11, 3)->default(0);
            $table->decimal('ReuseableManaRestore', 11, 3)->default(0);
            $table->decimal('InstantHealthRestore', 11, 3)->default(0);
            $table->decimal('InstantManaRestore', 11, 3)->default(0);
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('active_items', function (Blueprint $table) {
            //
        });
    }
}
