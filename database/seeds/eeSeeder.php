<?php

use Illuminate\Database\Seeder;

class eeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('ee')->insert([
            'Id' => null,
            'eeloc'=>'landing',
            'eep'=>'Nothing to see here',
            'eec'=>'Nothing to see here'

        ]);

    }
}
