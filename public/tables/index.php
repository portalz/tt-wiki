<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Stats Tables</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">
    <link rel="shortcut icon" sizes="16x16" href="../resc/favicon.ico">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
    <style>
        .cssthing {
            margin-left: auto;
            margin-right: auto;
        }
    </style>
    <script src="sortable.js"></script>

</head>
<body>

<div class="container">
    <div class="page-header"><h1>Stats Tables</h1></div>
    <div align="center">
        <span class="cssthing"><a href="itemtable.php" class="btn btn-default"><h4>Item Table</h4></a>&nbsp;&nbsp;&nbsp;<a
                    href="formtable.php" class="btn btn-default"><h4>Form Table</h4></a></span>
    </div>
</div>
</body>
</html>