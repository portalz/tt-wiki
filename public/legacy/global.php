<?php
/**
 * Copyright (c) 2018., K.S. (kportalz@protonmail.com)
 */

//a stripped down version of the original global.php to support legacy code
require('config.php');

$starttime = explode(' ', microtime());
$starttime = $starttime[1] + $starttime[0];

//Handle any errors before the page loads
//connect to the mysql server
$link = new mysqli(MYSQL_SERVER, MYSQL_USER, MYSQL_PASS, MYSQL_DB);
if ($link->connect_errno) {
    echo "Failed to connect to MySQL: (" . $link->connect_errno . ") " . $link->connect_error;
}
/*
//////////////////////////////////////////////////
FUNCTIONS USED GLOBALLY
//////////////////////////////////////////////////
*/
//The function to kick things on errors.
function fuckOff($e)
{
    switch ($e) {
        case "401":
            include "templates/errors/401.php";
            die();
            break;
        case "403":
            include "templates/errors/403.php";
            die();
            break;
        case "404":
            include "templates/errors/404.php";
            die();
            break;
        case "418":
            include "templates/errors/418.php";
            die();
            break;
        case "500":
            include "templates/errors/500.php";
            die();
            break;
        case "503":
            include "templates/errors/503.php";
            die();
            break;
        default:
            include "templates/errors/500.php";
            die();
            break;
    }
}

//escapes mysql variables
function escape($var, $conn)
{
    return mysqli_real_escape_string($conn, $var);
}

//filter for possible XSS
function noxss($input)
{
    return htmlspecialchars($input, ENT_QUOTES, 'UTF-8');
}


?>
