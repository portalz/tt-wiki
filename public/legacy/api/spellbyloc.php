<?php
$is_api = 1;
require_once("../global.php");


if (isset($_GET['location']) or isset($_GET['region'])) {
    if (isset($_GET['location']) && isset($_GET['region'])) {
        die();
    } elseif (isset($_GET['location'])) {
        $text = escape($_GET['location'], $link);
        $sth = mysqli_query($link, "SELECT `Id`,`FriendlyName` FROM `active_spells` WHERE `LearnedAtLocation`='$text' ORDER BY `Id` ASC");
        $rows = array();
        while ($r = mysqli_fetch_assoc($sth)) {
            $rows[] = $r;
        }
        print json_encode($rows);
    } elseif (isset($_GET['region'])) {
        $text = escape($_GET['region'], $link);
        $sth = mysqli_query($link, "SELECT `Id`,`FriendlyName` FROM `active_spells` WHERE `LearnedAtRegion`='$text' ORDER BY `Id` ASC");
        $rows = array();
        while ($r = mysqli_fetch_assoc($sth)) {
            $rows[] = $r;
        }
        print json_encode($rows);
    }
}