<?php
/**
 * Copyright (c) 2018., K.S. (kportalz@protonmail.com)
 */
require_once("global.php");
$title = "Content Statistics";
$navpath = array($title);
function runQuery($q, $n)
{
    global $link;
    return mysqli_fetch_row(mysqli_query($link, $q))[$n];
}

//round((100*$form_male_total)/$form_total,2)
?>


<!doctype html>

<html lang="en">
<head>
    <meta charset="utf-8">

    <title><?=$title;?></title>
</head>

<body>
<div>
    <p>The database of game content that this site includes features the following:</p>
    <ul>
        <li>
            <strong><?= $effect_total = mysqli_fetch_row(mysqli_query($link, "SELECT COUNT(*) FROM `active_effects`"))[0]; ?></strong>
            unique effects
        </li>
        <li>
            <strong><?= $form_total = mysqli_fetch_row(mysqli_query($link, "SELECT COUNT(*) FROM `active_forms` WHERE `MobilityType`='full'"))[0]; ?></strong>
            unique forms
        </li>
        <li>
            <strong><?= $item_total = mysqli_fetch_row(mysqli_query($link, "SELECT COUNT(*) FROM `active_items`"))[0]; ?></strong>
            unique items
        </li>
        <li><strong><?= mysqli_fetch_row(mysqli_query($link, "SELECT COUNT(*) FROM `active_spells`"))[0]; ?></strong>
            unique spells
        </li>
        <li><strong><?= mysqli_fetch_row(mysqli_query($link, "SELECT COUNT(*) FROM `active_locations`"))[0]; ?></strong>
            unique locations
        </li>
        <li><strong><?= runQuery("SELECT COUNT(*) FROM `image_relations`", 0); ?></strong> images from
            <strong><?= runQuery("SELECT COUNT(*) FROM `artist_details`", 0); ?></strong> artists.
        </li>
    </ul>


    <div>
        <h4>Effect Breakdown</h4>
        <p class="indent">Of the <?= $effect_total; ?>
            effects; <?= runQuery("SELECT COUNT(*) FROM `active_effects` WHERE `isLevelUpPerk`=1", 0); ?> are level-up
            perks,
            <?= runQuery("SELECT COUNT(*) FROM `active_effects` WHERE `ObtainedAtLocation`!=''", 0); ?> are obtained by
            searching at a location and some other number are cast but I haven't bothered to make a query for that yet.

        </p>
    </div>

    <div>
        <h4>Form Breakdown</h4>
        <p class="indent">Of the <?= $form_total; ?>
            forms; <?= $form_male_total = runQuery("SELECT COUNT(*) FROM `active_forms` WHERE `Gender`='male' AND `MobilityType`='full'", 0); ?>
            (<?= round((100 * $form_male_total) / $form_total, 2); ?>%) are male
            and <?= $form_female_total = runQuery("SELECT COUNT(*) FROM `active_forms` WHERE `Gender`='female' AND `MobilityType`='full'", 0); ?>
            (<?= round((100 * $form_female_total) / $form_total, 2); ?>%) are female.</p>
    </div>

    <div>
        <h4>Item Breakdown</h4>
        <p class="indent">
            Of the <?= $item_total; ?> items:
        <ul>
            <li><?= $item_hat_total = runQuery("SELECT COUNT(*) FROM `active_items` WHERE `ItemType`='hat'", 0); ?>
                (<?= round((100 * $item_hat_total) / $item_total, 2); ?>%) are hats
            </li>
            <li><?= $item_shirt_total = runQuery("SELECT COUNT(*) FROM `active_items` WHERE `ItemType`='shirt'", 0); ?>
                (<?= round((100 * $item_shirt_total) / $item_total, 2); ?>%) are shirts
            </li>
            <li><?= $item_undershirt_total = runQuery("SELECT COUNT(*) FROM `active_items` WHERE `ItemType`='undershirt'", 0); ?>
                (<?= round((100 * $item_undershirt_total) / $item_total, 2); ?>%) are undershirts
            </li>
            <li><?= $item_pants_total = runQuery("SELECT COUNT(*) FROM `active_items` WHERE `ItemType`='pants'", 0); ?>
                (<?= round((100 * $item_pants_total) / $item_total, 2); ?>%) are pants
            </li>
            <li><?= $item_underpants_total = runQuery("SELECT COUNT(*) FROM `active_items` WHERE `ItemType`='underpants'", 0); ?>
                (<?= round((100 * $item_underpants_total) / $item_total, 2); ?>%) are underpants
            </li>
            <li><?= $item_shoes_total = runQuery("SELECT COUNT(*) FROM `active_items` WHERE `ItemType`='shoes'", 0); ?>
                (<?= round((100 * $item_shoes_total) / $item_total, 2); ?>%) are shoes
            </li>
            <li><?= $item_consumable_total = runQuery("SELECT COUNT(*) FROM `active_items` WHERE `ItemType`='consumable'", 0); ?>
                (<?= round((100 * $item_consumable_total) / $item_total, 2); ?>%) are consumable
                <ul>
                    Of the <?=$item_consumable_total;?> consumable items:
                    <li><?= $item_consumable_tome_total = runQuery("SELECT COUNT(*) FROM `active_items` WHERE `ItemType`='consumable' AND `ConsumableSubItemType`=1", 0); ?>
                        (<?= round((100 * $item_consumable_tome_total) / $item_consumable_total, 2); ?>%) are tomes.</li>

                    <li><?= $item_consumable_spellbook_total = runQuery("SELECT COUNT(*) FROM `active_items` WHERE `ItemType`='consumable' AND `ConsumableSubItemType`=2", 0); ?>
                        (<?= round((100 * $item_consumable_spellbook_total) / $item_consumable_total, 2); ?>%) are spellbooks.</li>

                    <li><?= $item_consumable_restorative_total = runQuery("SELECT COUNT(*) FROM `active_items` WHERE `ItemType`='consumable' AND `ConsumableSubItemType`=3", 0); ?>
                        (<?= round((100 * $item_consumable_restorative_total) / $item_consumable_total, 2); ?>%) are restoratives.</li>

                    <li><?= $item_consumable_wpbomb_total = runQuery("SELECT COUNT(*) FROM `active_items` WHERE `ItemType`='consumable' AND `ConsumableSubItemType`=4", 0); ?>
                        (<?= round((100 * $item_consumable_wpbomb_total) / $item_consumable_total, 2); ?>%) are willpower bombs.</li>

                    <li><?php echo $item_consumable_other_total=($item_consumable_total)-(($item_consumable_wpbomb_total+$item_consumable_restorative_total+$item_consumable_spellbook_total+$item_consumable_tome_total));?>

                        (<?= round((100 * ($item_consumable_other_total)-($item_consumable_wpbomb_total+$item_consumable_restorative_total+$item_consumable_spellbook_total+$item_consumable_tome_total)) / $item_consumable_total, 2); ?>%) are willpower bombs.</li>

                </ul>
            </li>
            <li><?= $item_consumable_reuseable_total = runQuery("SELECT COUNT(*) FROM `active_items` WHERE `ItemType`='consumable_reuseable'", 0); ?>
                (<?= round((100 * $item_consumable_reuseable_total) / $item_total, 2); ?>%) are consumable_reuseable
            </li>
            <li><?= $item_pet_total = runQuery("SELECT COUNT(*) FROM `active_items` WHERE `ItemType`='pet'", 0); ?>
                (<?= round((100 * $item_pet_total) / $item_total, 2); ?>%) are pets
            </li>
            <li><?= $item_accessory_total = runQuery("SELECT COUNT(*) FROM `active_items` WHERE `ItemType`='accessory'", 0); ?>
                (<?= round((100 * $item_accessory_total) / $item_total, 2); ?>%) are accessories
            </li><li><?= $item_rune_total = runQuery("SELECT COUNT(*) FROM `active_items` WHERE `ItemType`='rune'", 0); ?>
                (<?= round((100 * $item_rune_total) / $item_total, 2); ?>%) are runes
            </li>

        </ul>

        </p>
    </div>
    <div>
        <h4>Location Breakdown</h4>
        <p class="indent">
            There are <?= runQuery("SELECT COUNT(*) FROM `active_locations`", 0); ?> locations on the man map
            in <?= $location_total = runQuery("SELECT COUNT(DISTINCT `Region`) FROM `active_locations`", 0); ?> regions.
            The region with the most locations
            is <?= runQuery("SELECT `Region`,COUNT(*) as `count` FROM `active_locations` GROUP BY `Region` ORDER BY `count` DESC", 0); ?>
            with <?= $location_region_popular_total = runQuery("SELECT `Region`,COUNT(*) as `count` FROM `active_locations` GROUP BY `Region` ORDER BY `count` DESC", 1); ?>
            locations.
            The
            location <?= runQuery("SELECT `LearnedAtLocation`,COUNT(*) as `count` FROM `active_spells` WHERE `LearnedAtLocation`!='' GROUP BY `LearnedAtLocation` ORDER BY `count` DESC", 0); ?>
            has the most location-specific spells
            with <?= runQuery("SELECT `LearnedAtLocation`,COUNT(*) as `count` FROM `active_spells` WHERE `LearnedAtLocation`!='' GROUP BY `LearnedAtLocation` ORDER BY `count` DESC", 1); ?>
            .
            The region with the most region-specific spells
            is <?= runQuery("SELECT `LearnedAtRegion`,COUNT(*) as `count` FROM `active_spells` WHERE `LearnedAtRegion`!='' GROUP BY `LearnedAtRegion` ORDER BY `count` DESC", 0); ?>
            with <?= runQuery("SELECT `LearnedAtRegion`,COUNT(*) as `count` FROM `active_spells` WHERE `LearnedAtRegion`!='' GROUP BY `LearnedAtRegion` ORDER BY `count` DESC", 1); ?>
            .

        </p>

    </div>
    <div>
        <h4>Spell Breakdown</h4>
        <p>
            There are <?= runQuery("SELECT COUNT(DISTINCT `MobilityType`) FROM `active_spells`", 0); ?> kinds of spells:
        </p>

    </div>
    <div>


</body>
</html>