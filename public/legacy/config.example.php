<?php
/**
 * Copyright (c) 2018., K.S. (kportalz@protonmail.com)
 */


define('SERVER_ADMIN', 'admin@exmaple.com'); //the email address of the server administrator
define('BASE_DIR', 'https://example.com/wiki'); //the base directory of the installation
define('SITE_NAME', 'ttwiki');

//MySQL Connection Details
define('MYSQL_SERVER', 'localhost');
define('MYSQL_USER', '');
define('MYSQL_PASS', '');
define('MYSQL_DB', '');

define('JSON_LOCATION','http://server/path/to/your/json/datasource');
