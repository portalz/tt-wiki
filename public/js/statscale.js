//Stat Scaling JS
var stats = ["Discipline", "Perception", "Charisma", "Fortitude", "Agility", "Allure", "Magicka", "Succour", "Luck", "HealthBonusPercent", "ManaBonusPercent", "ExtraSkillCriticalPercent", "HealthRecoveryPerUpdate", "ManaRecoveryPerUpdate", "SneakPercent", "AntiSneakPercent", "EvasionPercent", "EvasionNegationPercent", "MeditationExtraMana", "CleanseExtraHealth", "MoveActionPointDiscount", "SpellExtraTFEnergyPercent", "SpellExtraHealthDamagePercent", "CleanseExtraTFEnergyRemovalPercent", "SpellMisfireChanceReduction", "SpellHealthDamageResistance", "SpellTFEnergyDamageResistance", "ExtraInventorySpace"];
$(document).ready(function () {
    $("#formlevelinput").change(function () {
        var level = $('#formlevelinput').val();
        for (var i = 0; i < stats.length; ++i) {
            rowid = "#stat-" + stats[i];
            if ($(rowid).length === 1) {
                maths = (-$(rowid).data("defval") + (-$(rowid).data("defval") * (($("#formlevelinput").val() - 1) * .10))).toFixed(2);
                $("#statcol-" + stats[i]).html(-maths)
            }
        }
    });
});

//var thing = -stat+(-stat*((level-1)*.10))
