/*
 * Copyright (c) 2017.,  K.S. (kportalz@protonmail.com)
 *
 */

$(document).ready(function () {
    $(".close").click(function () {
        var strid = $(this).data("strid");
        $.post("/wiki/misc/clientop.php", {
            a: strid,
            p: "hide"
        }).done(function (data) {
        });
        $(this).closest("div").fadeOut("slow", function () {
        });
    });
});