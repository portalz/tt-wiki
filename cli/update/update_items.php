<?php
/**
 * Copyright (c) 2018., K.S. (kportalz@protonmail.com)
 */

function updateItems($jsonloc, $mysqli)
{
    $start = explode(' ', microtime())[0] + explode(' ', microtime())[1];
    $itemjson = file_get_contents($jsonloc . "ItemPetJSON.json");
    $items = json_decode($itemjson, true);

    foreach ($items as $item) {
        $stmt = $mysqli->prepare("INSERT INTO `active_items` (`Id`,`FriendlyName`,`Description`,`PortraitUrl`,`IsUnique`,`Discipline`,`Perception`,`Charisma`,`Fortitude`,`Agility`,`Allure`,`Magicka`,`Succour`,`Luck`,`MoneyValue`,`MoneyValueSell`,`ItemType`,`UseCooldown`,`Findable`,`FindWeight`,`InstantHealthRestore`,`InstantManaRestore`,`ReuseableHealthRestore`,`ReuseableManaRestore`,`GivesEffectSourceId`,`CurseTFFormSourceId`,`ConsumableSubItemType`,`RuneLevel`,`UsageMessage_Item`,`UsageMessage_Player`,`CurseTFFormdbName`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
        $stmt->bind_param('issssdddddddddddsiiiiiiiiiiisss', $item["Id"], $item["FriendlyName"], $item["Description"], $item["PortraitUrl"], $item["IsUnique"], $item["Discipline"], $item["Perception"], $item["Charisma"], $item["Fortitude"], $item["Agility"], $item["Allure"], $item["Magicka"], $item["Succour"], $item["Luck"], $item["MoneyValue"], $item["MoneyValueSell"], $item["ItemType"], $item["UseCooldown"], $item["Findable"], $item["FindWeight"], $item["InstantHealthRestore"], $item["InstantManaRestore"], $item["ReuseableHealthRestore"], $item["ReuseableManaRestore"], $item["GivesEffectSourceId"], $item["CurseTFFormSourceId"], $item["ConsumableSubItemType"], $item["RuneLevel"], $item["UsageMessage_Item"],$item["UsageMessage_Player"],$item["CurseTFFormdbName"]);
        $stmt->execute();
        echo "Added item " . $item["FriendlyName"] . "(" . $item["Id"] . ")[" . $stmt->affected_rows . " row(s)]" . PHP_EOL;
    }
    echo('Database import completed in ' . round((explode(' ', microtime())[0] + explode(' ', microtime())[1]) - $start, 4) . ' seconds.' . PHP_EOL);
    return true;
}