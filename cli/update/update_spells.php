<?php
/**
 * Copyright (c) 2018., K.S. (kportalz@protonmail.com)
 */

function updateSpells($jsonloc,$mysqli)
{
	$start = explode(' ', microtime())[0] + explode(' ', microtime())[1];
	$itemjson = file_get_contents($jsonloc."SpellJSON.json");
	$items = json_decode($itemjson, true);

	foreach($items as $item)
	{
	    $stmt = $mysqli->prepare("INSERT INTO `active_spells` (`Id`,`FriendlyName`,`Description`,`ManaCost`,`TFPointsAmount`,`HealthDamageAmount`,`LearnedAtRegion`,`LearnedAtLocation`,`DiscoveryMessage`,`IsLive`,`IsPlayerLearnable`,`MobilityType`,`FormSourceId`,`GivesEffectSourceId`,`ExclusiveToFormSourceId`,`ExclusiveToItemSourceId`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
        $stmt->bind_param('issiddssssisiiii',$item["Id"],$item["FriendlyName"],$item["Description"],$item["ManaCost"],$item["TFPointsAmount"],$item["HealthDamageAmount"],$item["LearnedAtRegion"],$item["LearnedAtLocation"],$item["DiscoveryMessage"],$item["IsLive"],$item["IsPlayerLearnable"],$item["MobilityType"],$item["FormSourceId"],$item["GivesEffectSourceId"],$item["ExclusiveToFormSourceId"],$item["ExclusiveToItemSourceId"]);
        $stmt->execute();
        echo "Added spell " . $item["FriendlyName"] . "(" . $item["Id"] . ")[" . $stmt->affected_rows . " row(s)]" . PHP_EOL;
    }
	 echo('Database import completed in '.round((explode(' ', microtime())[0] + explode(' ', microtime())[1]) - $start, 4).' seconds.').PHP_EOL; 
    return true;
}