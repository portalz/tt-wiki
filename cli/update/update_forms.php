<?php
/**
 * Copyright (c) 2018., K.S. (kportalz@protonmail.com)
 */

function updateForms($jsonloc,$mysqli)
{
	$start = explode(' ', microtime())[0] + explode(' ', microtime())[1];
	$itemjson = file_get_contents($jsonloc."FormJSON.json");
	$items = json_decode($itemjson, true);

	foreach($items as $item)
	{
        $stmt = $mysqli->prepare("INSERT INTO `active_forms` (`Id`, `FriendlyName`, `Description`, `TFEnergyType`, `TFEnergyRequired`, `Gender`,`MobilityType`, `PortraitUrl`, `IsUnique`, `Discipline`, `Perception`, `Charisma`, `Fortitude`, `Agility`, `Allure`, `Magicka`, `Succour`, `Luck`, `Submission_Dominance`, `Corruption_Purity`, `Chaos_Order`, `HealthBonusPercent`, `ManaBonusPercent`, `ExtraSkillCriticalPercent`, `HealthRecoveryPerUpdate`, `ManaRecoveryPerUpdate`, `SneakPercent`, `EvasionPercent`, `EvasionNegationPercent`, `MeditationExtraMana`, `CleanseExtraHealth`, `MoveActionPointDiscount`, `SpellExtraTFEnergyPercent`, `SpellExtraHealthDamagePercent`, `CleanseExtraTFEnergyRemovalPercent`, `SpellMisfireChanceReduction`, `SpellHealthDamageResistance`, `SpellTFEnergyDamageResistance`, `ExtraInventorySpace`,`ItemSourceId`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
        $stmt->bind_param('isssdsssiddddddddddddddddddddddddddddddi',$item["Id"], $item["FriendlyName"], $item["Description"], $item["TFEnergyType"], $item["TFEnergyRequired"], $item["Gender"],$item["MobilityType"], $item["PortraitUrl"], $item["IsUnique"], $item["Discipline"], $item["Perception"], $item["Charisma"], $item["Fortitude"], $item["Agility"], $item["Allure"], $item["Magicka"], $item["Succour"], $item["Luck"], $item["Submission_Dominance"], $item["Corruption_Purity"], $item["Chaos_Order"], $item["HealthBonusPercent"], $item["ManaBonusPercent"], $item["ExtraSkillCriticalPercent"], $item["HealthRecoveryPerUpdate"], $item["ManaRecoveryPerUpdate"], $item["SneakPercent"], $item["EvasionPercent"], $item["EvasionNegationPercent"], $item["MeditationExtraMana"], $item["CleanseExtraHealth"], $item["MoveActionPointDiscount"], $item["SpellExtraTFEnergyPercent"], $item["SpellExtraHealthDamagePercent"], $item["CleanseExtraTFEnergyRemovalPercent"], $item["SpellMisfireChanceReduction"], $item["SpellHealthDamageResistance"], $item["SpellTFEnergyDamageResistance"], $item["ExtraInventorySpace"],$item["ItemSourceId"]);
        $stmt->execute();
        echo "Added form " . $item["FriendlyName"] . "(" . $item["Id"] . ")[" . $stmt->affected_rows . " row(s)]" . PHP_EOL;
	}
	echo('Database import completed in '.round((explode(' ', microtime())[0] + explode(' ', microtime())[1]) - $start, 4).' seconds.'.PHP_EOL); 
    return true;
}