<?php
/**
 * Copyright (c) 2017.,  K.S. (kportalz@protonmail.com)
 *
 */
//Generates php to make database updaters.
//It's probably best if you just pretend this mess doesn't exist. I wish I could, but it was useful in making the update scripts in 2015.
//2018 Update: don't touch this, don't even think about touching this. This isn't how we make import scripts anymore. Now we use "Prepared statements"
$fields = array("Id", "dbName", "FriendlyName", "FormdbName", "Description", "ManaCost", "TFPointsAmount", "HealthDamageAmount", "LearnedAtRegion", "LearnedAtLocation", "DiscoveryMessage", "IsLive", "IsPlayerLearnable", "GivesEffect", "ExclusiveToForm", "ExclusiveToItem", "MobilityType");
$base = "INSERT INTO `ttcalc`.`active_effects` (";
//echo $base.'</br>';
foreach($fields as $field)
{
	$base .= "`$field`, ";
	//echo $base.PHP_EOL;
}
$base=rtrim($base, ", ");
//echo $base.PHP_EOL;
$base .= ") VALUES (";
echo $base.PHP_EOL;
foreach ($fields as $field)
{
	$base .= '\'".$item["'.$field.'"]."\',';
	//echo $base.PHP_EOL;
}
//oh no you've read too far. delete this file. save yourself.
$base = rtrim($base, ",");
$base .= ")";
echo PHP_EOL;
echo $base;
echo PHP_EOL;

foreach($fields as $field){
echo '$item["'.$field.'"] = escape($item["'.$field.'"], $link);'.PHP_EOL;
}