<?php
/**
 * Copyright (c) 2018., K.S. (kportalz@protonmail.com)
 */

function updateEffects($jsonloc, $mysqli)
{
    $start = explode(' ', microtime())[0] + explode(' ', microtime())[1];
    $itemjson = file_get_contents($jsonloc . "EffectJSON.json");
    $items = json_decode($itemjson, true);

    foreach ($items as $item) {
        $stmt = $mysqli->prepare("INSERT INTO `active_effects` VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
        $stmt->bind_param('issiiiisissssssdddddddddiii', $item["Id"],$item["FriendlyName"],$item["Description"],$item["AvailableAtLevel"],$item["isLevelUpPerk"],$item["Duration"],$item["Cooldown"],$item["ObtainedAtLocation"],$item["IsRemovable"],$item["MessageWhenHit"],$item["MessageWhenHit_M"],$item["MessageWhenHit_F"],$item["AttackerWhenHit"],$item["AttackerWhenHit_M"],$item["AttackerWhenHit_F"],$item["Discipline"],$item["Perception"],$item["Charisma"],$item["Fortitude"],$item["Agility"],$item["Allure"],$item["Magicka"],$item["Succour"],$item["Luck"],$item["PreRequisiteEffectSourceId"],$item["RequiredGameMode"],$item["BlessingCurseStatus"]);
        $stmt->execute();
        echo "Added effect " . $item["FriendlyName"] . "(" . $item["Id"] . ")[" . $stmt->affected_rows . " row(s)]" . PHP_EOL;
    }
    echo('Database import completed in ' . round((explode(' ', microtime())[0] + explode(' ', microtime())[1]) - $start, 4) . ' seconds.' . PHP_EOL);
    return true;
}
