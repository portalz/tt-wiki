<?php
/**
 * Copyright (c) 2018., K.S. (kportalz@protonmail.com)
 */

function updateLocations($jsonloc, $mysqli)
{
    $start = explode(' ', microtime())[0] + explode(' ', microtime())[1];
    $itemjson = file_get_contents($jsonloc . "LocationJSON.json");
    $items = json_decode($itemjson, true);
    foreach ($items as $item) {
        $stmt = $mysqli->prepare("INSERT INTO `active_locations` (`Id`, `dbName`, `Name`, `Description`, `X`, `Y`, `IsSafe`, `ImageUrl`, `Name_North`, `Name_East`, `Name_South`, `Name_West`, `Region`, `CovenantController`, `TakeoverAmount`, `FriendlyName_North`, `FriendlyName_East`, `FriendlyName_South`, `FriendlyName_West`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
        $stmt->bind_param('isssiisssssssiissss', $item["Id"], $item["dbName"], $item["Name"], $item["Description"], $item["X"], $item["Y"], $item["IsSafe"], $item["ImageUrl"], $item["Name_North"], $item["Name_East"], $item["Name_South"], $item["Name_West"], $item["Region"], $item["CovenantController"], $item["TakeoverAmount"], $item["FriendlyName_North"], $item["FriendlyName_East"], $item["FriendlyName_South"], $item["FriendlyName_West"]);
        $stmt->execute();
        echo "Added location " . $item["Name"] . "(" . $item["Id"] . ")[" . $stmt->affected_rows . " row(s)]" . PHP_EOL;
    }
    echo('Database import completed in ' . round((explode(' ', microtime())[0] + explode(' ', microtime())[1]) - $start, 4) . ' seconds.' . PHP_EOL);
    return true;
}