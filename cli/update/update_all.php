<?php
/**
 * Copyright (c) 2018., K.S. (kportalz@protonmail.com)
 */
//Enable verbose error reporting in case /anything/ goes wrong
error_reporting(E_ALL);
mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);

//This section is for command line arguments.
//-========================================================================-//
$options = array('effect' => null, 'form' => null, 'item' => null, 'spell' => null, 'furniture' => null, 'location' => null);
if (php_sapi_name() != "cli") {
    die("This must run in commandline. Also, shame. This should absolutely not be in your public directory.");
}
if (sizeof($argv) == 1 OR in_array("--help", $argv) OR in_array("-h", $argv)) {
    echo "Help" . PHP_EOL . PHP_EOL
        . "--all or -a to do all" . PHP_EOL
        . "--effects or -e will do effects" . PHP_EOL
        . "--forms or -f will do forms" . PHP_EOL
        . "--items or -i will do items and pets" . PHP_EOL
        . "--locations or -l will do locations" . PHP_EOL
        . "--spells or -s will do spells" . PHP_EOL
        . "--meubles or -m will do furniture" . PHP_EOL
        . "--help or -h to display this menu" . PHP_EOL
        . "--dryrun will enable and disable maintenance mode and update the last update time without doing anything" . PHPEOL;
    exit();
}
if (in_array("-a", $argv) OR in_array("--all", $argv)) {
    $options = array_fill_keys(array_keys($options), true);
}
if (in_array("-e", $argv) OR in_array("--effects", $argv)) {
    $options['effect'] = true;
}
if (in_array("-f", $argv) OR in_array("--forms", $argv)) {
    $options['form'] = true;
}
if (in_array("-i", $argv) OR in_array("--items", $argv)) {
    $options['item'] = true;
}
if (in_array("-l", $argv) OR in_array("--locations", $argv)) {
    $options['location'] = true;
}
if (in_array("-s", $argv) OR in_array("--spells", $argv)) {
    $options['spell'] = true;
}
if (in_array("-m", $argv) OR in_array("--meubles", $argv)) {
    $options['furniture'] = true;
}
if (in_array("--dryrun", $argv)) {
    $options['dry'] = true;
}
if (!in_array(true, $options)) {
    die("No options selected." . PHP_EOL);
}





require_once "../../public/legacy/config.php";
$mysqli = new mysqli(MYSQL_SERVER, MYSQL_USER, MYSQL_PASS, MYSQL_DB);
if ($mysqli->connect_errno) {
    echo "Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
}


//-========================================================================-//
//Actually run updates
//-========================================================================-//
include "update_effects.php";
include "update_forms.php";
include "update_furniture.php";
include "update_items.php";
include "update_locations.php";
include "update_spells.php";
echo "JSON files will be loaded from " . JSON_LOCATION . PHP_EOL;
$errors = 0;

$startmain = explode(' ', microtime())[0] + explode(' ', microtime())[1];


if ($options["effect"] == true) {
    echo "Running effects import script..." . PHP_EOL;
    $stmt = $mysqli->prepare("TRUNCATE `active_effects`");
    $stmt->execute();
    unset($stmt);
    if (updateEffects(JSON_LOCATION, $mysqli) == false) {
        die("error in effects");
    }
}

if ($options["form"] == true) {
    echo "Running forms import script..." . PHP_EOL;
    $stmt = $mysqli->prepare("TRUNCATE `active_forms`");
    $stmt->execute();
    unset($stmt);
    if (updateForms(JSON_LOCATION, $mysqli) == false) {
        die("error in forms");
    }
}

if ($options["furniture"] == true) {
    echo "Running furniture import script..." . PHP_EOL;
    $stmt = $mysqli->prepare("TRUNCATE `active_furniture`");
    $stmt->execute();
    unset($stmt);
    if (updateFurniture(JSON_LOCATION, $mysqli) == false) {
        die("error in furniture");
    }
}


if ($options["item"] == true) {
    echo "Running items import script..." . PHP_EOL;
    $stmt = $mysqli->prepare("TRUNCATE `active_items`");
    $stmt->execute();
    unset($stmt);
    if (updateItems(JSON_LOCATION, $mysqli) == false) {
        die("error in items");
    }
}
if ($options["location"] == true) {
    echo "Running locations import script..." . PHP_EOL;
    $stmt = $mysqli->prepare("TRUNCATE `active_locations`");
    $stmt->execute();
    unset($stmt);
    if (updateLocations(JSON_LOCATION, $mysqli) == false) {
        die("error in locations");
    }
}
if ($options["spell"] == true) {
    echo "Running spells import script..." . PHP_EOL;
    $stmt = $mysqli->prepare("TRUNCATE `active_spells`");
    $stmt->execute();
    unset($stmt);
    if (updateSpells(JSON_LOCATION, $mysqli) == false) {
        die("error in spells");
    }
}

echo "checking file consistency".PHP_EOL;
$formjson = file_get_contents(JSON_LOCATION."FormJSON.json");
$forms = json_decode($formjson, true);
foreach ($forms as $form)
{
    if($form["MobilityType"]=="full" && $form["ItemSourceId"]!=null)
        echo "WARNING: form Id ".$form["Id"]." is animate but references item Id ".$form["ItemSourceId"].". This is likely to create a ghost item.".PHP_EOL;
}
echo "done".PHP_EOL;