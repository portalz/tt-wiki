<?php
/**
 * Copyright (c) 2018., K.S. (kportalz@protonmail.com)
 */

function updateFurniture($jsonloc, $mysqli)
{
    $start = explode(' ', microtime())[0] + explode(' ', microtime())[1];
    $itemjson = file_get_contents($jsonloc . "FurnitureJSON.json");
    $items = json_decode($itemjson, true);

    foreach ($items as $item) {
        $stmt = $mysqli->prepare("INSERT INTO `active_furniture` (`Id`, `dbType`, `FriendlyName`, `GivesEffectSourceId`, `APReserveRefillAmount`, `BaseCost`, `BaseContractTurnLength`, `GivesItemSourceId`, `MinutesUntilReuse`, `Description`, `PortraitUrl`) VALUES (?,?,?,?,?,?,?,?,?,?,?)");
        $stmt->bind_param('issiddiidss', $item["Id"], $item["dbType"], $item["FriendlyName"], $item["GivesEffectSourceId"], $item["APReserveRefillAmount"], $item["BaseCost"], $item["BaseContractTurnLength"], $item["GivesItemSourceId"], $item["MinutesUntilReuse"], $item["Description"], $item["PortraitUrl"]);
        $stmt->execute();
        echo "Added furniture " . $item["FriendlyName"] . "(" . $item["Id"] . ")[" . $stmt->affected_rows . " row(s)]" . PHP_EOL;
    }
    echo('Database import completed in ' . round((explode(' ', microtime())[0] + explode(' ', microtime())[1]) - $start, 4) . ' seconds.' . PHP_EOL);
    return true;
}